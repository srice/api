<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ticket_category_id')->nullable()->unsigned();
            $table->bigInteger('comite_id')->unsigned()->nullable();
            $table->string('sujet');
            $table->tinyInteger('status')->default(\App\Model\Support\Ticket\Ticket::STATUS_NEW);
            $table->tinyInteger('priority')->default(\App\Model\Support\Ticket\Ticket::PRIORITY_NORMAL);
            $table->timestamps();

            $table->foreign('ticket_category_id')->references('id')->on('ticket_categories')->cascadeOnDelete();
            $table->foreign('comite_id')->references('id')->on('comites')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
