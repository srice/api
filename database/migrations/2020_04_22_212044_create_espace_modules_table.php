<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspaceModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_modules', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('espace_id')->nullable()->unsigned();
            $table->bigInteger('module_id')->nullable()->unsigned();
            $table->boolean('state')->default(false);
            $table->boolean('checkout')->default(false);

            $table->foreign('espace_id')->references('id')->on('espaces')->cascadeOnDelete();
            $table->foreign('module_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_modules');
    }
}
