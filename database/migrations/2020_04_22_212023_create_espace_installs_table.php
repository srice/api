<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspaceInstallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_installs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('espace_id')->nullable()->unsigned();
            $table->time('time')->nullable();
            $table->string('description');
            $table->integer('state')->default(0);
            $table->string('errorLog')->nullable();

            $table->foreign('espace_id')->references('id')->on('espaces')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_installs');
    }
}
