<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('famille_id')->unsigned();
            $table->string('name');
            $table->integer('kernel')->default(0)->comment("0: Aucun|1: Espace|2: Module");
            $table->bigInteger('module_id')->default(0)->nullable()->comment("0: Espace à installer |id du module: module à installer")->unsigned();

            $table->foreign('famille_id')->references('id')->on('familles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
