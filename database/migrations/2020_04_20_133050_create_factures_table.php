<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('commande_id')->nullable()->unsigned();
            $table->bigInteger('comite_id')->nullable()->unsigned();
            $table->timestamp('date');
            $table->string('amount');
            $table->integer('state')->nullable()->default(0)->comment("0: Brouillon |1: Valider |2: Non Payer |3: Partiellement Payer |4: Payer |5: Impayer");

            $table->foreign('commande_id')->references('id')->on('commandes')->cascadeOnDelete();
            $table->foreign('comite_id')->references('id')->on('comites')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factures');
    }
}
