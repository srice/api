<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServerRegistarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_registars', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::table('servers', function (Blueprint $table) {
            $table->foreign('registar_id')->references('id')->on('server_registars')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_registars');
    }
}
