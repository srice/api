<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrats', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('comite_id')->unsigned();
            $table->bigInteger('contrat_famille_id')->unsigned();
            $table->bigInteger('commande_id')->nullable()->unsigned();
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->text('description')->nullable();
            $table->integer('state')->default(0)->comment("0: Brouillon |1: Valider |2: En attente de signature client |3: Executer |4: Bientôt expirer |5: Expirer |6: Résilier");
            $table->string('signated_key')->nullable();

            $table->foreign('comite_id')->references('id')->on('comites')->cascadeOnDelete();
            $table->foreign('contrat_famille_id')->references('id')->on('contrat_familles');
            $table->foreign('commande_id')->references('id')->on('commandes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrats');
    }
}
