<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravauxCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travaux_comments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('travaux_id')->unsigned()->nullable();
            $table->text('comment');
            $table->timestamps();

            $table->foreign('travaux_id')->references('id')->on('travauxes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travaux_comments');
    }
}
