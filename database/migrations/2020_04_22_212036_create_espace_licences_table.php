<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspaceLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_licences', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('espace_id')->nullable()->unsigned();
            $table->string('numero');
            $table->boolean('module')->default(false);
            $table->bigInteger('module_id')->nullable()->unsigned();
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();

            $table->foreign('espace_id')->references('id')->on('espaces')->cascadeOnDelete();
            $table->foreign('module_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_licences');
    }
}
