<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');

        for($i=0;$i < 10;$i++) {
            \App\Model\Article\Article::create([
                "title" => $faker->sentence,
                "content" => $faker->paragraph
            ]);
        }
    }
}
