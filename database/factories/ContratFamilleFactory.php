<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Contrat\ContratFamille::class, function (Faker $faker) {
    return [
        "service_id" => null,
        "name" => $faker->word
    ];
});
