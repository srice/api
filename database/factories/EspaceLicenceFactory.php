<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Espace\EspaceLicence::class, function (Faker $faker) {
    return [
        "espace_id" => null,
        "numero" => "XXXX-XXXX-XXXX-2020",
        "module" => false,
        "module_id" => null,
        "start" => now(),
        "end" => now()->addYear()
    ];
});
