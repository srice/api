<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\FactureItem::class, function (Faker $faker) {
    return [
        "facture_id" => null,
        "service_id" => null,
        "description" => $faker->realText(),
        "quantite" => 1,
        "amount" => 120.00
    ];
});
