<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Commande\Commande::class, function (Faker $faker) {
    return [
        "comite_id" => null,
        "date" => now()
    ];
});
