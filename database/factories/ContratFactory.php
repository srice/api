<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Contrat\Contrat::class, function (Faker $faker) {
    return [
        "comite_id" => null,
        "contrat_famille_id" => null,
        "commande_id" => null,
        "start" => now(),
        "end" => now()->addYear(),
        "description" => $faker->realText(),
        "state" => 0
    ];
});
