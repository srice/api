<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Support\Ticket\TicketCategory::class, function (Faker $faker) {
    return [
        "name" => "Test"
    ];
});
