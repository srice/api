<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Infrastructure\Server\Server::class, function (Faker $faker) {
    return [
        "registar_id" => null,
        "name" => "ns2566321.ip-ovh.org",
        "ip" => "214.25.36.102"
    ];
});
