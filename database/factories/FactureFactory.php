<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Facture::class, function (Faker $faker) {
    return [
        "commande_id" => null,
        "comite_id" => null,
        "date" => now(),
        "amount" => 120.00,
        "state" => 0
    ];
});
