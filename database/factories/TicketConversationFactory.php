<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Support\Ticket\TicketConversation::class, function (Faker $faker) {
    return [
        "ticket_id" => null,
        "text" => $faker->realText(),
        "user" => null,
        "support" => null
    ];
});
