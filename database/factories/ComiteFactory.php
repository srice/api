<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Comite\Comite;
use Faker\Generator as Faker;

$factory->define(Comite::class, function (Faker $faker) {
    return [
        "name" => "Comite de Test",
        "adresse" => "Rue de Test",
        "codePostal" => "00000",
        "ville" => "Ville de Test",
        "tel" => "00 00 00 00 00",
        "email" => $faker->companyEmail,
        "customerId" => null
    ];
});
