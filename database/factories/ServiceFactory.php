<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Prestation\Service::class, function (Faker $faker) {
    return [
        "famille_id" => 1,
        "name"  => $faker->sentence,
        "kernel" => 1
    ];
});
