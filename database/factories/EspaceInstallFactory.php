<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Espace\EspaceInstall::class, function (Faker $faker) {
    return [
        "espace_id" => null,
        "time" => now()->format('H:i:s'),
        "description" => $faker->realText(),
        "state" => 0,
        "errorLog" => null
    ];
});
