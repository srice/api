<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Prestation\ModuleTask::class, function (Faker $faker) {
    $start = now();
    return [
        "module_id" => null,
        "subject" => $faker->sentence,
        "description" => $faker->realText(),
        "start"  => $start,
        "end" => $start->addDays(rand(1,15)),
        "state" => rand(0,4)
    ];
});
