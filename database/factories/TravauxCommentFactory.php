<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Support\Travaux\TravauxComment::class, function (Faker $faker) {
    return [
        "travaux_id" => null,
        "comment" => "Lorem"
    ];
});
