<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Comite\Payment::class, function (Faker $faker) {
    return [
        "comite_id" => null,
        "stripe_id" => null
    ];
});
