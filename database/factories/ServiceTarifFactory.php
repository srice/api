<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Prestation\ServiceTarif::class, function (Faker $faker) {
    return [
        "service_id" => null,
        "name"  => "Nom du service",
        "amount" => 0.00
    ];
});
