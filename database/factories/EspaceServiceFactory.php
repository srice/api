<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Espace\EspaceService::class, function (Faker $faker) {
    return [
        "espace_id" => null,
        "server_id" => 1,
        "database" => "test"
    ];
});
