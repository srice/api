<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Support\Ticket\Ticket::class, function (Faker $faker) {
    return [
        "ticket_category_id" => null,
        "comite_id" => null,
        "sujet" => $faker->sentence(6),
    ];
});
