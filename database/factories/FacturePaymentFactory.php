<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\FacturePayment::class, function (Faker $faker) {
    return [
        "facture_id" => null,
        "payment_id" => null,
        "date" => now(),
        "amount" => 120.00,
        "state" => 0
    ];
});
