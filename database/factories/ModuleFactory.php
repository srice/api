<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Prestation\Module::class, function (Faker $faker) {
    return [
        "name" => "Module N°".$faker->randomNumber(2),
        "description" => $faker->text,
        "version" => $faker->randomNumber(1).'.'.$faker->randomNumber(1).'.'.$faker->randomNumber(1),
        "release" => $faker->numberBetween(0,6)
    ];
});
