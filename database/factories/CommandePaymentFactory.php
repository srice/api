<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Commande\CommandePayment::class, function (Faker $faker) {
    return [
        "commande_id" => null,
        "payment_id" => null,
        "date" => now(),
        "amount" => 12.00,
        "state" => 0
    ];
});
