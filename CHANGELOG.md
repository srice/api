# V.0.7.11 (06/05/2020 - 12:13)

* Correction du bug #11

# V.0.7.10 (06/05/2020 - 12:03)
### Core
* Laravel 7.10

### Webservice
#### Blog
* Gestion des articles: `List / Create / Get / Update / Delete`
* Gestion des catégories articles: `List / Create / Get / Update / Delete / Blogs`

#### Notification
* Initialisation du service (non opérant)

### Support
#### Ticket
* Gestion des catégories de ticket: `List / Create / Get / Update / Delete / Tickets`
* Gestion des tickets: `List / Create / Get / Update / Delete`
* Gestion des conversations de ticket: `List / Create / Get / Update / Delete`

#### Travaux
* Gestion des secteurs de travaux: `List / Create / Get / Update / Delete / Travaux`
* Gestion des Travaux: `List / Create / Get / Update / Delete`
* Gestion des commentaire des travaux: `List / Create / Get / Update / Delete`

# V.0.6.2 (24/04/2020 - 14:36)
### Infrastructure
#### Server
* Gestion des registars: `List / Create / Get / Update / Delete`
* Gestion des serveurs: `List / Create / Get / Update / Delete`

# V.0.5.6 (24/04/2020 - 11:53) 
### Core
* Ajout du système Sentry

### Espace
* Gestion des espaces: `List / Create / Get / Update / Delete`
* Gestion des services: `List / Create / Get / Update / Delete`
* Gestion des Installations: `List / Create / Get / Update / Delete`
* Gestion des Licences: `List / Create / Get / Update / Delete`
* Gestion des Modules: `List / Create / Get / Update / Delete`

# V.0.4.10 (22/04/2020 - 20:56)

### Facturation
#### Contrat
* Gestion des familles de contrat: `List / Create / Get / Update / Delete / Contrats`
* Gestion des contrats: `List / Create / Get / Update / Delete`
