<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketConversation extends Model
{
    protected $guarded = [];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }
}
