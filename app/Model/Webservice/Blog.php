<?php

namespace App\Model\Webservice;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = [];
    protected $dates = ["created_at", "updated_at", "published_at"];

    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id');
    }

}
