<?php

namespace App\Model\Webservice;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
}
