<?php

namespace App\Model\Prestation;

use Illuminate\Database\Eloquent\Model;

class Famille extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
