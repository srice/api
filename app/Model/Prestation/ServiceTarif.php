<?php

namespace App\Model\Prestation;

use App\Model\Facturation\Commande\CommandeItem;
use Illuminate\Database\Eloquent\Model;

class ServiceTarif extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function commande_items()
    {
        return $this->hasMany(CommandeItem::class);
    }
}
