<?php

namespace App\Model\Prestation;

use Illuminate\Database\Eloquent\Model;

class ModuleTask extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        "start",
        "end"
    ];

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }
}
