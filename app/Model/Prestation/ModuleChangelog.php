<?php

namespace App\Model\Prestation;

use Illuminate\Database\Eloquent\Model;

class ModuleChangelog extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ["date"];

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }


}
