<?php

namespace App\Model\Prestation;

use App\Model\Facturation\Commande\CommandeItem;
use App\Model\Facturation\Contrat\ContratFamille;
use App\Model\Facturation\FactureItem;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function famille()
    {
        $this->belongsTo(Famille::class, 'famille_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function tarifs()
    {
        return $this->hasMany(ServiceTarif::class);
    }

    public function commande_items()
    {
        return $this->hasMany(CommandeItem::class);
    }

    public function facture_items()
    {
        return $this->hasMany(FactureItem::class);
    }

    public function contrat_familles()
    {
        return $this->hasMany(ContratFamille::class);
    }
}
