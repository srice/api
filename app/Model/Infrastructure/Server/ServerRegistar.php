<?php

namespace App\Model\Infrastructure\Server;

use Illuminate\Database\Eloquent\Model;

class ServerRegistar extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function servers()
    {
        return $this->hasMany(Server::class);
    }
}
