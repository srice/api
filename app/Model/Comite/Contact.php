<?php

namespace App\Model\Comite;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
