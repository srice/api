<?php

namespace App\Model\Espace;

use App\Model\Prestation\Module;
use Illuminate\Database\Eloquent\Model;

class EspaceModule extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function espace()
    {
        return $this->belongsTo(Espace::class, 'espace_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }
}
