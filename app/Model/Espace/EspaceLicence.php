<?php

namespace App\Model\Espace;

use Illuminate\Database\Eloquent\Model;

class EspaceLicence extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function espace()
    {
        return $this->belongsTo(Espace::class, 'espace_id');
    }
}
