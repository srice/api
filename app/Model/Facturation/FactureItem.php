<?php

namespace App\Model\Facturation;

use App\Model\Prestation\Service;
use Illuminate\Database\Eloquent\Model;

class FactureItem extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function facture()
    {
        return $this->belongsTo(Facture::class, 'facture_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
