<?php

namespace App\Model\Facturation;

use App\Model\Comite\Payment;
use Illuminate\Database\Eloquent\Model;

class FacturePayment extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['date'];

    public function facture()
    {
        return $this->belongsTo(Facture::class, 'facture_id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }
}
