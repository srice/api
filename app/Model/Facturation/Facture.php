<?php

namespace App\Model\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Commande\Commande;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['date'];

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commande_id');
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function items()
    {
        return $this->hasMany(FactureItem::class);
    }

    public function payments()
    {
        return $this->hasMany(FacturePayment::class);
    }
}
