<?php

namespace App\Model\Facturation\Contrat;

use App\Model\Prestation\Service;
use Illuminate\Database\Eloquent\Model;

class ContratFamille extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function contrats()
    {
        return $this->hasMany(Contrat::class);
    }
}
