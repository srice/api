<?php

namespace App\Model\Facturation\Commande;

use App\Model\Comite\Comite;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Facture;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function items()
    {
        return $this->hasMany(CommandeItem::class);
    }

    public function payments()
    {
        return $this->hasMany(CommandePayment::class);
    }

    public function factures()
    {
        return $this->hasMany(Facture::class);
    }

    public function contrats()
    {
        return $this->hasMany(Contrat::class);
    }
}
