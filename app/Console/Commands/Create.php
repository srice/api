<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Routing\Route;

class Create extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create {class} {dossier?} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genere les information usuel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dossier = $this->argument('dossier');
        $class = $this->argument('class');

        $this->createController($class, $dossier);
        $this->createTest($class, $dossier);
        $this->createFactory($class);

        $this->info('DONE !!!');
    }


    private function createController($class, $dossier = null)
    {
        $this->info('Création du controller');
        $this->call('make:controller', [
            "name" => $dossier.'\\'.$class.'Controller'
        ]);

        return null;
    }

    private function createTest($class, $dossier = null)
    {
        $this->info('Création du test');
        $this->call('make:test', [
            "name" => $dossier.'\\'.$class.'Test'
        ]);
        return null;
    }

    private function createFactory($class)
    {
        $this->info('Création de la factory');
        $this->call('make:factory', [
            "name" => $class.'Factory'
        ]);
        return null;
    }
}
