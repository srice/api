<?php

namespace App\Http\Controllers\Webservice\Blog;

use App\Http\Controllers\Controller;
use App\Model\Webservice\Blog;
use App\Model\Webservice\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class BlogCategoryController
 * @package App\Http\Controllers\Webservice\Blog
 * @group Webservice/blog/category
 */
class BlogCategoryController extends Controller
{
    /**
     * @var BlogCategory
     */
    private $blogCategory;
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogCategoryController constructor.
     * @param BlogCategory $blogCategory
     * @param Blog $blog
     */
    public function __construct(BlogCategory $blogCategory, Blog $blog)
    {
        $this->blogCategory = $blogCategory;
        $this->blog = $blog;
    }

    /**
     * List Category
     * Liste des catégories d'article
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $categories = $this->blogCategory->newQuery()->get();

        return response()->json($categories->toArray(), 201);
    }

    /**
     * Create Category
     * Création d'une catégorie d'article
     * @authenticated
     * @bodyParam name string required Nom de la catégorie. Example: Comité
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $category = $this->blogCategory->newQuery()->create([
            "name" => $request->name
        ]);

        return response()->json($category->toArray(), 201);
    }

    /**
     * Get Category
     * Affiche les information d'une catégorie d'article
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $category = $this->blogCategory->newQuery()->find($id);

        return response()->json($category->toArray(), 201);
    }

    /**
     * Update Category
     * Met à jour une catégorie d'article
     * @authenticated
     * @bodyParam name string Nom de la catégorie. Example: Comité
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $category = $this->blogCategory->newQuery()->find($id);
        if($request->exists('name') == true){$category->name = $request->name;}
        $category->save();

        return response()->json($category->toArray(), 201);
    }

    /**
     * Delete Category
     * Supprime une catégorie d'article
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $category = $this->blogCategory->newQuery()->find($id);
        $category->delete();

        return response()->json($category->toArray(), 201);
    }

    /**
     * List Articles For Category
     * Liste les articles d'une catégorie spécifié
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function blogs($id)
    {
        $blogs = $this->blog->newQuery()->where('blog_category_id', $id)->get();

        return response()->json($blogs->toArray(), 201);
    }
}
