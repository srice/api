<?php

namespace App\Http\Controllers\Webservice\Blog;

use App\Http\Controllers\Controller;
use App\Model\Webservice\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Class BlogController
 * @package App\Http\Controllers\Webservice\Blog
 * @group Webservice/Blog
 */
class BlogController extends Controller
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogController constructor.
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * List Blog
     * LIste des articles
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $blogs = $this->blog->newQuery()->get();

        return response()->json($blogs->toArray(), 201);
    }

    /**
     * Create Blog
     * Création d'un article
     * @authenticated
     * @bodyParam blog_category_id int required ID de la catégorie affilier à l'article. Example: 1
     * @bodyParam title string required Titre de l'article. Example: Nouvelle dérogation COVID19 pour les CE et CSE
     * @bodyParam content string required Contenue de l'article. Example: Lorem
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "blog_category_id" => "required",
                "title" => "required",
                "contents" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $blog = $this->blog->newQuery()->create([
            "blog_category_id" => $request->blog_category_id,
            "title" => $request->title,
            "slug" => Str::slug($request->title),
            "content" => $request->contents,
        ]);

        return response()->json($blog->toArray(), 201);
    }

    /**
     * Get Blog
     * Affiche les information d'un article
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $blog = $this->blog->newQuery()->find($id);

        return response()->json($blog->toArray(), 201);
    }

    /**
     * Update Blog
     * Met à jour les information d'un article
     * @authenticated
     * @bodyParam blog_category_id int required ID de la catégorie affilier à l'article. Example: 1
     * @bodyParam title string required Titre de l'article. Example: Nouvelle dérogation COVID19 pour les CE et CSE
     * @bodyParam content string required Contenue de l'article. Example: Lorem
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $blog = $this->blog->newQuery()->find($id);
        if($request->exists('blog_category_id') == true) {$blog->blog_category_id = $request->blog_category_id;}
        if($request->exists('title') == true) {$blog->title = $request->title;}
        if($request->exists('contents') == true) {$blog->content = $request->contents;}
        $blog->save();

        return response()->json($blog->toArray(), 201);
    }

    /**
     * Delete Blog
     * Supprime un article
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $blog = $this->blog->newQuery()->find($id);
        $blog->delete();

        return response()->json($blog->toArray(), 201);
    }
}
