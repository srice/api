<?php

namespace App\Http\Controllers\Webservice\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class NotificationController
 * @package App\Http\Controllers\Webservice\Notification
 * @group Webservice/Notification
 */

class NotificationController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function push(Request $request)
    {
        try {
            $request->validate([
                "espaces" => "required|array",
                "type" => "required|integer",
                "message" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        // Zone de Notification appel HTTP vers le ou les espaces
        return null;
    }
}
