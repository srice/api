<?php

namespace App\Http\Controllers\Comite;

use App\Http\Controllers\Controller;
use App\Model\Comite\Contact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Class ContactController
 * @package App\Http\Controllers\Comite
 * @group Comite/Contact
 * [Gestion des contacts d'un comité]
 */
class ContactController extends Controller
{

    /**
     * Create Contact
     * [Permet la création d'un contact et utilisateur associé]
     * [Lors de son execution d'un email est envoyer à l'adresse mail avec le mot de passe générer aléatoirement]
     * @authenticated
     * @bodyParam name string required Nom/Prénom du contact. Example: Jean Dupond
     * @bodyParam email string required Email du contact. Example: jdupond@sfr.fr
     * @bodyParam position string required Position du contact au sein du comité. Example: Comptable
     * @bodyParam telephone string required Téléphone personnel du contact. Example: 06 00 00 00 00
     * @response 422 {
     *  "data": "Retourne l'exeption",
     *  "errors": "Retourne les erreurs de validation"
     * }
     * @response 201 {
     *  "message": "Retourne les informations du contact"
     * }
     */
    public function store(Request $request, $comite_id)
    {
        try {
            $request->validate([
                "name" => "required",
                "email" => "required|unique:users,contacts",
                "position" => "required",
                "telephone" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "data" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }
        $password = Str::random(8);

        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($password)
        ]);

        $contact = Contact::create([
            "comite_id" => $comite_id,
            "user_id" => $user->id,
            "position" => $request->position,
            "telephone" => $request->telephone
        ]);

        return response()->json([
            "data" => $contact->toArray()
        ], 201);
    }

    /**
     * Get Contact
     * [Permet l'affichage des informations usuel d'un contact]
     * @authenticated
     * @response 201 {
     *  "message": "Retourne les informations du contact"
     * }
     */

    public function get($comite_id, $contact_id)
    {
        $contact = Contact::find($contact_id)->load('comite', 'user');
        return response()->json([
            'data' => $contact->toArray()
        ], 201);
    }

    /**
     * Update Contact
     * [Permet de mettre à jours les informations d'un contact]
     * @authenticated
     * @bodyParam name string Nom/Prénom du contact. Example: Jean Dupond
     * @bodyParam email string Email du contact. Example: jdupond@sfr.fr
     * @bodyParam position string Position du contact au sein du comité. Example: Comptable
     * @bodyParam telephone string Téléphone personnel du contact. Example: 06 00 00 00 00
     * @response 201 {
     *  "message": "Retourne les informations du contact"
     * }
     */

    public function update(Request $request, $comite_id, $contact_id)
    {
        $contact = Contact::find($contact_id);
        if($request->position){
            $contact->position = $request->position;
        }

        if($request->telephone){
            $contact->telephone = $request->telephone;
        }
        $contact->save();

        $user = User::find($contact->user_id);
        if($request->name){
            $user->name = $request->name;
        }

        if($request->email){
            $user->email = $request->email;
        }
        $user->save();

        return response()->json([
            'data' => $contact->toArray()
        ], 201);
    }

    /**
     * Delete Contact
     * [Supprime l'ensemble des informations d'un contact]
     * @authenticated
     * @response 201 {
     *     "null"
     * }
     */

    public function delete($comite_id, $contact_id)
    {
        Contact::find($contact_id)->delete();

        return response()->json([
            'data' => null
        ], 201);
    }
}
