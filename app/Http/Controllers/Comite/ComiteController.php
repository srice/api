<?php

namespace App\Http\Controllers\Comite;

use App\Http\Controllers\Controller;
use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\Notifications\ComiteCreateComite;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Stripe\Customer;
use Stripe\Stripe;

/**
 * Class ComiteController
 * @package App\Http\Controllers\Comite
 * @group Comite
 * @module Comite
 */
class ComiteController extends Controller
{
    /**
     * Create Comité
     * [Permet la création d'un comite, Contact Primaire, Compte Stripe et Utilisateur Primaire]
     *
     * @authenticated
     * @bodyParam name string required Le nom du comité
     * @bodyParam adresse string required L'adresse du comité
     * @bodyParam codePostal string required Code Postal du comité (5 Digit). Example: 44000
     * @bodyParam ville string required Ville du comité. Example: Nantes
     * @bodyParam tel string required Numéro de téléphone principal du comité. Example: 00 00 00 00 00
     * @bodyParam email string required Email principal du comité. Example: contact@comite.com
     *
     * @response 422 {
     *  "message": "Retourne un message d'erreur avec les erreurs de validations"
     * }
     *
     * @response 201 {
     *  "message": "Retourne les informations du comité créer"
     * }
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $password = Str::random(8);
        try {
            $request->validate([
                "name" => "required",
                "adresse" => "required",
                "codePostal" => "required",
                "ville" => "required",
                "tel" => "required",
                "email" => "required|email"
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                "data" => "Erreur de Validation"
            ], 422);
        }


        $customer = $this->createCustomer(
            $request->name,
            $request->email,
            $request->adresse,
            $request->codePostal,
            $request->ville,
            $request->tel
        );


        $comite = Comite::create([
            "name" => $request->name,
            "adresse" => $request->adresse,
            "codePostal" => $request->codePostal,
            "ville" => $request->ville,
            "tel" => $request->tel,
            "email" => $request->email,
            "customerId" => $customer->id
        ]);

        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($password)
        ]);

        $user->notify(new ComiteCreateComite($comite, $password));

        $contact = Contact::create([
            "comite_id" => $comite->id,
            "user_id" => $user->id,
            "position" => $request->position,
            "telephone" => $request->telephone
        ]);

        return response()->json([
            "data" => $comite->toArray()
        ], 201);
    }

    /**
     * Get Comité
     * [Permet l'affichage des informations usuel d'un comité]
     * @authenticated
     * @response 201 {
     *  "message": "Retourne les informations du comité"
     * }
     */
    public function get($comite_id)
    {
        $comite = Comite::find($comite_id)->load('contacts', 'payments');

        return response()->json([
            'data' => $comite->toArray()
        ], 201);
    }

    /**
     * Update Comité
     * [Permet de mettre à jour les informations du comité en liaison avec stripe]
     * @authenticated
     * @bodyParam name string Le nom du comité. Example: Comité de Test -> Comité de Test 1
     * @bodyParam adresse string L'adresse du comité
     * @bodyParam codePostal string Code Postal du comité (5 Digit). Example: 44000
     * @bodyParam ville string Ville du comité. Example: Nantes
     * @bodyParam tel string Numéro de téléphone principal du comité. Example: 00 00 00 00 00
     * @bodyParam email string Email principal du comité. Example: contact@comite.com
     * @response 201 {
     *  "message": "Retourne les informations du comité mise à jour"
     * }
     */
    public function update(Request $request, $comite_id)
    {
        // Mise à jour du comité
        $comite = Comite::find($comite_id);
        if ($request->name) {
            $comite->name = $request->name;
        }
        if ($request->adresse) {
            $comite->adresse = $request->adresse;
        }
        if ($request->codePostal) {
            $comite->codePostal = $request->codePostal;
        }
        if ($request->ville) {
            $comite->ville = $request->ville;
        }
        if ($request->tel) {
            $comite->tel = $request->tel;
        }
        if ($request->email) {
            $comite->email = $request->email;
        }
        $comite->save();
        $comite = Comite::find($comite_id);

        $customer = $this->updateCustomer($comite->customerId,
            $comite->name,
            $comite->email,
            $comite->adresse,
            $comite->codePostal,
            $comite->ville,
            $comite->tel
        );

        return response()->json([
            'data' => $comite->toArray()
        ], 201);
    }

    /**
     * Delete Comité
     * [Supprime l'ensemble des informations d'un comité]
     * @authenticated
     * @method "DELETE"
     * @response 201 {
     *  "message": "Texte: Comite Supprimer"
     * }
     */

    public function delete($comite_id)
    {
        $comite = Comite::find($comite_id);
        $comite->delete();

        return response()->json([
            'data' => "Comite Supprimer"
        ], 201);
    }

    public function createCustomer($name, $email, $adresse, $codePostal, $ville, $phone)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        return Customer::create([
            "name" => $name,
            "email" => $email,
            "address" => [
                "line1" => $adresse,
                "postal_code" => $codePostal,
                "city" => $ville
            ],
            "phone" => $phone
        ]);
    }

    public function updateCustomer($customer_id, $name, $email, $adresse, $codePostal, $ville, $phone)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        return Customer::update($customer_id, [
            "name" => $name,
            "email" => $email,
            "address" => [
                "line1" => $adresse,
                "postal_code" => $codePostal,
                "city" => $ville
            ],
            "phone" => $phone
        ]);
    }
}

