<?php

namespace App\Http\Controllers\Comite;

use App\Http\Controllers\Controller;
use App\Model\Comite\Comite;
use App\Model\Comite\Payment;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardExpirationMonth;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardNumber;
use Stripe\PaymentMethod;
use Stripe\SetupIntent;
use Stripe\Stripe;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Comite
 * @group Comite/Payment
 */
class PaymentController extends Controller
{
    /**
     * Create Payment
     * [Création du mode de paiement pour un comité particulier.<br>Un choix doit être fais entre carte bancaire ou IBAN]
     * @authenticated
     * @bodyParam methode string required Methode de paiement(card || sepa_debit).Example: card
     * @bodyParam card_number string (Valid) Numéro de la carte bancaire. Example: 4242 4242 4242 4242
     * @bodyParam exp_month int (Valid) Mois de validité de la carte bancaire. Example: 04
     * @bodyParam exp_year int (Valid) Année de validité de la carte bancaire. Example: 20
     * @bodyParam cvc int (Valid) Code de validité de la carte bancaire. Example: 123
     * @bodyParam iban string (Valid) Iban du compte bancaire. Example: FR17 1523 9652 9632 8990 1235 123
     *
     * @response 422 {
     *  "data": "Message Exception",
     *  "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     *  "payment": "Information relative au mode de paiement",
     *  "intent": "Intention STIPE du mode de paiement"
     * }
     *
     * @param Request $request
     * @param $comite_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $comite_id)
    {
        try {
            $request->validate([
                "methode" => "required",
                "card_number" => $request->card_number,
                "card_number" => new CardNumber(),
                "exp_month" => new CardExpirationMonth($request->exp_year),
                "exp_year" => new CardExpirationYear($request->exp_month),
                "cvc" => new CardCvc($request->card_number)
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                "data" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $comite = Comite::find($comite_id);

        if ($request->methode == 'card') {
            $pm = $this->createSetupPayment(
                $comite->customerId,
                $request->methode,
                $request->card_number,
                $request->exp_month,
                $request->exp_year,
                $request->cvc
            );
        } else {
            $pm = $this->createSetupPayment(
                $comite->customerId,
                $request->methode,
                null,
                null,
                null,
                null,
                $request->iban,
                $comite
            );
        }

        $payment = Payment::create([
            "comite_id" => $comite_id,
            "stripe_id" => $pm->id
        ]);

        return response()->json([
            'payment' => $payment->toArray(),
            'intent' => $pm->toArray()
        ], 201);
    }

    /**
     * Get Payment
     * [Affiche les informations relative au mode de paiement]
     * @authenticated
     *
     * @response 201 {
     *  "payment": "Information relative au mode de paiement",
     *  "intent": "Intention STIPE du mode de paiement"
     * }
     */
    public function get($comite_id, $payment_id)
    {
        $payment = Payment::find($payment_id)->load('comite');
        $intent = $this->retrievePayment($payment->stripe_id);

        return response()->json([
            'payment' => $payment->toArray(),
            'intent' => $intent->toArray()
        ], 201);
    }

    /**
     * Delete Payment
     * [Supprime le mode de paiement]
     *
     * @response 201 {
     *  "data": "null"
     * }
     */
    public function delete($comite_id, $payment_id)
    {
        $payment = Payment::find($payment_id)->delete();

        return response()->json([
            'data' => 'null'
        ], 201);
    }

    public function retrievePayment($stripe_id)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        return PaymentMethod::retrieve($stripe_id);
    }

    public function createSetupPayment($customer_id, $type, $card_number = null, $exp_month = null, $exp_year = null, $cvc = null, $iban = null, $comite = null)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        if ($type == 'card') {
            return PaymentMethod::create([
                "type" => 'card',
                "card" => [
                    "number" => $card_number,
                    "exp_month" => $exp_month,
                    "exp_year" => $exp_year,
                    "cvc" => $cvc
                ]
            ])->attach([
                "customer" => $customer_id
            ]);
        } else {
            return PaymentMethod::create([
                "type" => 'sepa_debit',
                "sepa_debit" => [
                    "iban" => $iban,
                ],
                "billing_details" => [
                    "name" => $comite->name,
                    "email" => $comite->email
                ]
            ])->attach([
                "customer" => $customer_id
            ]);
        }
    }
}
