<?php

namespace App\Http\Controllers\Espace;

use App\Http\Controllers\Controller;
use App\Model\Espace\EspaceLicence;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Class EspaceLicenceController
 * @package App\Http\Controllers\Espace
 * @group Espace/Licence
 */
class EspaceLicenceController extends Controller
{
    /**
     * @var EspaceLicence
     */
    private $espaceLicence;

    /**
     * EspaceLicenceController constructor.
     * @param EspaceLicence $espaceLicence
     */
    public function __construct(EspaceLicence $espaceLicence)
    {
        $this->espaceLicence = $espaceLicence;
    }

    /**
     * List Licence
     * Liste des licences affilier à l'espace
     * @authenticated
     *
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($espace_id)
    {
        $licences = $this->espaceLicence->newQuery()->where('espace_id', $espace_id)->get();

        return response()->json($licences->toArray(), 201);
    }

    /**
     * Create Licence
     * Création d'une licence affilier à l'espace
     * @authenticated
     * @bodyParam module bool required Définie sir la licence est affilier également à un module..Example: false
     * @bodyParam module_id int ID du module affilier à la licence.Example: 1
     * @bodyParam start date required Date de début de la licence.Example: 2020-04-23 14:00:00
     * @bodyParam end date required Date de fin de la licence.Example: 2021-04-23 14:00:00
     *
     * @param Request $request
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $espace_id)
    {
        try {
            $request->validate([
                "module" => "required",
                "start" => "required|date",
                "end" => "required|date|different:start"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $numLicence = Str::upper(Str::random(4)).'-'.Str::upper(Str::random(4)).'-'.Str::upper(Str::random(4)).'-'.now()->year;
        $licence = $this->espaceLicence->newQuery()->create([
            "espace_id" => $espace_id,
            "numero" => $numLicence,
            "module" => $request->module,
            "module_id" => $request->module_id,
            "start" => $request->start,
            "end" => $request->end
        ]);

        return response()->json($licence->toArray(), 201);
    }

    /**
     * Get Licence
     * Affiche les information d'une licence
     * @authenticated
     *
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($espace_id, $id)
    {
        $licence = $this->espaceLicence->newQuery()->find($id);

        return response()->json($licence->toArray(), 201);
    }

    /**
     * Update Licence
     * Mise à jour d'une licence spécifique
     * @authenticated
     * @bodyParam module bool Définie sir la licence est affilier également à un module..Example: false
     * @bodyParam module_id int ID du module affilier à la licence.Example: 1
     * @bodyParam start date Date de début de la licence.Example: 2020-04-23 14:00:00
     * @bodyParam end date Date de fin de la licence.Example: 2021-04-23 14:00:00
     *
     * @param Request $request
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $espace_id, $id)
    {
        $licence = $this->espaceLicence->newQuery()->find($id);
        if($request->exists('module') == true){$licence->module = $request->module;}
        if($request->exists('module_id') == true){$licence->module_id = $request->module_id;}
        if($request->exists('start') == true){$licence->start = $request->start;}
        if($request->exists('end') == true){$licence->end = $request->end;}
        $licence->save();

        return response()->json($licence->toArray(), 201);
    }

    /**
     * Delete Licence
     * Supprime une licence
     * @authenticated
     *
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($espace_id, $id)
    {
        $licence = $this->espaceLicence->newQuery()->find($id);
        $licence->delete();

        return response()->json($licence->toArray(), 201);
    }
}
