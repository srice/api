<?php

namespace App\Http\Controllers\Espace;

use App\Http\Controllers\Controller;
use App\Model\Espace\Espace;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class EspaceController
 * @package App\Http\Controllers\Espace
 * @group Espace
 */
class EspaceController extends Controller
{
    /**
     * @var Espace
     */
    private $espace;

    /**
     * EspaceController constructor.
     * @param Espace $espace
     */
    public function __construct(Espace $espace)
    {
        $this->espace = $espace;
    }

    /**
     * List Espace
     * Liste des espaces
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $espaces = $this->espace->newQuery()->get();

        return response()->json($espaces->toArray(), 201);
    }

    /**
     * Create Espace
     * Création de l'espace
     * @authenticated
     * @bodyParam comite_id int required ID du comité affilier à l'espace en construction. Example: 1
     * @bodyParam contrat_id int required ID du contrat affilier à l'espace en construction. Example: 1
     * @bodyParam domain string required Nom de domaine sans extension. Example: test
     * @bodyParam path string required Chemin de l'application. Example: test
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "comite_id" => "required",
                "contrat_id" => "required",
                "domain" => "required",
                "path" => "required",
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $espace = $this->espace->newQuery()->create([
            "comite_id" => $request->comite_id,
            "contrat_id" => $request->contrat_id,
            "domain" => $request->domain,
            "path" => $request->path
        ]);

        return response()->json($espace->toArray(), 201);
    }

    /**
     * Get Espace
     * Information d'un espace
     * @authenticated
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $espace = $this->espace->newQuery()->find($id);

        return response()->json($espace->toArray(), 201);
    }

    /**
     * Update Espace
     * Mise à jour de l'espace
     * @authenticated
     * @bodyParam comite_id int  ID du comité affilier à l'espace en construction. Example: 1
     * @bodyParam contrat_id int  ID du contrat affilier à l'espace en construction. Example: 1
     * @bodyParam domain string  Nom de domaine sans extension. Example: test
     * @bodyParam path string  Chemin de l'application. Example: test
     * @bodyParam state int  Etat actuel de l'application | 0: Hors Ligne <br> 1: Maintenance <br> 2: En Ligne <br> 3: n Maintenance <br> 4: Expirer. Example: 0
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $espace = $this->espace->newQuery()->find($id);
        $espace->comite_id = $request->comite_id;
        $espace->contrat_id = $request->contrat_id;
        $espace->domain = $request->domain;
        $espace->path = $request->path;
        $espace->save();

        return response()->json($espace->toArray(), 201);
    }

    /**
     * Delete Espace
     * Supprime un espace
     * @authenticated
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $espace = $this->espace->newQuery()->find($id);
        $espace->delete();

        return response()->json($espace->toArray(), 201);
    }
}
