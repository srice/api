<?php

namespace App\Http\Controllers\Espace;

use App\Http\Controllers\Controller;
use App\Model\Espace\EspaceService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class EspaceServiceController
 * @package App\Http\Controllers\Espace
 * @group Espace/Service
 */
class EspaceServiceController extends Controller
{
    /**
     * @var EspaceService
     */
    private $espaceService;

    /**
     * EspaceServiceController constructor.
     * @param EspaceService $espaceService
     */
    public function __construct(EspaceService $espaceService)
    {
        $this->espaceService = $espaceService;
    }

    /**
     * List Service
     * Liste les services d'un espace
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($espace_id)
    {
        $service = $this->espaceService->newQuery()->where('espace_id', $espace_id)->get();

        return response()->json($service->toArray(), 201);
    }

    /**
     * Create Service
     * Création d'un service affilier à un espace
     * @authenticated
     * @bodyParam server_id int required ID du serveur ou est installer l'application. Example: 1
     * @bodyParam database string required Nom de la base de donnée. Example: test
     *
     * @param \Illuminate\Http\Request $request
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $espace_id)
    {
        try {
            $request->validate([
                "server_id" => "required",
                "database" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $service = $this->espaceService->newQuery()->create([
            "espace_id" => $espace_id,
            "server_id" => $request->server_id,
            "database" => $request->database
        ]);

        return response()->json($service->toArray(), 201);
    }

    /**
     * Get Service
     * Affiche les information d'un service
     * @authenticated
     *
     * @param $espace_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($espace_id, $id)
    {
        $service = $this->espaceService->newQuery()->find($id);

        return response()->json($service->toArray(), 201);
    }

    /**
     * Update Service
     * Met à jour les information relative à un service
     * @authenticated
     * @bodyParam server_id int ID du serveur ou est installer l'application. Example: 1
     * @bodyParam database string Nom de la base de donnée. Example: test
     *
     * @param \Illuminate\Http\Request $request
     * @param $espace_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $espace_id, $id)
    {
        $service = $this->espaceService->newQuery()->find($id);
        if($request->exists('server_id') == true) {
            $service->server_id = $request->server_id;
        }
        if($request->exists('database') == true) {
            $service->database = $request->database;
        }
        $service->save();

        return response()->json($service->toArray(), 201);
    }

    /**
     * Delete Service
     * Supprime un service
     * @authenticated
     *
     * @param $espace_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($espace_id, $id)
    {
        $service = $this->espaceService->newQuery()->find($id);
        $service->delete();

        return response()->json($service->toArray(), 201);
    }
}
