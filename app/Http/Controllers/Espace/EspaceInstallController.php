<?php

namespace App\Http\Controllers\Espace;

use App\Http\Controllers\Controller;
use App\Model\Espace\EspaceInstall;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class EspaceInstallController
 * @package App\Http\Controllers\Espace
 * @group Espace/Install
 */
class EspaceInstallController extends Controller
{
    /**
     * @var EspaceInstall
     */
    private $espaceInstall;

    /**
     * EspaceInstallController constructor.
     * @param EspaceInstall $espaceInstall
     */
    public function __construct(EspaceInstall $espaceInstall)
    {
        $this->espaceInstall = $espaceInstall;
    }

    /**
     * List Install
     * Liste des étapes d'installation de l'espace
     * @authenticated
     *
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($espace_id)
    {
        $installs = $this->espaceInstall->newQuery()->where('espace_id', $espace_id)->get();

        return response()->json($installs->toArray(), 201);
    }

    /**
     * Create Install
     * Création des étapes d'installation d'un espace
     * @authenticated
     * @bodyParam description string required Description de l'étape.Example: Déclaration du domaine
     *
     * @param Request $request
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $espace_id)
    {
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Initialisation de la connexion"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Déclaration du domaine"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Création de la configuration serveur"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Mise en place de la configuration serveur (Apache, SSL)"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Démarrage de la configuration serveur"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Initialisation de l'application (git)"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Initialisation de l'application (composer)"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Configuration de l'application"]);
        $this->espaceInstall->newQuery()->create(["espace_id" => $espace_id, "description" => "Nettoyage de l'installation"]);

        return response()->json(null, 201);
    }

    /**
     * Get Install
     * Information sur une étape
     * @authenticated
     *
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($espace_id, $id)
    {
        $install = $this->espaceInstall->newQuery()->find($id);

        return response()->json($install->toArray(), 201);
    }

    /**
     * Update Install
     * Mise à jour d'une étape de l'installation
     * @authenticated
     * @bodyParam description string required Description de l'étape.Example: Déclaration du domaine
     *
     * @param Request $request
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $espace_id, $id)
    {
        $install = $this->espaceInstall->newQuery()->find($id);
        if($request->exists('time') == true){$install->time = $request->time;}
        if($request->exists('description') == true){$install->description = $request->description;}
        if($request->exists('state') == true){$install->state = $request->state;}
        if($request->exists('errorLog') == true){$install->errorLog = $request->errorLog;}
        $install->save();

        return response()->json($install->toArray(), 201);
    }

    /**
     * Delete Install
     * Supprime une étape de l'installation
     * @authenticated
     *
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($espace_id, $id)
    {
        $install = $this->espaceInstall->newQuery()->find($id);
        $install->delete();

        return response()->json($install->toArray(), 201);
    }
}
