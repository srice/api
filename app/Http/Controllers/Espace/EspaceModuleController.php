<?php

namespace App\Http\Controllers\Espace;

use App\Http\Controllers\Controller;
use App\Model\Espace\EspaceModule;
use App\Model\Prestation\Module;
use Illuminate\Http\Request;

/**
 * Class EspaceModuleController
 * @package App\Http\Controllers\Espace
 * @group Espace/Module
 */
class EspaceModuleController extends Controller
{
    /**
     * @var EspaceModule
     */
    private $espaceModule;
    /**
     * @var Module
     */
    private $module;

    /**
     * EspaceModuleController constructor.
     * @param EspaceModule $espaceModule
     * @param Module $module
     */
    public function __construct(EspaceModule $espaceModule, Module $module)
    {
        $this->espaceModule = $espaceModule;
        $this->module = $module;
    }

    /**
     * List Module
     * Liste des module affilier à l'espace
     * @authenticated
     *
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($espace_id)
    {
        $modules = $this->espaceModule->newQuery()->where('espace_id', $espace_id)->get();

        return response()->json($modules->toArray(), 201);
    }

    /**
     * Create Module
     * Création de la liste des modules affilier à l'espace
     * @authenticated
     *
     * @param Request $request
     * @param $espace_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $espace_id)
    {
        $modules = $this->module->newQuery()->get();
        foreach ($modules as $module) {
            $this->espaceModule->newQuery()->create([
                "espace_id" => $espace_id,
                "module_id" => $module->id,
                "state" => 0,
                "checkout" => 0
            ]);
        }

        return response()->json($this->espaceModule->newQuery()->where('espace_id', $espace_id)->get()->toArray(), 201);
    }

    /**
     * Get Module
     * Affiche les information relative à un module
     * @authenticated
     *
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($espace_id, $id)
    {
        $module = $this->espaceModule->newQuery()->find($id);

        return response()->json($module->toArray(), 201);
    }

    /**
     * Update Module
     * Mise à jour des informations d'un module
     * @authenticated
     *
     * @param Request $request
     * @param $espace_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $espace_id, $id)
    {
        $module = $this->espaceModule->newQuery()->find($id);
        if($request->exists('module_id') == true) {$module->module_id = $request->module_id;}
        if($request->exists('state') == true) {$module->state = $request->state;}
        if($request->exists('checkout') == true) {$module->checkout = $request->checkout;}
        $module->save();

        return response()->json($module->toArray(), 201);
    }

    /**
     * Delete Module
     * Supprime un module de l'espace
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $module = $this->espaceModule->newQuery()->find($id);
        $module->delete();

        return response()->json($module->toArray(), 201);
    }
}
