<?php

namespace App\Http\Controllers\Infrastructure\Server;

use App\Http\Controllers\Controller;
use App\Model\Infrastructure\Server\ServerRegistar;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ServerRegistarController
 * @package App\Http\Controllers\Infrastructure\Server
 * @group Infrastructure/Registar
 */
class ServerRegistarController extends Controller
{
    /**
     * @var ServerRegistar
     */
    private $serverRegistar;

    /**
     * ServerRegistarController constructor.
     * @param ServerRegistar $serverRegistar
     */
    public function __construct(ServerRegistar $serverRegistar)
    {
        $this->serverRegistar = $serverRegistar;
    }

    /**
     * List Registar
     * Liste des Fournisseurs d'accès (Registar)
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $registars = $this->serverRegistar->newQuery()->get();

        return response()->json($registars->toArray(), 201);
    }

    /**
     * Store Registar
     * Création d'un fournisseur (Registar)
     * @authenticated
     * @bodyParam name string required Nom du fournisseur. Example: OVH
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $registar = $this->serverRegistar->newQuery()->create([
            "name" => $request->name
        ]);

        return response()->json($registar->toArray(), 201);
    }

    /**
     * Get Registar
     * Information d'un fournisseur (Registar)
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $registar = $this->serverRegistar->newQuery()->find($id);

        return response()->json($registar->toArray(), 201);
    }

    /**
     * Update Registar
     * Met à jour les informations d'un fournisseur (Registar)
     * @authenticated
     * @bodyParam name string required Nom du fournisseur. Example: OVH
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $registar = $this->serverRegistar->newQuery()->find($id);
        if($request->exists('name') == true){$registar->name = $request->name;}
        $registar->save();

        return response()->json($registar->toArray(), 201);
    }

    /**
     * Delete Registar
     * Supprime le fournisseur (Registar)
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $registar = $this->serverRegistar->newQuery()->find($id);
        $registar->delete();

        return response()->json($registar->toArray(), 201);
    }
}
