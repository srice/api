<?php

namespace App\Http\Controllers\Infrastructure\Server;

use App\Http\Controllers\Controller;
use App\Model\Infrastructure\Server\Server;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ServerController
 * @package App\Http\Controllers\Infrastructure\Server
 * @group Infrastructure/Server
 */
class ServerController extends Controller
{
    /**
     * @var Server
     */
    private $server;

    /**
     * ServerController constructor.
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * List Server
     * Liste des Serveurs
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $servers = $this->server->newQuery()->get();

        return response()->json($servers->toArray(), 201);
    }

    /**
     * Create Server
     * Création d'un serveur
     * @authenticated
     * @bodyParam registar_id int required ID du registar supportant le serveur. Example: 1
     * @bodyParam name string required Nom du serveur. Example: ns225366.ip-ovh.org
     * @bodyParam ip string required Adresse IP du serveur. Example: 215.36.220.120
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "registar_id" => "required",
                "name" => "required",
                "ip" => "required|ip",
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $server = $this->server->newQuery()->create([
            "registar_id" => $request->registar_id,
            "name" => $request->name,
            "ip" => $request->ip
        ]);

        return response()->json($server->toArray(), 201);
    }

    /**
     * Get Server
     * Affiche les information du serveur
     * @authenticated
     *
     * @param $server_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($server_id)
    {
        return response()->json($this->server->newQuery()->find($server_id)->toArray(), 201);
    }

    /**
     * Update Server
     * Met à jour les information d'un serveur
     * @authenticated
     * @bodyParam registar_id int required ID du registar supportant le serveur. Example: 1
     * @bodyParam name string required Nom du serveur. Example: ns225366.ip-ovh.org
     * @bodyParam ip string required Adresse IP du serveur. Example: 215.36.220.120
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $server = $this->server->newQuery()->find($id);
        if($request->exists('registar_id') == true){$server->registar_id = $request->registar_id;}
        if($request->exists('name') == true){$server->name = $request->name;}
        if($request->exists('ip') == true){$server->ip = $request->ip;}
        if($request->exists('connect') == true){$server->connect = $request->connect;}
        $server->save();

        return response()->json($server->toArray(), 201);
    }

    /**
     * Delete Server
     * Supprime un serveur
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $server = $this->server->newQuery()->find($id);
        $server->delete();

        return response()->json($server->toArray(), 201);
    }
}
