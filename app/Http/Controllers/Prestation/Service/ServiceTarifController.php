<?php

namespace App\Http\Controllers\Prestation\Service;

use App\Http\Controllers\Controller;
use App\Model\Prestation\ServiceTarif;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Tarifications des Services
 * Class ServiceTarifController
 * @package App\Http\Controllers\Prestation\Service
 * @group Prestation/Service/Tarif
 */
class ServiceTarifController extends Controller
{
    /**
     * @var ServiceTarif
     */
    private $serviceTarif;

    /**
     * ServiceTarifController constructor.
     * @param ServiceTarif $serviceTarif
     */
    public function __construct(ServiceTarif $serviceTarif)
    {
        $this->serviceTarif = $serviceTarif;
    }

    /**
     * List Tarifs
     * Liste des tarifs par service
     * @response 404 {
     * "message": "Aucun Tarif pour ce service"
     * }
     *
     * @response 201 {
     * "tarifs": "Liste des tarif par service"
     * }
     * @param $service_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($service_id)
    {
        $tarifs = $this->serviceTarif->newQuery()->where('service_id', $service_id)->get();
        if ($tarifs->isEmpty() == true) {
            return response()->json([
                "Aucun Tarif"
            ], 404);
        } else {
            return response()->json([
                "services" => $tarifs
            ], 201);
        }
    }

    /**
     * Create Tarif
     * Création d'un tarif
     * @bodyParam name string required Nom du tarif. Example: Tarif Unique
     * @bodyParam amount string required Montant du tarif. Example: 12.00
     * @response 422 {
     * "exception": "Exception de Validation",
     * "errors": "Erreur de Validation"
     * }
     *
     * @response 201 {
     * "tarif": "Tarif Créer"
     * }
     * @param Request $request
     * @param $service_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $service_id)
    {
        try {
            $request->validate([
                "name" => "required",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $tarif = $this->serviceTarif->newQuery()->create([
            "service_id" => $service_id,
            "name" => $request->name,
            "amount" => $request->amount
        ])->get()->load('service');

        return response()->json([
            "tarif" => $tarif->toArray()
        ], 201);
    }

    /**
     * Get Tarif
     * Information d'un tarif
     *
     * @response 201 {
     * "tarif": "Information d'un tarif"
     * }
     * @param $service_id
     * @param $tarif_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function get($service_id, $tarif_id)
    {
        $service = $this->serviceTarif->newQuery()->find($tarif_id)->load('service');

        return response()->json([
            "tarif" => $service->toArray()
        ], 201);
    }

    /**
     * Update Tarif
     * Mise à jour d'un tarif
     * @bodyParam name string Nom du tarif. Example: Tarif Unique
     * @bodyParam amount string Montant du tarif. Example: 13.00
     *
     * @response 422 {
     * "exception": "Exception de Validation",
     * "errors": "Erreur de Validation"
     * }
     *
     * @response 201 {
     * "tarif": "Tarif Mise à jour"
     * }
     * @param Request $request
     * @param $service_id
     * @param $tarif_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(Request $request, $service_id, $tarif_id)
    {
        try {
            $request->validate([
                "name" => "required",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $tarif = $this->serviceTarif->newQuery()->find($tarif_id);
        $tarif->name = $request->name;
        $tarif->amount = $request->amount;
        $tarif->save();

        return response()->json([
            "tarif" => $tarif->load('service')->toArray()
        ], 201);

    }


    /**
     * Delete Tarif
     * Suppression d'un tarif
     *
     * @response 201 {
     * "tarif": "Tarif Supprimer"
     * }
     * @param $service_id
     * @param $tarif_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($service_id, $tarif_id)
    {
        $tarif = $this->serviceTarif->newQuery()->find($tarif_id);
        $tarif->delete();

        return response()->json([
            "tarif" => $tarif->load('service')->toArray()
        ], 201);
    }


}
