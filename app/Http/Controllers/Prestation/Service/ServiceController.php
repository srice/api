<?php

namespace App\Http\Controllers\Prestation\Service;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ServiceController
 * @package App\Http\Controllers\Prestation\Service
 * @group Prestation/Service
 */
class ServiceController extends Controller
{
    /**
     * List Service
     * Liste des Services
     * @authenticated
     * @response 201 {
     * "services": "Liste des services"
     * }
     */
    public function list()
    {
        $services = Service::all();

        return response()->json([
            "services" => $services->toArray()
        ], 201);
    }

    /**
     * Create Service
     * Création d'un service
     * @authenticated
     * @bodyParam famille_id int required ID de la famille associé au service.Example: 1
     * @bodyParam name string required Désignation du service.example: Accès test au logiciel Srice
     * @bodyParam kernel int required Liste de Choix (0: Aucun |1: création d'espace |2: Activation d'un module).example: 0
     * @bodyParam module_id int Si le kernel est à 2, module à activé.
     * @response 422 {
     * "exception": "Message d'exception",
     * "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     * "service": "Service créer"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "famille_id" => "required",
                "name" => "required",
                "kernel" => "required"
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $service = Service::create([
            "famille_id" => $request->famille_id,
            "name" => $request->name,
            "kernel" => $request->kernel,
            "module_id" => $request->module_id
        ]);

        return response()->json([
            "service" => $service->toArray()
        ], 201);
    }

    /**
     * Get Service
     * Information sur un service précis
     * @authenticated
     * @response 201 {
     * "service": "Information sur le service"
     * }
     * @param $service_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function get($service_id)
    {
        $service = Service::find($service_id);

        return response()->json([
            "service" => $service->toArray()
        ], 201);
    }

    /**
     * Update Service
     * Mise à jour d'un service
     * @authenticated
     * @bodyParam famille_id int ID de la famille associé au service.Example: 1
     * @bodyParam name string Désignation du service.example: Accès test au logiciel Srice
     * @bodyParam kernel int Liste de Choix (0: Aucun |1: création d'espace |2: Activation d'un module).example: 0
     * @bodyParam module_id int Si le kernel est à 2, module à activé.
     * @response 201 {
     * "service": "Information sur le service mis à jour"
     * }
     * @param Request $request
     * @param $service_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $service_id)
    {
        $service = Service::find($service_id);
        if ($request->famille_id) {
            $service->famille_id = $request->famille_id;
        }
        if ($request->name) {
            $service->name = $request->name;
        }
        if ($request->kernel) {
            $service->kernel = $request->kernel;
        }
        if ($request->module_id) {
            $service->module_id = $request->module_id;
        }
        $service->save();

        return response()->json([
            "service" => $service->toArray()
        ], 201);
    }

    /**
     * Delete Service
     * Supprime un service
     * @authenticated
     * @response 201 {
     * "service": "Service supprimer"
     * }
     * @param $service_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($service_id)
    {
        $service = Service::find($service_id);
        $service->delete();

        return response()->json([
            "service" => $service->toArray()
        ], 201);
    }
}
