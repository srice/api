<?php

namespace App\Http\Controllers\Prestation\Famille;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class FamilleController
 * @package App\Http\Controllers\Prestation\Famille
 * @group Prestation/Famille
 */
class FamilleController extends Controller
{
    /**
     * List Famille
     * [Liste des familles de services]
     * @authenticated
     * @response 201 {
     *  "data": "Liste des Familles de service"
     * }
     */
    public function list()
    {
        $famille = Famille::all()->load('services');

        return response()->json([
            "data" => $famille->toArray()
        ], 201);
    }

    /**
     * Create Famille
     * [Création d'une famille de service]
     *
     * @authenticated
     * @bodyParam name string required Nom de la famille. Example: Application
     *
     * @response 422 {
     *  "data": "Exception",
     *  "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     *  "famille": "Famille créer"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "data" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $famille = Famille::create([
            "name" => $request->name
        ]);

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Get Famille
     * [Affiche les informations d'une famille]
     * @authenticated
     *
     * @response 201 {
     * "famille": "Information de la famille (Array)"
     * }
     */
    public function get($famille_id)
    {
        $famille = Famille::find($famille_id)->load('services');

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Update Famille
     * [Met à jour la famille]
     * @authenticated
     * @bodyParam name string required Nom de la famille.Example: Application
     *
     * @response 422 {
     *  "data": "Exception",
     *  "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     *  "famille": "Famille mise à jour"
     * }
     */
    public function update(Request $request, $famille_id)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "data" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $famille = Famille::find($famille_id);
        $famille->name = $request->name;
        $famille->save();

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Delete Famille
     * Supprime une famille de service
     * @authenticated
     * @response 201 {
     * "famille": "Famille Supprimer"
     * }
     *
     */
    public function delete($famille_id)
    {
        $famille = Famille::find($famille_id);
        $famille->delete();

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Get Services
     * Liste des services d'une famille
     * @response 201 {
     * "services": "Liste des services de la famille ID"
     * }
     * @param $famille_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function services($famille_id)
    {
        $services = Service::where('famille_id', $famille_id)->get();

        return response()->json([
            "services" => $services->toArray()
        ], 201);
    }
}
