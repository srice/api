<?php

namespace App\Http\Controllers\Prestation\Module;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Module;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ModuleController
 * @package App\Http\Controllers\Prestation\Module
 * @group Prestation/Module
 */
class ModuleController extends Controller
{
    /**
     * @var Module
     */
    private $module;

    /**
     * ModuleController constructor.
     * @param Module $module
     */
    public function __construct(Module $module)
    {
        $this->module = $module;
    }

    /**
     * List Module
     * Les modules doivent être lister pour pouvoir être accessible par l'ensemble des infrastructures de l'API (Organe Moteur)
     * @authenticated
     * @response 201 {
     * "modules": "Liste des Modules"
     * }
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $modules = $this->module->newQuery()->get();

        return response()->json([
            "modules" => $modules->toArray()
        ], 201);
    }

    /**
     * Create Module
     * Création d'un module
     * @authenticated
     * @bodyParam name string required Nom du module. Example: ANCV
     * @bodyParam description text Description du module. Example: Ce module permet la gestion et la comptabilité des Chèques Vacance ANCV
     * @bodyParam version string required Version du module. Example: 0.0.1
     * @bodyParam release int Etat de développement du module. Example: 0
     * @response 422 {
     * "exception": "Message exception de validation",
     * "errors: "Liste des erreurs de validation"
     * }
     * @response 201 {
     * "module": "Module Crée"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required",
                "version" => "required"
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $module = $this->module->newQuery()->create([
            "name" => $request->name,
            "description" => $request->description,
            "version" => $request->version,
            "release" => $request->release
        ]);

        return response()->json([
            "module" => $module->toArray()
        ], 201);
    }

    /**
     * Get Module
     * Information d'un module
     * @authenticated
     * @response 201 {
     * "module": "Module"
     * }
     * @param $module_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($module_id)
    {
        $module = $this->module->newQuery()->find($module_id)->load('services', 'changelogs', 'tasks');

        return response()->json([
            "module" => $module->toArray()
        ], 201);
    }

    /**
     * Update Module
     * Mise à jour d'un module
     * @authenticated
     * @bodyParam name string required Nom du module. Example: ANCV
     * @bodyParam description text Description du module. Example: Ce module permet la gestion et la comptabilité des Chèques Vacance ANCV
     * @bodyParam version string required Version du module. Example: 0.0.1
     * @bodyParam release int Etat de développement du module. Example: 0
     * @response 201 {
     * "module": "Module mis à jour"
     * }
     * @param Request $request
     * @param $module_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $module_id)
    {
        $module = $this->module->newQuery()->find($module_id);
        $module->name = $request->name;
        $module->description = $request->description;
        $module->version = $request->version;
        $module->release = $request->release;
        $module->save();

        return response()->json([
            "module" => $module->toArray()
        ], 201);
    }

    /**
     * Delete Module
     * Suppression d'un module
     * @authenticated
     * @response 201 {
     * "module": "Module supprimée"
     * }
     * @param $module_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($module_id)
    {
        $module = $this->module->newQuery()->find($module_id);
        $module->delete();

        return response()->json([
            "module" => $module->toArray()
        ], 201);
    }
}
