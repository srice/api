<?php

namespace App\Http\Controllers\Prestation\Module;

use App\Http\Controllers\Controller;
use App\Model\Prestation\ModuleChangelog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * Note de Mise à jour de module
 * Class ModuleChangelogController
 * @package App\Http\Controllers\Prestation\Module
 * @group Prestation/Module/Changelog
 */
class ModuleChangelogController extends Controller
{
    /**
     * @var ModuleChangelog
     */
    private $changelog;

    /**
     * ModuleChangelogController constructor.
     * @param ModuleChangelog $changelog
     */
    public function __construct(ModuleChangelog $changelog)
    {
        $this->changelog = $changelog;
    }

    /**
     * List Changelog
     * Affiche la liste des mise à jours sur un module
     * @authenticated
     *
     * @response 201 {
     * "changelogs": "Liste des Changelogs"
     * }
     *
     * @param $module_id
     * @return JsonResponse
     */
    public function list($module_id)
    {
        $changelogs = $this->changelog->newQuery()->where('module_id', $module_id)->get();

        return response()->json([
            "changelogs" => $changelogs->toArray()
        ], 201);
    }

    /**
     * Create Changelog
     * Création d'une note
     * @authenticated
     * @bodyParam version required string Version de la mise à jours. Example: 0.0.1
     * @bodyParam description required string Description de la mise à jours. Example: Description de la mise à jours
     * @bodyParam date required date Date de la mise à jours. Example: 2020-04-18 08:00:00
     * @bodyParam state int Etat de la mise à jours. Example: 0
     *
     * @response 422 {
     * "exception": "Message exception validation",
     * "errors": "Liste des erreurs de validations"
     * }
     *
     * @response 201 {
     * "changelog": "Changelog Crée"
     * }
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request, $module_id)
    {
        try {
            $request->validate([
                "version" => "required|string",
                "description" => "required|string",
                "date" => "required|date",
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $changelog = $this->changelog->newQuery()->create([
            "module_id" => $module_id,
            "version" => $request->version,
            "description" => $request->description,
            "date" => $request->date,
            "state" => $request->state
        ]);

        return response()->json([
            "changelog" => $changelog->toArray()
        ], 201);
    }

    /**
     * Get Changelog
     * Note de Mise à jour
     * @authenticated
     * @response 201 {
     * "changelog": "Information sur une mise à jour"
     * }
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get($module_id, $id)
    {
        $changelog = $this->changelog->newQuery()->find($id);

        return response()->json([
            "changelog" => $changelog->toArray()
        ], 201);
    }

    /**
     * Update Changelog
     * Mise à jour d'une note de mise à jour
     *
     * @authenticated
     * @bodyParam version string Version de la mise à jours. Example: 0.0.1
     * @bodyParam description string Description de la mise à jours. Example: Description de la mise à jours
     * @bodyParam date date Date de la mise à jours. Example: 2020-04-18 08:00:00
     * @bodyParam state int Etat de la mise à jours. Example: 0
     *
     * @response 201 {
     * "changelog": "Changelog mise à jour"
     * }
     *
     * @param Request $request
     * @param $module_id
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $module_id, $id)
    {
        $changelog = $this->changelog->newQuery()->find($id);
        $changelog->version = $request->version;
        $changelog->description = $request->description;
        $changelog->date = $request->date;
        $changelog->state = $request->state;
        $changelog->save();

        return \response()->json([
            "changelog" => $changelog->toArray()
        ], 201);
    }

    /**
     * Delete Changelog
     * Supprime une note de mise à jour
     * @authenticated
     *
     * @response 201 {
     * "changelog": "Changelog supprimée"
     * }
     *
     * @param $module_id
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete($module_id, $id)
    {
        $changelog = $this->changelog->newQuery()->find($id);
        $changelog->delete();

        return response()->json([
            "changelog" => $changelog->toArray()
        ], 201);
    }
}
