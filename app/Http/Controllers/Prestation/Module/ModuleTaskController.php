<?php

namespace App\Http\Controllers\Prestation\Module;

use App\Http\Controllers\Controller;
use App\Model\Prestation\ModuleTask;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Module Task
 * Class ModuleTaskController
 * @package App\Http\Controllers\Prestation\Module
 * @group Prestation/Module/Task
 */
class ModuleTaskController extends Controller
{
    /**
     * @var ModuleTask
     */
    private $moduleTask;

    /**
     * ModuleTaskController constructor.
     * @param ModuleTask $moduleTask
     */
    public function __construct(ModuleTask $moduleTask)
    {
        $this->moduleTask = $moduleTask;
    }

    /**
     * List Taches
     * Tache d'un module
     * @authenticated
     * @response 201 {
     * "tasks": "Liste des taches"
     * }
     *
     * @param $module_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($module_id)
    {
        $tasks = $this->moduleTask->newQuery()->where('module_id', $module_id)->get();

        return response()->json([
            "tasks" => $tasks->toArray()
        ], 201);
    }

    /**
     * Create Task
     * Création d'une tache
     * @authenticated
     * @bodyParam subject string required Sujet de la tache.Example: Restructration du module
     * @bodyParam description string required Description de la tache.Example: Il s'agit de refactoriser le système du module avec la norme PSR-15, PSR-16, PSR-22
     * @bodyParam start date required Date de début de la tache.Example: 2020-04-17 18:00:00
     * @bodyParam end date required Date de fin de la tache.Example: 2020-04-23 00:00:00
     * @bodyParam state int Etat de la tache.Default: 0.Example: 0
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $module_id)
    {
        try {
            $request->validate([
                "subject" => "required|string",
                "description" => "required|string",
                "start" => "required|date",
                "end" => "required|date|different:start",
            ]);
        }catch (ValidationException $exception) {
            return  response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $task = $this->moduleTask->newQuery()->create([
            "module_id" => $module_id,
            "subject" => $request->subject,
            "description" => $request->description,
            "start" => $request->start,
            "end" => $request->end,
            "state" => $request->state
        ]);

        return  response()->json([
            "task" => $task->toArray()
        ], 201);
    }

    /**
     * Get Task
     * Information d'une tache particulière
     * @authenticated
     * @response 201 {
     * "task": "Information de la tache"
     * }
     *
     * @param $module_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($module_id, $id)
    {
        $task = $this->moduleTask->newQuery()->find($id);

        return response()->json([
            "task" => $task->toArray()
        ], 201);
    }

    /**
     * Update Tache
     * Mise à jour d'une tache
     * @authenticated
     * @bodyParam subject string  Sujet de la tache.Example: Restructration du module
     * @bodyParam description string  Description de la tache.Example: Il s'agit de refactoriser le système du module avec la norme PSR-15, PSR-16, PSR-22
     * @bodyParam start date  Date de début de la tache.Example: 2020-04-17 18:00:00
     * @bodyParam end date  Date de fin de la tache.Example: 2020-04-23 00:00:00
     * @bodyParam state int Etat de la tache.Default: 0.Example: 0
     *
     * @response 201 {
     * "task": "Tache mise à jour"
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $module_id, $id)
    {
        $task = $this->moduleTask->newQuery()->find($id);
        $task->subject = $request->subject;
        $task->description = $request->description;
        $task->start = $request->start;
        $task->end = $request->end;
        $task->state = $request->state;
        $task->save();

        return response()->json([
            "task" => $task->toArray()
        ], 201);
    }

    /**
     * Delete Tache
     * Supprime une tache d'un module
     * @authenticated
     *
     * @response 201 {
     * "task": "Tache Supprimer"
     * }
     *
     * @param $module_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($module_id, $id)
    {
        $task = $this->moduleTask->newQuery()->find($id);
        $task->delete();

        return  response()->json([
            "task" => $task->toArray()
        ], 201);
    }
}
