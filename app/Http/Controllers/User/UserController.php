<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 * @package App\Http\Controllers\User
 * @group Utilisateur
 */
class UserController extends Controller
{
    /**
     * Information utilisateur
     * [Affiche les informations relative à l'utilisateur connecter]
     * @authenticated
     *
     * @response {
     *  "data": "Information de l'utilisateur"
     * }
     * @response 401 {
     *  "data": "Non Connecter"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return response()->json([
            "data" => Auth::guard('api')->user()
        ]);
    }
}
