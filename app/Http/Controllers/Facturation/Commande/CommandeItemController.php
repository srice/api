<?php

namespace App\Http\Controllers\Facturation\Commande;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Commande\CommandeItem;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class CommandeItemController
 * @package App\Http\Controllers\Facturation\Commande
 * @group Facturation/Commande/Item
 */
class CommandeItemController extends Controller
{
    /**
     * @var CommandeItem
     */
    private $commandeItem;

    /**
     * CommandeItemController constructor.
     * @param CommandeItem $commandeItem
     */
    public function __construct(CommandeItem $commandeItem)
    {
        $this->commandeItem = $commandeItem;
    }

    /**
     * List Commande Items
     * Liste des articles d'une commande
     * @authenticated
     *
     * @response 201 {
     * "items": "liste des articles d'une commande"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($commande_id)
    {
        $items = $this->commandeItem->newQuery()->where('commande_id', $commande_id)->get()->load('commande');

        return response()->json([
            "items" => $items->toArray()
        ], 201);
    }

    /**
     * Add Item
     * Ajoute un article à la commande
     * @authenticated
     * @bodyParam service_id int required Identifiant du service.Example:1
     * @bodyParam service_tarif_id int required Identifiant du tarif du service.Example:1
     * @bodyParam description string Description supplémentaire du service.Example: Lorem
     * @bodyParam quantite int required Quantité de l'article.Example: 1
     * @bodyParam amount int required Montant total de l'article.Example: 12.00
     *
     * @response 422 {
     * "exception": "Message d'exception de validation",
     * "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     * "item": "Article crée"
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $commande_id)
    {
        try {
            $request->validate([
                "service_id" => "required",
                "service_tarif_id" => "required",
                "quantite" => "required|integer",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $item = $this->commandeItem->newQuery()->create([
            "commande_id" => $commande_id,
            "service_id" => $request->service_id,
            "service_tarif_id" => $request->service_tarif_id,
            "description" => $request->description,
            "quantite" => $request->quantite,
            "amount" => $request->amount
        ]);

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Get Item
     * Information d'un article
     * @authenticated
     *
     * @response 201 {
     * "item": "Information d'un article"
     * }
     *
     * @param $commande_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($commande_id, $id)
    {
        $item = $this->commandeItem->newQuery()->find($id);

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Update Item
     * Mise à jour d'un article de la commande
     *
     * @authenticated
     * @bodyParam service_id int  Identifiant du service.Example:1
     * @bodyParam service_tarif_id int  Identifiant du tarif du service.Example:1
     * @bodyParam description string Description supplémentaire du service.Example: Lorem
     * @bodyParam quantite int  Quantité de l'article.Example: 1
     * @bodyParam amount int  Montant total de l'article.Example: 12.00
     *
     * @response 201 {
     * "item": "Information d'un article"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @param $commande_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $commande_id, $id)
    {
        $item = $this->commandeItem->newQuery()->find($id);
        $item->service_id = $request->service_id;
        $item->service_tarif_id = $request->service_tarif_id;
        $item->description = $request->description;
        $item->quantite = $request->quantite;
        $item->amount = $request->amount;
        $item->save();

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Delete Item
     * Supprime un article d'une commande
     * @authenticated
     *
     * @response 201 {
     * "item": "Article supprimée"
     * }
     *
     * @param $commande_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($commande_id, $id)
    {
        $item = $this->commandeItem->newQuery()->find($id);
        $item->delete();

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }
}
