<?php

namespace App\Http\Controllers\Facturation\Commande;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Commande\CommandePayment;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class CommandePaymentController
 * @package App\Http\Controllers\Facturation\Commande
 * @group Facturation/Commande/Payment
 */
class CommandePaymentController extends Controller
{
    /**
     * @var CommandePayment
     */
    private $commandePayment;

    /**
     * CommandePaymentController constructor.
     * @param CommandePayment $commandePayment
     */
    public function __construct(CommandePayment $commandePayment)
    {
        $this->commandePayment = $commandePayment;
    }

    /**
     * List Commande Payment
     * Liste des paiements par commande
     * @authenticated
     *
     * @response 201 {
     * "payments": "Liste des Payments"
     * }
     *
     * @param $commande_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($commande_id)
    {
        $payment = $this->commandePayment->newQuery()->where('commande_id', $commande_id)->get()->load('mode_payment');

        return response()->json([
            "payments" => $payment->toArray()
        ], 201);
    }

    /**
     * Create Payment
     * Création d'un payment pour la commande
     * @authenticated
     * @bodyParam payment_id int required Mode de règlement enregistrer par le comité.Example: 1
     * @bodyParam date datetime required Date du règlement.Example: 2020-04-19 18:00:00
     * @bodyParam amount string required Montant du règlement.Example: 120.00
     * @bodyParam state int Etat du règlement.Example: 1
     *
     * @response 422 {
     * "exception": "Message Exception de validation",
     * "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     * "payment": "Payment crée"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @param $commande_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $commande_id)
    {
        try {
            $request->validate([
                "payment_id" => "required",
                "date" => "required|date",
                "amount" => "required"
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $payment = $this->commandePayment->newQuery()->create([
            "commande_id" => $commande_id,
            "payment_id" => $request->payment_id,
            "date" => $request->date,
            "amount" => $request->amount,
            "state" => $request->state
        ]);

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }

    /**
     * Get Payment
     * Information sur un payment particulier
     * @authenticated
     *
     * @response 201 {
     * "payment": "Payment"
     * }
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($commande_id, $id)
    {
        $payment = $this->commandePayment->newQuery()->find($id)->load('mode_payment');

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }

    /**
     * Update Payment
     * Mise à jour d'un payment
     * @authenticated
     *
     * @response 201 {
     * "payment": "Payment mise à jours"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @param $commande_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $commande_id, $id)
    {
        $payment = $this->commandePayment->newQuery()->find($id);
        $payment->commande_id = $commande_id;
        $payment->payment_id = $request->payment_id;
        $payment->date = $request->date;
        $payment->amount = $request->amount;
        $payment->state = $request->state;
        $payment->save();

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }

    /**
     * Delete Payment
     * Supprime un payment d'une commande
     * @authenticated
     *
     * @response 201 {
     * "payment": "Payment supprimée"
     * }
     *
     * @param $commande_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($commande_id, $id)
    {
        $payment = $this->commandePayment->newQuery()->find($id);
        $payment->delete();

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }
}
