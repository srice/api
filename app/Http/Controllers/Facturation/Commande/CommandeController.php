<?php

namespace App\Http\Controllers\Facturation\Commande;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Commande\Commande;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class CommandeController
 * @package App\Http\Controllers\Facturation\Commande
 * @group Facturation/Commande
 */
class CommandeController extends Controller
{
    /**
     * @var Commande
     */
    private $commande;

    /**
     * CommandeController constructor.
     * @param Commande $commande
     */
    public function __construct(Commande $commande)
    {
        $this->commande = $commande;
    }

    /**
     * List Commande
     * Liste des Commandes
     * @authenticated
     * @response 201 {
     * "commande": "Liste des commandes"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $commandes = $this->commande->newQuery()->get();

        return response()->json([
            "commandes" => $commandes->toArray()
        ], 201);
    }

    /**
     * Create Commande
     * Création d'une commande à vide
     * @authenticated
     * @bodyParam comite_id int required Identifiant d'un comite affilier à la commande. Example: 1
     * @bodyParam date date required Date de la commande. Example: 2020-04-18 21:13:00
     * @bodyParam espaceCheck int La commande est t-elle affilier à un espace. Example: 1
     * @bodyParam esapce_id int Identifiant de l'esapce affilier à la commande. Example: 1
     *
     * @response 422 {
     * "exception": "Message d'exception de validation",
     * "errors": "Liste des erreurs de validation"
     * }
     *
     * @response 201 {
     * "commande": "Commande crée"
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "comite_id" => "required",
                "date"  => "required|date"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $commande = $this->commande->newQuery()->create([
            "comite_id" => $request->comite_id,
            "date" => $request->date,
            "espaceCheck" => $request->espaceCheck,
            "espace_id" => $request->esapce_id
        ]);

        return response()->json([
            "commande" => $commande->toArray()
        ], 201);
    }

    /**
     * Get Commande
     * Affiche les informations d'une commande
     * @authenticated
     *
     * @response 201 {
     * "commande": "Information de la commande"
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $commande = $this->commande->newQuery()->find($id)->load('comite', 'items', 'payments');

        return  response()->json([
            "commande" => $commande->toArray()
        ], 201);
    }

    /**
     * Update Commande
     * Mise à jour de la commande
     * @authenticated
     * @bodyParam comite_id int Identifiant d'un comite affilier à la commande. Example: 1
     * @bodyParam date date Date de la commande. Example: 2020-04-18 21:13:00
     * @bodyParam espaceCheck int La commande est t-elle affilier à un espace. Example: 1
     * @bodyParam esapce_id int Identifiant de l'esapce affilier à la commande. Example: 1
     *
     * @response 201 {
     * "commande": "Information de la commande mise à jour"
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $commande = $this->commande->newQuery()->find($id);
        $commande->comite_id = $request->comite_id;
        $commande->date = $request->date;
        $commande->espaceCheck = $request->espaceCheck;
        $commande->espace_id = $request->espace_id;
        $commande->save();

        return  response()->json([
            "commande" => $commande->toArray()
        ], 201);
    }

    /**
     * Delete Commande
     * Supprime la commande
     * @authenticated
     *
     * @response 201 {
     * "commande": "Commande "
     * }
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $commande = $this->commande->newQuery()->find($id);
        $commande->delete();

        return  response()->json([
            "commande" => $commande->toArray()
        ], 201);
    }
}
