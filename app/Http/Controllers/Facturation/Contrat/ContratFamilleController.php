<?php

namespace App\Http\Controllers\Facturation\Contrat;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Contrat\ContratFamille;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ContratFamilleController
 * @package App\Http\Controllers\Facturation\Contrat
 * @group Facturation/Contrat/Famille
 */
class ContratFamilleController extends Controller
{
    /**
     * @var ContratFamille
     */
    private $contratFamille;

    /**
     * ContratFamilleController constructor.
     * @param ContratFamille $contratFamille
     */
    public function __construct(ContratFamille $contratFamille)
    {
        $this->contratFamille = $contratFamille;
    }

    /**
     * List Famille
     * Liste des familles de contrat
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $familles = $this->contratFamille->newQuery()->get();

        return response()->json([
            "familles" => $familles->toArray()
        ], 201);
    }

    /**
     * Create Famille
     * Création d'une famille de contrat
     * @authenticated
     * @bodyParam service_id int required ID du service associé à la famille de contrat. Example: 1
     * @bodyParam name string required Désignation de la famille. Example: Accès au service
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "service_id" => "required",
                "name" => "required|string"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $famille = $this->contratFamille->newQuery()->create([
            "service_id" => $request->service_id,
            "name" => $request->name
        ]);

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Get Famille
     * Affiche les information de la famille de contrat
     * @authenticated
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $famille = $this->contratFamille->newQuery()->find($id);

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Update Famille
     * Met à jour la famille de contrat
     * @authenticated
     * @bodyParam service_id int ID du service associé à la famille de contrat. Example: 1
     * @bodyParam name string Désignation de la famille. Example: Accès au service
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $famille = $this->contratFamille->newQuery()->find($id);
        $famille->service_id = $request->service_id;
        $famille->name = $request->name;
        $famille->save();

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    /**
     * Delete Famille
     * Supprime la famille de contrat
     * @authenticated
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $famille = $this->contratFamille->newQuery()->find($id);
        $famille->delete();

        return response()->json([
            "famille" => $famille->toArray()
        ], 201);
    }

    public function contrats($famille_id)
    {
        $famille = $this->contratFamille->newQuery()->find($famille_id)->load('contrats');

        return response()->json([
            "services" => $famille->contrats->toArray()
        ], 201);
    }
}
