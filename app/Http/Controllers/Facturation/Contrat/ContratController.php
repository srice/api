<?php

namespace App\Http\Controllers\Facturation\Contrat;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Contrat\Contrat;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ContratController
 * @package App\Http\Controllers\Facturation\Contrat
 * @group Facturation/Contrat
 */
class ContratController extends Controller
{
    /**
     * @var Contrat
     */
    private $contrat;

    /**
     * ContratController constructor.
     * @param Contrat $contrat
     */
    public function __construct(Contrat $contrat)
    {
        $this->contrat = $contrat;
    }

    /**
     * List Contrat
     * Liste des contrats
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $contrats = $this->contrat->newQuery()->get();

        return response()->json([
            "contrats" => $contrats->toArray()
        ], 201);
    }

    /**
     * Create Contrat
     * Création d'un contrat. Il peut être affilier à une commande.
     * @authenticated
     * @bodyParam comite_id int required ID du comité affilier au contrat. Example: 1
     * @bodyParam contrat_famille_id int required ID de la famille de contrat. Example: 1
     * @bodyParam commande_id int ID de la commande affilier au contrat. Example: 1
     * @bodyParam start date required Date de début du contrat. Example: 2020-04-22 15:12:00
     * @bodyParam end date required Date de fin du contrat. Example: 2021-04-22 00:00:00
     * @bodyParam description string Description facultative du contrat. Example: Lorem
     * @bodyParam state int Etat du contrat | 0: Brouillon <br> 1: Valider <br> 2: En attente de signature client <br> 3: Executer <br> 4: Bientôt expirer <br> 5: Expirer <br> 6: Résilier. Example: 0
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "comite_id" => "required",
                "contrat_famille_id" => "required",
                "start" => "required|date",
                "end" => "required|date|different:start",
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $contrat = $this->contrat->newQuery()->create([
            "comite_id" => $request->comite_id,
            "contrat_famille_id" => $request->contrat_famille_id,
            "commande_id" => $request->commande_id,
            "start" => $request->start,
            "end" => $request->end,
            "description" => $request->description,
            "state" => $request->state
        ]);

        return response()->json([
            "contrat" => $contrat->toArray()
        ], 201);
    }

    /**
     * Get Contrat
     * Information du contrat
     * @authenticated
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $contrat = $this->contrat->newQuery()->find($id);

        return response()->json([
            "contrat" => $contrat->toArray()
        ], 201);
    }

    /**
     * Update Contrat
     * Mise à jour d'un contrat
     * @authenticated
     * @bodyParam comite_id int ID du comité affilier au contrat. Example: 1
     * @bodyParam contrat_famille_id int ID de la famille de contrat. Example: 1
     * @bodyParam commande_id int ID de la commande affilier au contrat. Example: 1
     * @bodyParam start date Date de début du contrat. Example: 2020-04-22 15:12:00
     * @bodyParam end date Date de fin du contrat. Example: 2021-04-22 00:00:00
     * @bodyParam description string Description facultative du contrat. Example: Lorem
     * @bodyParam state int Etat du contrat | 0: Brouillon <br> 1: Valider <br> 2: En attente de signature client <br> 3: Executer <br> 4: Bientôt expirer <br> 5: Expirer <br> 6: Résilier. Example: 0
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $contrat = $this->contrat->newQuery()->find($id);
        $contrat->comite_id = $request->comite_id;
        $contrat->contrat_famille_id = $request->contrat_famille_id;
        $contrat->commande_id = $request->commande_id;
        $contrat->start = $request->start;
        $contrat->end = $request->end;
        $contrat->description = $request->description;
        $contrat->state = $request->state;
        $contrat->save();

        return response()->json([
            "contrat" => $contrat->toArray()
        ], 201);
    }

    /**
     * Delete Contrat
     * Supprime le contrat
     * @authenticated
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $contrat = $this->contrat->newQuery()->find($id);
        $contrat->delete();

        return response()->json([
            "contrat" => $contrat->toArray()
        ], 201);
    }
}
