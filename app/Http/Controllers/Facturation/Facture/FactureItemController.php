<?php

namespace App\Http\Controllers\Facturation\Facture;

use App\Http\Controllers\Controller;
use App\Model\Facturation\FactureItem;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class FactureItemController
 * @package App\Http\Controllers\Facturation\Facture
 * @group Facturation/Facture/Item
 */
class FactureItemController extends Controller
{
    /**
     * @var FactureItem
     */
    private $factureItem;

    /**
     * FactureItemController constructor.
     * @param FactureItem $factureItem
     */
    public function __construct(FactureItem $factureItem)
    {
        $this->factureItem = $factureItem;
    }

    /**
     * List Items
     * Liste les articles d'une facture
     * @authenticated
     *
     * @param $facture_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($facture_id)
    {
        $items = $this->factureItem->newQuery()->where('facture_id', $facture_id)->get();

        return response()->json([
            "items" => $items->toArray()
        ], 201);
    }

    /**
     * Create Item
     * Ajout d'un article dans la facture
     * @authenticated
     * @bodyParam service_id int required ID du service associé à l'article. Example: 1
     * @bodyParam description string Description supplémentaire de l'article. Example: Lorem || null
     * @bodyParam quantite int required Quantite de l'article. Example: 1
     * @bodyParam amount string required Monta total de l'article. Example: 120.00
     *
     * @param \Illuminate\Http\Request $request
     * @param $facture_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $facture_id)
    {
        try {
            $request->validate([
                "service_id" => "required",
                "quantite" => "required|integer",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $item = $this->factureItem->newQuery()->create([
            "facture_id" => $facture_id,
            "service_id" => $request->service_id,
            "description" => $request->description,
            "quantite" => $request->quantite,
            "amount" => $request->amount
        ]);

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Get Item
     * Information de l'article
     * @authenticated
     *
     * @param $facture_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($facture_id, $id)
    {
        $item = $this->factureItem->newQuery()->find($id);

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Update Item
     * Mise à jour de l'article de la facture
     * @authenticated
     * @bodyParam service_id int ID du service associé à l'article. Example: 1
     * @bodyParam description string Description supplémentaire de l'article. Example: Lorem || null
     * @bodyParam quantite int Quantite de l'article. Example: 1
     * @bodyParam amount string Monta total de l'article. Example: 120.00
     *
     * @param \Illuminate\Http\Request $request
     * @param $facture_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $facture_id, $id)
    {
        $item = $this->factureItem->newQuery()->find($id);
        $item->service_id = $request->service_id;
        $item->description = $request->description;
        $item->quantite = $request->quantite;
        $item->amount = $request->amount;
        $item->save();

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }

    /**
     * Delete Item
     * Supprime un article de la facture
     * @authenticated
     *
     * @param $facture_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($facture_id, $id)
    {
        $item = $this->factureItem->newQuery()->find($id);
        $item->delete();

        return response()->json([
            "item" => $item->toArray()
        ], 201);
    }
}
