<?php

namespace App\Http\Controllers\Facturation\Facture;

use App\Http\Controllers\Controller;
use App\Model\Facturation\Facture;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class FactureController
 * @package App\Http\Controllers\Facturation\Facture
 * @group  Facturation/Facture
 */
class FactureController extends Controller
{
    /**
     * @var Facture
     */
    private $facture;

    /**
     * FactureController constructor.
     * @param Facture $facture
     */
    public function __construct(Facture $facture)
    {
        $this->facture = $facture;
    }

    /**
     * List Factures
     * Listing des factures
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $factures = $this->facture->newQuery()->get();

        return response()->json([
            "factures" => $factures->toArray()
        ], 201);
    }

    /**
     * Create facture
     * Création d'une facture | Peut être affilier à une commande
     * Note: La facture deviens automatiquement impayer après 7 jours.
     * @authenticated
     * @bodyParam commande_id int ID de la commande affilier.Example: 1
     * @bodyParam comite_id int required ID du comite.Example: 1
     * @bodyParam date string required Date de la facture.Example: 2020-04-22 10:58:00
     * @bodyParam amount string required Montant de la facture.Example: 120.00
     * @bodyParam state int Etat actuelle de la facture | 0: Brouillon <br> 1: Valider <br> 2: Non Payer <br> 3: Partiellement Payer <br> 4: Payer <br> 5: Impayer.Example: 120.00
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "comite_id" => "required",
                "date" => "required|date",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        if($request->exists('commande_id') == true){
            $facture = $this->facture->newQuery()->create([
                "commande_id" => $request->commande_id,
                "comite_id" => $request->comite_id,
                "date" => $request->date,
                "amount" => $request->amount,
                "state" => $request->state
            ]);
        }else{
            $facture = $this->facture->newQuery()->create([
                "comite_id" => $request->comite_id,
                "date" => $request->date,
                "amount" => $request->amount,
                "state" => $request->state
            ]);
        }

        return response()->json([
            "facture" => $facture->toArray()
        ], 201);
    }

    /**
     * Get Facture
     * Affiche les informations de la facture
     * @authenticated
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $facture = $this->facture->newQuery()->find($id);

        return response()->json([
            "facture" => $facture->toArray()
        ], 201);
    }

    /**
     * Update Facture
     * Mise à jour de la facture
     * @authenticated
     * @bodyParam comite_id int ID du comite.Example: 1
     * @bodyParam date string Date de la facture.Example: 2020-04-22 10:58:00
     * @bodyParam amount string Montant de la facture.Example: 120.00
     * @bodyParam state int Etat actuelle de la facture | 0: Brouillon <br> 1: Valider <br> 2: Non Payer <br> 3: Partiellement Payer <br> 4: Payer <br> 5: Impayer.Example: 120.00
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $facture = $this->facture->newQuery()->find($id);
        $facture->comite_id = $request->comite_id;
        $facture->date = $request->date;
        $facture->amount = $request->amount;
        $facture->state = $request->state;
        $facture->save();

        return response()->json([
            "facture" => $facture->toArray()
        ], 201);
    }

    /**
     * Delete Facture
     * Supprime la facture
     * @authenticated
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $facture = $this->facture->newQuery()->find($id);
        $facture->delete();

        return response()->json([
            "facture" => $facture->toArray()
        ], 201);
    }
}
