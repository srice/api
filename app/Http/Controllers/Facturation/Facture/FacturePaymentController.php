<?php

namespace App\Http\Controllers\Facturation\Facture;

use App\Http\Controllers\Controller;
use App\Model\Facturation\FacturePayment;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class FacturePaymentController
 * @package App\Http\Controllers\Facturation\Facture
 * @group Facturation/Facture/Payment
 */
class FacturePaymentController extends Controller
{
    /**
     * @var FacturePayment
     */
    private $facturePayment;

    /**
     * FacturePaymentController constructor.
     * @param FacturePayment $facturePayment
     */
    public function __construct(FacturePayment $facturePayment)
    {
        $this->facturePayment = $facturePayment;
    }

    /**
     * List Payment
     * Liste des paiement affilier à une facture
     * @authenticated
     *
     * @param $facture_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($facture_id)
    {
        $payments = $this->facturePayment->newQuery()->where('facture_id', $facture_id)->get();

        return response()->json([
            "payments" => $payments->toArray()
        ], 201);
    }

    /**
     * Create Payment
     * Création d'un paiement d'une facture
     * @authenticated
     * @bodyParam payment_id int required ID du mode de paiement `Comite`.Example: 1
     * @bodyParam date date required Date du paiement.Example: 2020-04-22 12:30:00
     * @bodyParam amount string required Montant du paiement.Example: 120.00
     * @bodyParam state int Etat du paiement | 0: En cours d'exécution <br> 1: Executer <br> 2: Erreur de paiement.Example: 0
     *
     * @param \Illuminate\Http\Request $request
     * @param $facture_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $facture_id)
    {
        try {
            $request->validate([
                "payment_id" => "required|integer",
                "date" => "required|date",
                "amount" => "required|string"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $payment = $this->facturePayment->newQuery()->create([
            "facture_id" => $facture_id,
            "payment_id" => $request->payment_id,
            "date" => $request->date,
            "amount" => $request->amount,
            "state" => $request->state
        ]);

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);

    }

    /**
     * Get payment
     * Information sur le paiement d'une facture
     * @authenticated
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $payment = $this->facturePayment->newQuery()->find($id);

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }

    /**
     * Update Payment
     * Mise à jour d'un paiement d'une facture
     * @authenticated
     * @bodyParam payment_id int ID du mode de paiement `Comite`.Example: 1
     * @bodyParam date date Date du paiement.Example: 2020-04-22 12:30:00
     * @bodyParam amount string Montant du paiement.Example: 120.00
     * @bodyParam state int Etat du paiement | 0: En cours d'exécution <br> 1: Executer <br> 2: Erreur de paiement.Example: 0
     *
     * @param \Illuminate\Http\Request $request
     * @param $facture_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $facture_id, $id)
    {
        $payment = $this->facturePayment->newQuery()->find($id);
        $payment->payment_id = $request->payment_id;
        $payment->date = $request->date;
        $payment->amount = $request->amount;
        $payment->state = $request->state;
        $payment->save();

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }

    /**
     * Delete Payment
     * Supprime un paiement d'une facture
     * Note: Suppression possible uniquement si le paiement est en cours d'execution (0)
     * @authenticated
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $payment = $this->facturePayment->newQuery()->find($id);
        if($payment->state == 0) {
            $payment->delete();
        }else{
            return response()->json([
                "error" => "L'état du paiement ne permet pas sa suppression. Etat:".$payment->state
            ], 423);
        }

        return response()->json([
            "payment" => $payment->toArray()
        ], 201);
    }
}
