<?php

namespace App\Http\Controllers\Support\Travaux;

use App\Http\Controllers\Controller;
use App\Model\Support\Travaux\TravauxComment;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TravauxCommentController extends Controller
{
    /**
     * @var TravauxComment
     */
    private $travauxComment;

    /**
     * TravauxCommentController constructor.
     * @param TravauxComment $travauxComment
     */
    public function __construct(TravauxComment $travauxComment)
    {
        $this->travauxComment = $travauxComment->newQuery();
    }

    public function list($travaux_id)
    {
        $comments = $this->travauxComment->where('travaux_id', $travaux_id)->get();

        return response()->json($comments->toArray(), 201);
    }

    public function store(Request $request, $travaux_id)
    {
        try {
            $request->validate([
                "comment" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $comment = $this->travauxComment->create([
            "travaux_id" => $travaux_id,
            "comment" => $request->comment
        ]);

        return response()->json($comment->toArray(), 201);
    }

    public function get($travaux_id, $id)
    {
        $comment = $this->travauxComment->find($id);

        return response()->json($comment->toArray(), 201);
    }

    public function update(Request $request, $travaux_id, $id)
    {
        $comment = $this->travauxComment->find($id);
        if($request->exists('comment') == true){$comment->comment = $request->comment;}
        $comment->save();

        return response()->json($comment->toArray(), 201);
    }

    public function destroy($travaux_id, $id)
    {
        $comment = $this->travauxComment->find($id);
        $comment->delete();

        return response()->json($comment->toArray(), 201);
    }
}
