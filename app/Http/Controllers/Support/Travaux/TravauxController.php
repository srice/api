<?php

namespace App\Http\Controllers\Support\Travaux;

use App\Http\Controllers\Controller;
use App\Model\Support\Travaux\Travaux;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TravauxController extends Controller
{
    /**
     * @var Travaux
     */
    private $travaux;

    /**
     * TravauxController constructor.
     * @param Travaux $travaux
     */
    public function __construct(Travaux $travaux)
    {
        $this->travaux = $travaux->newQuery();
    }

    public function list()
    {
        $trav = $this->travaux->get();

        return response()->json($trav->toArray(), 201);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                "travaux_secteur_id" => "required",
                "type" => "required|integer",
                "summary" => "required",
                "details" => "required",
                "status" => "required|integer"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception,
                "errors" => $exception->errors()
            ], 422);
        }

        $travaux = $this->travaux->create([
            "travaux_secteur_id" => $request->travaux_secteur_id,
            "type" => $request->type,
            "summary" => $request->summary,
            "details" => $request->details,
            "status" => $request->status
        ]);

        return response()->json($travaux->toArray(), 201);
    }

    public function get($id)
    {
        $travaux = $this->travaux->find($id);

        return response()->json($travaux->toArray(), 201);
    }

    public function update(Request $request, $id)
    {
        $travaux = $this->travaux->find($id);
        if($request->exists('travaux_secteur_id') == true){$travaux->travaux_secteur_id = $request->travaux_secteur_id;}
        if($request->exists('type') == true){$travaux->type = $request->type;}
        if($request->exists('summary') == true){$travaux->summary = $request->summary;}
        if($request->exists('details') == true){$travaux->details = $request->details;}
        if($request->exists('status') == true){$travaux->status = $request->status;}
        $travaux->save();

        return response()->json($travaux->toArray(), 201);
    }

    public function destroy($id)
    {
        $travaux = $this->travaux->find($id);
        $travaux->delete();

        return response()->json($travaux->toArray(), 201);
    }
}
