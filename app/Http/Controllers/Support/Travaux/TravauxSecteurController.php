<?php

namespace App\Http\Controllers\Support\Travaux;

use App\Http\Controllers\Controller;
use App\Model\Support\Travaux\TravauxSecteur;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TravauxSecteurController extends Controller
{
    /**
     * @var TravauxSecteur
     */
    private $travauxSecteur;

    /**
     * TravauxSecteurController constructor.
     * @param TravauxSecteur $travauxSecteur
     */
    public function __construct(TravauxSecteur $travauxSecteur)
    {
        $this->travauxSecteur = $travauxSecteur->newQuery();
    }

    public function list()
    {
        $secteur = $this->travauxSecteur->get();

        return response()->json($secteur->toArray(), 201);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $secteur = $this->travauxSecteur->create([
            "name" => $request->name
        ]);

        return response()->json($secteur->toArray(), 201);
    }

    public function get($id)
    {
        $secteur = $this->travauxSecteur->find($id);

        return response()->json($secteur->toArray(), 201);
    }

    public function update(Request $request, $id)
    {
        $secteur = $this->travauxSecteur->find($id);
        if($request->exists('name') == true){$secteur->name = $request->name;}
        $secteur->save();

        return response()->json($secteur->toArray(), 201);
    }

    public function destroy($id)
    {
        $secteur = $this->travauxSecteur->find($id);
        $secteur->delete();

        return response()->json($secteur->toArray(), 201);
    }

    public function travaux($id)
    {
        $secteur = $this->travauxSecteur->find($id);

        return response()->json($secteur->travauxes->toArray(), 201);
    }
}
