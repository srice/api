<?php

namespace App\Http\Controllers\Support\Ticket;

use App\Http\Controllers\Controller;
use App\Model\Support\Ticket\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

/**
 * Class TicketController
 * @package App\Http\Controllers\Support\Ticket
 * @group Support/Ticket
 */
class TicketController extends Controller
{
    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * TicketController constructor.
     * @param Ticket $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket->newQuery();
    }

    /**
     * List Ticket
     * Liste des tickets
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $tickets = $this->ticket->get();

        return response()->json($tickets->toArray(), 201);
    }

    /**
     * Create Ticket
     * Création d'un ticket
     * @authenticated
     * @bodyParam ticket_category_id int required ID de la catégorie du ticket.Example: 1
     * @bodyParam comite_id int required ID du comité emetter du ticket.Example: 1
     * @bodyParam sujet string required Sujet du ticket.Example: Création de salarié impossible.
     * @bodyParam status int Status du ticket | 1: Nouveau ticket <br> 2: Ticket Ouvert <br> 3: Ticket en attente <br> 4: Ticket Résolue <br> 5: Ticket Cloturer | Default: 1.Example 1
     * @bodyParam priority Priorité du ticket | 1: Low <br> 2: Normal <br> 3: High <br> 4: Critical | Default: 2 . Example: 2
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "ticket_category_id" => "required",
                "comite_id" => "required",
                "sujet" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $ticket = $this->ticket->create([
            "ticket_category_id" => $request->ticket_category_id,
            "comite_id" => $request->comite_id,
            "sujet" => $request->sujet,
            "status" => $request->status,
            "priority" => $request->priority
        ]);

        Log::channel('slack')->info('Création du ticket N°'.$ticket->id, [
            "comite" => $ticket->comite->name,
            "sujet" => $ticket->sujet
        ]);

        return response()->json($ticket->toArray(), 201);
    }

    /**
     * Get Ticket
     * Affiche les information d'un ticket
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $ticket = $this->ticket->find($id);

        return response()->json($ticket->toArray(), 201);
    }

    /**
     * Update Ticket
     * Mise à jour d'un ticket
     * @authenticated
     * @bodyParam ticket_category_id int ID de la catégorie du ticket.Example: 1
     * @bodyParam comite_id int ID du comité emetter du ticket.Example: 1
     * @bodyParam sujet string Sujet du ticket.Example: Création de salarié impossible.
     * @bodyParam status int Status du ticket | 1: Nouveau ticket <br> 2: Ticket Ouvert <br> 3: Ticket en attente <br> 4: Ticket Résolue <br> 5: Ticket Cloturer | Default: 1.Example 1
     * @bodyParam priority Priorité du ticket | 1: Low <br> 2: Normal <br> 3: High <br> 4: Critical | Default: 2 . Example: 2
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $ticket = $this->ticket->find($id);
        if($request->exists('ticket_category_id') == true){$ticket->ticket_category_id = $request->ticket_category_id;}
        if($request->exists('comite_id') == true){$ticket->comite_id = $request->comite_id;}
        if($request->exists('sujet') == true){$ticket->sujet = $request->sujet;}
        if($request->exists('status') == true){$ticket->status = $request->status;}
        if($request->exists('priority') == true){$ticket->priority = $request->priority;}
        $ticket->save();

        return response()->json($ticket->toArray(), 201);
    }

    /**
     * Delete Ticket
     * Supprime un ticket
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $ticket = $this->ticket->find($id);
        $ticket->delete();

        return response()->json($ticket->toArray(), 201);
    }
}
