<?php

namespace App\Http\Controllers\Support\Ticket;

use App\Http\Controllers\Controller;
use App\Model\Support\Ticket\TicketCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class TicketCategoryController
 * @package App\Http\Controllers\Support\Ticket
 * @group Support/Ticket/Category
 */
class TicketCategoryController extends Controller
{
    /**
     * @var TicketCategory
     */
    private $ticketCategory;

    /**
     * TicketCategoryController constructor.
     * @param TicketCategory $ticketCategory
     */
    public function __construct(TicketCategory $ticketCategory)
    {
        $this->ticketCategory = $ticketCategory->newQuery();
    }

    /**
     * List Categories
     * Liste des catégories de ticket
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $categories = $this->ticketCategory->get();

        return response()->json($categories->toArray(), 201);
    }

    /**
     * Create Category
     * Création d'une catégorie de ticket
     * @authenticated
     * @bodyParam name string required Nom de la catégorie. Example: Support Commercial
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $category = $this->ticketCategory->create([
            "name" => $request->name
        ]);

        return response()->json($category->toArray(), 201);
    }

    /**
     * Get Category
     * Affiche les informations d'une catégories
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $category = $this->ticketCategory->find($id);

        return response()->json($category->toArray(), 201);
    }

    /**
     * Update Category
     * Mise à jours de la catégorie
     * @authenticated
     * @bodyParam name string required Nom de la catégorie. Example: Support Commercial
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $category = $this->ticketCategory->find($id);
        if($request->exists('name') == true){$category->name = $request->name;}
        $category->save();

        return response()->json($category->toArray(), 201);
    }

    /**
     * Delete Category
     * Supprime une catégorie
     * @authenticated
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $category = $this->ticketCategory->find($id);
        $category->delete();

        return response()->json($category->toArray(), 201);
    }

    public function tickets($id)
    {
        $category = $this->ticketCategory->find($id);

        return response()->json($category->tickets->toArray(), 201);
    }
}
