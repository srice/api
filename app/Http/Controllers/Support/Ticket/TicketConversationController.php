<?php

namespace App\Http\Controllers\Support\Ticket;

use App\Http\Controllers\Controller;
use App\Model\Support\Ticket\TicketConversation;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class TicketConversationController
 * @package App\Http\Controllers\Support\Ticket
 * @group Support/Ticket/Conversation
 */
class TicketConversationController extends Controller
{
    /**
     * @var TicketConversation
     */
    private $ticketConversation;

    /**
     * TicketConversationController constructor.
     * @param TicketConversation $ticketConversation
     */
    public function __construct(TicketConversation $ticketConversation)
    {
        $this->ticketConversation = $ticketConversation->newQuery();
    }

    /**
     * List Conversations
     * Liste des conversation d'un ticket
     * @authenticated
     *
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($ticket_id)
    {
        $conversations = $this->ticketConversation->where('ticket_id')->get();

        return response()->json($conversations->toArray(), 201);
    }

    /**
     * Create Conversation
     * Création d'un message pour le ticket
     * @authenticated
     * @bodyParam text string required Texte de la conversation.Example: Lorem
     * @bodyParam user boolean Définie si le message vient d'un utilisateur. Example: false
     * @bodyParam support boolean Définie si le message vient d'un technicien du support. Example: true
     *
     * @param Request $request
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $ticket_id)
    {
        try {
            $request->validate([
                "text" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $conversation = $this->ticketConversation->create([
            "ticket_id" => $ticket_id,
            "text" => $request->text,
            "user" => $request->user,
            "support" => $request->support
        ]);

        return response()->json($conversation->toArray(), 201);
    }

    /**
     * Get Conversation
     * Affiche les information d'une conversation
     * @authenticated
     *
     * @param $ticket_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($ticket_id, $id)
    {
        $conversation = $this->ticketConversation->find($id);

        return response()->json($conversation->toArray(), 201);
    }

    /**
     * Update Conversation
     * Met à jour une conversation
     * @authenticated
     * @bodyParam text string Texte de la conversation.Example: Lorem
     * @bodyParam user boolean Définie si le message vient d'un utilisateur. Example: false
     * @bodyParam support boolean Définie si le message vient d'un technicien du support. Example: true
     *
     * @param Request $request
     * @param $ticket_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $ticket_id, $id)
    {
        $conversation = $this->ticketConversation->find($id);
        if($request->exists('text') == true){$conversation->text = $request->text;}
        if($request->exists('user') == true){$conversation->user = $request->user;}
        if($request->exists('support') == true){$conversation->support = $request->support;}
        $conversation->save();

        return response()->json($conversation->toArray(), 201);
    }

    /**
     * Delete Conversation
     * Supprime une conversation
     * @authenticated
     *
     * @param $ticket_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($ticket_id, $id)
    {
        $conversation = $this->ticketConversation->find($id);
        $conversation->delete();

        return response()->json($conversation->toArray(), 201);
    }
}
