<?php

namespace App\Notifications;

use App\Model\Comite\Comite;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ComiteCreateComite extends Notification
{
    use Queueable;
    /**
     * @var Comite
     */
    public $comite;
    public $password;

    /**
     * Create a new notification instance.
     *
     * @param Comite $comite
     * @param $password
     */
    public function __construct(Comite $comite, $password)
    {
        //
        $this->comite = $comite;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Bonjour '.$this->comite->name)
                    ->line('Votre compte à bien été créer')
                    ->line('Identifiant: '.$this->comite->email)
                    ->line('Mot de Passe: '.$this->password)
                    ->action('Panel Cloud SRICE', 'https://gestion.srice.eu');
    }
}
