[![pipeline status](https://gitlab.com/srice/api/badges/master/pipeline.svg)](https://gitlab.com/srice/api/-/commits/master)
[![coverage report](https://gitlab.com/srice/api/badges/master/coverage.svg)](https://gitlab.com/srice/api/-/commits/master)

# API SRICE
Système REST de l'environnement SRICE

# Release Versionning

Format: A.B.C
- A: Passage En production de fonction
- B: Ajout de Fonction Primaire ou Sectoriel
- C: Ajout ou modification de fonction secondaire ou tertiaire
