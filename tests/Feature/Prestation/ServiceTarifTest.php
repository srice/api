<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\Model\Prestation\ServiceTarif;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ServiceTarifTest extends TestCase
{

    public function testListTarifForService()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json("GET", '/api/prestation/service/'.$service->id.'/tarif')
            ->assertStatus(201);
    }
    public function testListBlankTarifForService()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();

        $this->actingAs($user, 'api')->json("GET", '/api/prestation/service/'.$service->id.'/tarif')
            ->assertStatus(404);
    }
    public function testCreateTarifSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $payload = [
            "service_id" => $service->id,
            "name" => "Tarif Unitaire",
            "amount" => "12.00"
        ];

        $this->actingAs($user, 'api')->json("POST", '/api/prestation/service/'.$service->id.'/tarif', $payload)
            ->assertStatus(201);
    }

    public function testCreateTarifRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();

        $this->actingAs($user, 'api')->json("POST", '/api/prestation/service/'.$service->id.'/tarif')
            ->assertStatus(422);
    }

    public function testGetTarif()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json("GET", '/api/prestation/service/'.$service->id.'/tarif/'.$tarif->id)
            ->assertStatus(201);
    }

    public function testUpdateTarifSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);

        $payload = [
            "name" => "Update Tarif",
            "amount" => "13.00"
        ];

        $this->actingAs($user, 'api')->json("POST", '/api/prestation/service/'.$service->id.'/tarif/'.$tarif->id, $payload)
            ->assertStatus(201);
    }

    public function testUpdateTarifRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);

        $payload = [
            "name" => "Update Tarif",
            "amount" => "13.00"
        ];

        $this->actingAs($user, 'api')->json("POST", '/api/prestation/service/'.$service->id.'/tarif/'.$tarif->id)
            ->assertStatus(422);
    }

    public function testDeleteTarif()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json("DELETE", '/api/prestation/service/'.$service->id.'/tarif/'.$tarif->id)
            ->assertStatus(201);
    }
}
