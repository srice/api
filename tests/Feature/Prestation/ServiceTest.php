<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Famille;
use App\Model\Prestation\Module;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ServiceTest extends TestCase
{
    public function testListService()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        factory(Service::class, 15)->create();

        $this->actingAs($user, 'api')->json('GET', '/api/prestation/service')
            ->assertStatus(201);
    }

    public function testCreateServiceNoKernelSuccess()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create([
            "name" => "Formation"
        ]);
        $payload = [
            "famille_id" => $famille->id,
            "name" => "Formation 1H",
            "kernel" => 0
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/service', $payload)
            ->assertStatus(201)
            ->assertJson([
                'service' => [
                    "id" => 1,
                    "name" => "Formation 1H",
                    "kernel" => 0,
                    "module_id" => null
                ]
            ]);
    }

    public function testCreateServiceEspaceSuccess()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create([
            "name" => "Application"
        ]);
        $payload = [
            "famille_id" => $famille->id,
            "name" => "Accès au service SRICE",
            "kernel" => 1
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/service', $payload)
            ->assertStatus(201)
            ->assertJson([
                'service' => [
                    "id" => 1,
                    "name" => "Accès au service SRICE",
                    "kernel" => 1,
                    "module_id" => null
                ]
            ]);
    }

    public function testCreateServiceModuleSuccess()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create([
            "name" => "Module"
        ]);
        $module = factory(Module::class)->create([
            "name" => "ANCV",
            "description" => null,
            "version"   => "0.0.1",
            "release"   => 6
        ]);
        $payload = [
            "famille_id" => $famille->id,
            "name" => "Module: ANCV",
            "kernel" => 2,
            "module_id" => $module->id
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/service', $payload)
            ->assertStatus(201)
            ->assertJson([
                'service' => [
                    "id" => 1,
                    "famille_id" => 1,
                    "name" => "Module: ANCV",
                    "kernel" => 2,
                    "module_id" => 1
                ]
            ]);
    }

    public function testCreateServiceRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create([
            "name" => "Formation"
        ]);
        $payload = [
            "name" => null
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/service', $payload)
            ->assertStatus(422);
    }

    public function testGetService()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();

        //dd($service);
        $this->actingAs($user, 'api')->json('GET', '/api/prestation/service/'.$service->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'service' => [
                    'famille_id',
                    'name',
                    'kernel',
                    'id'
                ]
            ]);
    }

    public function testUpdateServiceSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $module = factory(Module::class)->create([
            "name" => "ANCV",
            "description" => null,
            "version"   => "0.0.1",
            "release"   => 6
        ]);
        $service = factory(Service::class)->create([
            "famille_id" => 1,
            "name" => "Test",
            "kernel" => 2,
            "module_id" => $module->id
        ]);

        $payload = [
            "name" => "Test Update",
            "famille_id" => 1,
            "kernel"    => 2,
            "module_id" => $module->id
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/service/'.$service->id, $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'service' => [
                    'famille_id',
                    'name',
                    'kernel',
                    'id'
                ]
            ]);
    }

    public function testDeleteService()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();

        $this->actingAs($user, 'api')->json('DELETE', '/api/prestation/service/'.$service->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'service' => [
                    'famille_id',
                    'name',
                    'kernel',
                    'id'
                ]
            ]);
    }
}
