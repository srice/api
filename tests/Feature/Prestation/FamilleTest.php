<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FamilleTest extends TestCase
{
    public function testListFamille()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class, 15)->create();

        $this->actingAs($user, 'api')->json('GET', '/api/prestation/famille')
            ->assertStatus(201);

        $this->assertEquals(15, count($famille));

    }

    public function testCreateFamilleSuccess()
    {
        $user = factory(User::class)->create();

        $payload = [
            "name" => "Test famille"
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/famille', $payload)
            ->assertStatus(201);
    }

    public function testCreateFamilleRequired()
    {
        $user = factory(User::class)->create();

        $payload = [
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/famille', $payload)
            ->assertStatus(422);
    }

    public function testGetFamille()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create();
        $this->actingAs($user, 'api')->json('GET', '/api/prestation/famille/'.$famille->id)
            ->assertStatus(201);
    }

    public function testUpdateFamilleSuccess()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create();

        $payload = [
            "name" => "Update Famille"
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/famille/'.$famille->id, $payload)
            ->assertStatus(201)
            ->assertJson([
                'famille' => [
                    'id' => $famille->id,
                    'name' => $payload['name']
                ]
            ]);
    }

    public function testUpdateFamilleRequired()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create();

        $payload = [
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/famille/'.$famille->id, $payload)
            ->assertStatus(422);
    }

    public function testDeleteFamille()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create();

        $this->actingAs($user, 'api')->json('DELETE', '/api/prestation/famille/'.$famille->id)
            ->assertStatus(201);
    }

    public function testGetServiceByFamille()
    {
        $user = factory(User::class)->create();
        $famille = factory(Famille::class)->create();
        factory(Service::class, 2)->create([
            "famille_id" => $famille->id
        ]);

        $this->actingAs($user, 'api')->json('GET', '/api/prestation/famille/'.$famille->id.'/service')
            ->assertStatus(201);
    }
}
