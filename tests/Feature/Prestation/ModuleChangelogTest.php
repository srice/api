<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Module;
use App\Model\Prestation\ModuleChangelog;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ModuleChangelogTest extends TestCase
{
    public function testListChangelog()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        factory(ModuleChangelog::class, 10)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/prestation/module/'.$module->id.'/changelog')
            ->assertStatus(201);
    }

    public function testCreateChangelogSuccess()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $payload = [
            "module_id" => $module->id,
            "version" => "0.0.2",
            "description" => "Note de Mise à jour 0.0.2",
            "date" => now(),
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/prestation/module/'.$module->id.'/changelog', $payload)
            ->assertStatus(201);
    }

    public function testCreateChangelogRequired()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $payload = [
            "module_id" => $module->id,
            "version" => "0.0.2",
            "description" => "Note de Mise à jour 0.0.2",
            "date" => now(),
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/prestation/module/'.$module->id.'/changelog')
            ->assertStatus(422);
    }

    public function testGetChangelog()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $changelog = factory(ModuleChangelog::class)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/prestation/module/'.$module->id.'/changelog/'.$changelog->id)
            ->assertStatus(201);
    }

    public function testUpdateChangelogSuccess()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $changelog = factory(ModuleChangelog::class)->create([
            "module_id" => $module->id,
            "version" => "0.0.2",
            "description" => "Note de Mise à jour 0.0.2",
            "date" => now(),
            "state" => 0
        ]);
        $payload = [
            "module_id" => $module->id,
            "version" => "0.0.3",
            "description" => "Note de Mise à jour 0.0.3",
            "date" => now(),
            "state" => 1
        ];

        $this->actingAs($user, 'api')->json('post', '/api/prestation/module/'.$module->id.'/changelog/'.$changelog->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteChangelog()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $changelog = factory(ModuleChangelog::class)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/prestation/module/'.$module->id.'/changelog/'.$changelog->id)
            ->assertStatus(201);
    }
}
