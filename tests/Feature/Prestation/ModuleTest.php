<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Module;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\MockObject\Generator;
use Tests\TestCase;

class ModuleTest extends TestCase
{
    use WithFaker;
    public function testListModule()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        factory(Module::class, 10)->create();

        $this->actingAs($user, "api")->json('GET', '/api/prestation/module')
            ->assertStatus(201);
    }

    public function testCreateModuleSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $payload = [
            "name" => "Module N°".$this->faker->randomNumber(2),
            "description" => $this->faker->text,
            "version" => $this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1),
            "release" => $this->faker->numberBetween(0,6)
        ];

        $this->actingAs($user, "api")->json('POST', '/api/prestation/module', $payload)
            ->assertStatus(201);
    }

    public function testCreateModuleRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $payload = [
            "name" => "Module N°".$this->faker->randomNumber(2),
            "description" => $this->faker->text,
            "version" => $this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1),
            "release" => $this->faker->numberBetween(0,6)
        ];

        $this->actingAs($user, "api")->json('POST', '/api/prestation/module')
            ->assertStatus(422);
    }

    public function testGetModule()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create([
            "name" => "Module N°".$this->faker->randomNumber(2),
            "description" => $this->faker->text,
            "version" => $this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1),
            "release" => $this->faker->numberBetween(0,6)
        ]);

        $this->actingAs($user, "api")->json('GET', '/api/prestation/module/'.$module->id)
            ->assertStatus(201);
    }

    public function testUpdateModule()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create([
            "name" => "Module N°".$this->faker->randomNumber(2),
            "description" => $this->faker->text,
            "version" => $this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1),
            "release" => $this->faker->numberBetween(0,6)
        ]);

        $payload = [
            "name" => "Update Name Module",
            "version" => "0.0.3",
            "release" => 2
        ];

        $this->actingAs($user, "api")->json('POST', '/api/prestation/module/'.$module->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteModule()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create([
            "name" => "Module N°".$this->faker->randomNumber(2),
            "description" => $this->faker->text,
            "version" => $this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1).'.'.$this->faker->randomNumber(1),
            "release" => $this->faker->numberBetween(0,6)
        ]);

        $this->actingAs($user, "api")->json('DELETE', '/api/prestation/module/'.$module->id)
            ->assertStatus(201);
    }
}
