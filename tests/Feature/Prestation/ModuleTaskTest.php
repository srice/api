<?php

namespace Tests\Feature\Prestation;

use App\Model\Prestation\Module;
use App\Model\Prestation\ModuleTask;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ModuleTaskTest extends TestCase
{
    public function testListTask()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $tasks = factory(ModuleTask::class, 10)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('GET', '/api/prestation/module/'.$module->id.'/task')
            ->assertStatus(201);
    }

    public function testCreateTaskSuccess()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $payload = [
            "module_id" => $module->id,
            "subject" => "Création du module",
            "description" => "Description de la tache",
            "start" => now(),
            "end" => now()->addDays(rand(1,5)),
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/module/'.$module->id.'/task', $payload)
            ->assertStatus(201);
    }

    public function testCreateTaskRequired()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/module/'.$module->id.'/task')
            ->assertStatus(422);
    }

    public function testGetTask()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $task = factory(ModuleTask::class)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('GET', '/api/prestation/module/'.$module->id.'/task/'.$task->id)
            ->assertStatus(201);
    }

    public function testUpdateTask()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $task = factory(ModuleTask::class)->create([
            "module_id" => $module->id,
            "subject" => "Création du module",
            "description" => "Description de la tache",
            "start" => now(),
            "end" => now()->addDays(rand(1,5)),
            "state" => 0
        ]);

        $payload = [
            "subject" => "Mise à jour du module",
            "description" => "Mise à jours de la description de la tache",
            "start" => now(),
            "end" => now()->addDays(rand(1,5)),
            "state" => 2
        ];

        $this->actingAs($user, 'api')->json('POST', '/api/prestation/module/'.$module->id.'/task/'.$task->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteTask()
    {
        $user = factory(User::class)->create();
        $module = factory(Module::class)->create();
        $task = factory(ModuleTask::class)->create([
            "module_id" => $module->id
        ]);

        $this->actingAs($user, 'api')->json('DELETE', '/api/prestation/module/'.$module->id.'/task/'.$task->id)
            ->assertStatus(201);
    }
}
