<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Commande\Commande;
use App\Model\Facturation\Commande\CommandeItem;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\Model\Prestation\ServiceTarif;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommandeItemTest extends TestCase
{
    public function testListItems()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        factory(CommandeItem::class)->create([
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/'.$commande->id.'/item')
            ->assertStatus(201);
    }

    public function testCreateItemSuccess()
    {

        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id,
            "description" => "Lorem",
            "quantite" => 1,
            "amount" => 120.00
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/item', $payload)
            ->assertStatus(201);
    }

    public function testCreateItemRequired()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id,
            "description" => "Lorem",
            "quantite" => 1,
            "amount" => 120.00
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/item')
            ->assertStatus(422);
    }

    public function testGetItem()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $item = factory(CommandeItem::class)->create([
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/'.$commande->id.'/item/'.$item->id)
            ->assertStatus(201);
    }

    public function testUpdateItem()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $item = factory(CommandeItem::class)->create([
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id
        ]);

        $payload = [
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id,
            "description" => "Update",
            "quantite" => 2,
            "amount" => 240.00
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/item/'.$item->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteItem()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Famille::class)->create();
        $service = factory(Service::class)->create();
        $tarif = factory(ServiceTarif::class)->create([
            "service_id" => $service->id
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $item = factory(CommandeItem::class)->create([
            "commande_id" => $commande->id,
            "service_id" => $service->id,
            "service_tarif_id" => $tarif->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/commande/'.$commande->id.'/item/'.$item->id)
            ->assertStatus(201);
    }


}
