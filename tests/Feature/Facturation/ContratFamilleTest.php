<?php

namespace Tests\Feature\Facturation;

use App\Model\Facturation\Contrat\ContratFamille;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContratFamilleTest extends TestCase
{
    public function testListFamille()
    {
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/contrat/famille')
            ->assertStatus(201);
    }

    public function testCreateFamilleSuccess()
    {
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $payload = [
            "service_id" => $service->id,
            "name" => "Accès au service"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/famille', $payload)
            ->assertStatus(201);
    }

    public function testCreateFamilleRequired()
    {
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $payload = [
            "service_id" => $service->id,
            "name" => "Accès au service"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/famille')
            ->assertStatus(422);
    }

    public function testGetFamille()
    {
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/contrat/famille/'.$famille->id)
            ->assertStatus(201);
    }

    public function testUpdateFamille()
    {
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);

        $payload = [
            "name" => "Update"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/famille/'.$famille->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteFamille()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id,
            "name" => "fkdsfsdlkjflksdjflksdj"
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id,
            "name" => "kdfjdklsfjlksdjf"
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/contrat/famille/'.$famille->id)
            ->assertStatus(201);
    }

    public function testGetContratWithFamille()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id,
            "name" => "fkdsfsdlkjflksdjflksdj"
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id,
            "name" => "kdfjdklsfjlksdjf"
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/contrat/famille/'.$famille->id.'/contrats')
            ->assertStatus(201);
    }
}
