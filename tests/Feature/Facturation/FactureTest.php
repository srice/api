<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Commande\Commande;
use App\Model\Facturation\Facture;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FactureTest extends TestCase
{
    public function testListFacture()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Facture::class)->create([
            "comite_id" => $comite->id,
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture')
            ->assertStatus(201);
    }

    public function testCreateFactureSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            "comite_id" => $comite->id,
            "date" => now(),
            "amount" => 100.00,
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture', $payload)
            ->assertStatus(201);
    }

    public function testCreateFactureWithCommande()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);
        $payload = [
            "commande_id" => $commande->id,
            "comite_id" => $comite->id,
            "date" => now(),
            "amount" => 100.00,
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture', $payload)
            ->assertStatus(201);
    }

    public function testCreateFactureRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            "comite_id" => $comite->id,
            "date" => now(),
            "amount" => 100.00,
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture')
            ->assertStatus(422);
    }

    public function testGetFacture()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture/'.$facture->id)
            ->assertStatus(201);
    }

    public function testUpdateFacture()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "comite_id" => $comite->id,
            "date" => now()->subDays(5),
            "amount" => 100,
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteFacture()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/facture/'.$facture->id)
            ->assertStatus(201);
    }


}
