<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Facture;
use App\Model\Facturation\FactureItem;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FactureItemTest extends TestCase
{
    public function testListItemFacture()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);
        factory(FactureItem::class)->create([
            "facture_id" => $facture->id,
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture/'.$facture->id.'/item')
            ->assertStatus(201);
    }

    public function testCreateItemFactureSuccess()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);
        $payload = [
            "facture_id" => $facture->id,
            "service_id" => $service->id,
            "description" => "Lorem",
            "quantite" => 1,
            "amount" => 1.00
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/item', $payload)
            ->assertStatus(201);
    }

    public function testCreateItemFactureRequired()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/item')
            ->assertStatus(422);
    }

    public function testGetItemFacture()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);
        $item = factory(FactureItem::class)->create([
            "facture_id" => $facture->id,
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture/'.$facture->id.'/item/'.$item->id)
            ->assertStatus(201);
    }

    public function testUpdateItemFacture()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);
        $item = factory(FactureItem::class)->create([
            "facture_id" => $facture->id,
            "service_id" => $service->id
        ]);
        $payload = [
            "facture_id" => $facture->id,
            "service_id" => $service->id,
            "quantite" => 1,
            "amount" => 1.00
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/item/'.$item->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteItemFacture()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $famille = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $famille->id
        ]);
        $item = factory(FactureItem::class)->create([
            "facture_id" => $facture->id,
            "service_id" => $service->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/facture/'.$facture->id.'/item/'.$item->id)
            ->assertStatus(201);
    }
}
