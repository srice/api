<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Comite\Payment;
use App\Model\Facturation\Facture;
use App\Model\Facturation\FacturePayment;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FacturePaymentTest extends TestCase
{
    public function testListPayment()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        factory(FacturePayment::class)->create([
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture/'.$facture->id.'/payment')
            ->assertStatus(201);

    }

    public function testCreatePaymentSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id,
            "date" => now(),
            "amount" => "120.00",
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/payment', $payload)
            ->assertStatus(201);

    }

    public function testCreatePaymentRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id,
            "date" => now(),
            "amount" => "120.00",
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/payment')
            ->assertStatus(422);

    }

    public function testGetPayment()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $payment = factory(FacturePayment::class)->create([
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/facture/'.$facture->id.'/payment/'.$payment->id)
            ->assertStatus(201);
    }

    public function testUpdatePayment()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $payment = factory(FacturePayment::class)->create([
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id
        ]);

        $payload = [
            "date" => now()->subDays(5),
            "amount" => "1",
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/facture/'.$facture->id.'/payment/'.$payment->id, $payload)
            ->assertStatus(201);
    }

    public function testDeletePayment()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $payment = factory(FacturePayment::class)->create([
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/facture/'.$facture->id.'/payment/'.$payment->id)
            ->assertStatus(201);
    }

    public function testDeletePaymentNotExeuted()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_comite = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "cus_00000000000"
        ]);
        $facture = factory(Facture::class)->create([
            "comite_id" => $comite->id
        ]);
        $payment = factory(FacturePayment::class)->create([
            "facture_id" => $facture->id,
            "payment_id" => $p_comite->id,
            "state" => 2
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/facture/'.$facture->id.'/payment/'.$payment->id)
            ->assertStatus(423);
    }
}
