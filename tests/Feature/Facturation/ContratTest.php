<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Contrat\ContratFamille;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContratTest extends TestCase
{
    public function testListContrat()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/contrat/contrat/')
            ->assertStatus(201);
    }

    public function testCreateContratSuccess()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);

        $payload = [
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
            "start" => now(),
            "end" => now()->addYear(),
            "description" => "Lorem",
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/contrat/', $payload)
            ->assertStatus(201);
    }

    public function testCreateContratRequired()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);

        $payload = [
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
            "start" => now(),
            "end" => now()->addYear(),
            "description" => "Lorem",
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/contrat/')
            ->assertStatus(422);
    }

    public function testGetContrat()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/contrat/contrat/'.$contrat->id)
            ->assertStatus(201);
    }

    public function testUpdateContrat()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
        ]);

        $payload = [
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
            "start" => now(),
            "end" => now()->addYear(),
            "state" => 1
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/contrat/contrat/'.$contrat->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteContrat()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $p_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $p_service->id
        ]);
        $famille = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $famille->id,
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/contrat/contrat/'.$contrat->id)
            ->assertStatus(201);
    }
}
