<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Comite\Payment;
use App\Model\Facturation\Commande\Commande;
use App\Model\Facturation\Commande\CommandePayment;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\Model\Prestation\ServiceTarif;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommandePaymentTest extends TestCase
{
    public function testListPayment()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        factory(CommandePayment::class)->create([
            "commande_id" => $commande->id,
            "payment_id" => $m_payment->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/'.$commande->id.'/payment')
            ->assertStatus(201);
    }

    public function testCreatePaymentSuccess()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "payment_id" => $m_payment->id,
            "date" => now(),
            "amount" => 200.00,
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/payment', $payload)
            ->assertStatus(201);
    }

    public function testCreatePaymentRequired()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payload = [
            "payment_id" => $m_payment->id,
            "date" => now(),
            "amount" => 200.00,
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/payment')
            ->assertStatus(422);
    }

    public function testGetPayment()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payment = factory(CommandePayment::class)->create([
            "commande_id" => $commande->id,
            "payment_id" => $m_payment->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/'.$commande->id.'/payment/'.$payment->id)
            ->assertStatus(201);
    }

    public function testUpdatePayment()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payment = factory(CommandePayment::class)->create([
            "commande_id" => $commande->id,
            "payment_id" => $m_payment->id
        ]);

        $payload = [
            "commande_id" => $commande->id,
            "payment_id" => $m_payment->id,
            "date" => now(),
            "amount" => 100.00,
            "state" => 0
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id.'/payment/'.$payment->id, $payload)
            ->assertStatus(201);
    }

    public function testDeletePayment()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $m_payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_777777777"
        ]);
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id
        ]);

        $payment = factory(CommandePayment::class)->create([
            "commande_id" => $commande->id,
            "payment_id" => $m_payment->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/commande/'.$commande->id.'/payment/'.$payment->id)
            ->assertStatus(201);
    }
}
