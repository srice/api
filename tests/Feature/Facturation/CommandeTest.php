<?php

namespace Tests\Feature\Facturation;

use App\Model\Comite\Comite;
use App\Model\Facturation\Commande\Commande;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommandeTest extends TestCase
{
    public function testListCommande()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        factory(Commande::class, 10)->create([
            "comite_id" => $comite->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/')
            ->assertStatus(201);
    }

    public function testCreateCommandeSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            "comite_id" => $comite->id,
            "date" => now()
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/', $payload)
            ->assertStatus(201);
    }

    public function testCreateCommandeRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            "comite_id" => $comite->id,
            "date" => now()
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/')
            ->assertStatus(422);
    }

    public function testGetCommande()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id,
            "date" => now()
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/facturation/commande/'.$commande->id)
            ->assertStatus(201);
    }

    public function testUpdateCommandeSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id,
            "date" => now()
        ]);
        $payload = [
            "comite_id" => $comite->id,
            "date" => now()->addDays(1)
        ];

        $this->actingAs($user, 'api')->json('post', '/api/facturation/commande/'.$commande->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteCommande()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $commande = factory(Commande::class)->create([
            "comite_id" => $comite->id,
            "date" => now()
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/facturation/commande/'.$commande->id)
            ->assertStatus(201);
    }
}
