<?php

namespace Tests\Feature\Comite;

use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    public function testCreateContactSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            'name' => "test contact",
            'email' => "test@contact.com",
            "position" => "test Position",
            "telephone" => "00 00 00 00 01"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/'.$comite->id.'/contact', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data'
            ]);
    }

    public function testCreateContactNoRequiredParam()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $payload = [
            'name' => "test contact",
            'email' => "test@contact.com",
            "telephone" => "00 00 00 00 01"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/'.$comite->id.'/contact', $payload)
            ->assertStatus(422);
    }

    public function testGetContact()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $contact = factory(Contact::class)->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/comite/'.$comite->id.'/contact/'.$contact->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'comite_id',
                    'user_id',
                    'position',
                    'telephone',
                    'comite',
                    'user'
                ]
            ]);
    }

    public function testUpdateContact()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $contact = factory(Contact::class)->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id
        ]);

        $payload = [
            "name" => "Modification de contact",
            "email" => "edit@contact.com",
            "position" => "Update Position",
            "telephone" => "00 00 00 00 03"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/'.$comite->id.'/contact/'.$contact->id, $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'comite_id',
                    'user_id',
                    'position',
                    'telephone'
                ]
            ]);
    }

    public function testDeleteContact()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $contact = factory(Contact::class)->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/comite/'.$comite->id.'/contact/'.$contact->id)
            ->assertStatus(201);
    }

    public function testFilliableContactUser()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $contact = factory(Contact::class)->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id
        ]);

        $testing = $user->contact()->first();

        $this->assertDatabaseHas('contacts', [
            'user_id' => $testing->user_id
        ]);
    }
}
