<?php

namespace Tests\Feature\Comite;

use App\Http\Controllers\Comite\ComiteController;
use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Stripe\Customer;
use Stripe\Stripe;
use Tests\TestCase;

class ComiteTest extends TestCase
{
    public function testCreateComiteCorrectly()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $payload = [
            "name" => "Comite de Test",
            "adresse" => "Rue du Test",
            "codePostal" => "00000",
            "ville" => "Ville de Test",
            "tel" => "00 00 00 00 00",
            "email" => "test@comite.com",
            "position" => "test de position",
            "telephone" => "00 00 00 00 01"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/create', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'adresse',
                    'codePostal',
                    'ville',
                    'tel',
                    'email',
                    'customerId'
                ],
            ]);
    }

    public function testCreateComiteRequireField()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();


        $this->actingAs($user, 'api')->json('post', '/api/comite/create')
            ->assertStatus(422)
            ->assertJson([
                'data' => "Erreur de Validation"
            ]);
    }

    public function testGetComite()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $contact = factory(Contact::class)->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/comite/'.$comite->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'adresse',
                    'codePostal',
                    'ville',
                    'tel',
                    'email',
                    'customerId',
                    'contacts',
                    'payments'
                ],
            ]);
    }

    public function testUpdateComiteSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);

        $payload = [
            "name" => "Update du comite",
            "adresse" => "Rue Test",
            "codePostal" => "00000",
            "ville" => "VilleTest",
            "tel"   => "00 00 00 00 00",
            "email" => "no@re.re"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/'.$comite->id, $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'adresse',
                    'codePostal',
                    'ville',
                    'tel',
                    'email',
                    'customerId',
                ],
            ]);
    }

    public function testDeleteComite()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/comite/'.$comite->id)
            ->assertStatus(201);
    }

}
