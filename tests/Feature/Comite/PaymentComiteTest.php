<?php

namespace Tests\Feature\Comite;

use App\Model\Comite\Comite;
use App\Model\Comite\Payment;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentComiteTest extends TestCase
{
    public function testCreatePaymentCardSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);

        $payload = [
            "methode" => "card",
            "card_number" => "4242424242424242",
            "exp_month" => "12",
            "exp_year" => "20",
            "cvc" => "123"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/' . $comite->id . '/payment', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'payment',
                'intent'
            ]);
    }

    public function testCreatePaymentIbanSuccess()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);

        $payload = [
            "methode" => "sepa_debit",
            "iban" => "FR1420041010050500013M02606"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/' . $comite->id . '/payment', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'payment',
                'intent'
            ]);
    }

    public function testCreatePaymentRequired()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);

        $payload = [
        ];

        $this->actingAs($user, 'api')->json('post', '/api/comite/' . $comite->id . '/payment', $payload)
            ->assertStatus(422);
    }

    public function testFilliablePaymentComite()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);
        $payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_1GWpMlAyWfYU9MAL2dcQ5AUJ"
        ]);

        $testing = $payment->comite()->first();

        $this->assertDatabaseHas('payments', [
            'comite_id' => $testing->id
        ]);
    }

    public function testGetPayment()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);
        $payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_1GWpMlAyWfYU9MAL2dcQ5AUJ"
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/comite/' . $comite->id . '/payment/'.$payment->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'payment',
                'intent'
            ]);
    }

    public function testDeletePayment()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create([
            "customerId" => "cus_H4wJsdvawuZ5lo"
        ]);
        $payment = factory(Payment::class)->create([
            "comite_id" => $comite->id,
            "stripe_id" => "pm_1GWpMlAyWfYU9MAL2dcQ5AUJ"
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/comite/' . $comite->id . '/payment/'.$payment->id)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data'
            ]);
    }
}
