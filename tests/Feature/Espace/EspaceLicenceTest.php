<?php

namespace Tests\Feature\Espace;

use App\Model\Comite\Comite;
use App\Model\Espace\Espace;
use App\Model\Espace\EspaceLicence;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Contrat\ContratFamille;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Module;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EspaceLicenceTest extends TestCase
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $user;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $comite;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $f_service;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $service;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $f_contrat;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $contrat;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $espace;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $module;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = factory(User::class)->create();
        $this->comite = factory(Comite::class)->create();
        $this->f_service = factory(Famille::class)->create();
        $this->module = factory(Module::class)->create();
        $this->service = factory(Service::class)->create([
            "famille_id" => $this->f_service->id
        ]);
        $this->f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $this->service->id
        ]);
        $this->contrat = factory(Contrat::class)->create([
            "comite_id" => $this->comite->id,
            "contrat_famille_id" => $this->f_contrat->id,
        ]);
        $this->espace = factory(Espace::class)->create([
            "comite_id" => $this->comite->id,
            "contrat_id" => $this->contrat->id
        ]);
    }

    public function testListLicence()
    {
        factory(EspaceLicence::class)->create([
            "espace_id" => $this->espace->id
        ]);

        $this->actingAs($this->user, 'api')->json('get', '/api/espace/'.$this->espace->id.'/licence')
            ->assertStatus(201);
    }

    public function testCreateLicenceSuccessWithoutModule()
    {
        $payload = [
            "espace_id" => $this->espace->id,
            "numero" => "XXXX-XXXX-XXXX-2020",
            "module" => false,
            "module_id" => null,
            "start" => now(),
            "end" => now()->addYear()
        ];

        $this->actingAs($this->user, 'api')->json('post', '/api/espace/'.$this->espace->id.'/licence', $payload)
            ->assertStatus(201);
    }

    public function testCreateLicenceSuccessWithModule()
    {
        $payload = [
            "espace_id" => $this->espace->id,
            "numero" => "XXXX-XXXX-XXXX-2020",
            "module" => true,
            "module_id" => $this->module->id,
            "start" => now(),
            "end" => now()->addYear()
        ];

        $this->actingAs($this->user, 'api')->json('post', '/api/espace/'.$this->espace->id.'/licence', $payload)
            ->assertStatus(201);
    }

    public function testCreateLicenceRequired()
    {
        $payload = [
            "espace_id" => $this->espace->id,
            "numero" => "XXXX-XXXX-XXXX-2020",
            "module" => true,
            "module_id" => $this->module->id,
            "start" => now(),
            "end" => now()->addYear()
        ];

        $this->actingAs($this->user, 'api')->json('post', '/api/espace/'.$this->espace->id.'/licence')
            ->assertStatus(422);
    }

    public function testGetLicence()
    {
        $licence = factory(EspaceLicence::class)->create([
            "espace_id" => $this->espace->id
        ]);

        $this->actingAs($this->user, 'api')->json('get', '/api/espace/'.$this->espace->id.'/licence/'.$licence->id)
            ->assertStatus(201);
    }

    public function testUpdateLicence()
    {
        $licence = factory(EspaceLicence::class)->create([
            "espace_id" => $this->espace->id
        ]);

        $payload = [
            "module" => true,
            "module_id" => $this->module->id
        ];

        $this->actingAs($this->user, 'api')->json('post', '/api/espace/'.$this->espace->id.'/licence/'.$licence->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteLicence()
    {
        $licence = factory(EspaceLicence::class)->create([
            "espace_id" => $this->espace->id
        ]);

        $this->actingAs($this->user, 'api')->json('delete', '/api/espace/'.$this->espace->id.'/licence/'.$licence->id)
            ->assertStatus(201);
    }
}
