<?php

namespace Tests\Feature\Espace;

use App\Model\Comite\Comite;
use App\Model\Espace\Espace;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Contrat\ContratFamille;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Service;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EspaceTest extends TestCase
{
    public function testListEspace()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);
        factory(Espace::class)->create([
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/espace')
            ->assertStatus(201);
    }

    public function testCreateEspaceSuccess()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);
        $payload = [
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id,
            "domain" => "test",
            "path" => "test"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/espace', $payload)
            ->assertStatus(201);
    }

    public function testCreateEspaceRequired()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);

        $this->actingAs($user, 'api')->json('post', '/api/espace')
            ->assertStatus(422);
    }

    public function testGetEspace()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);
        $espace = factory(Espace::class)->create([
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id
        ]);

        $this->actingAs($user, 'api')->json('get', '/api/espace/'.$espace->id)
            ->assertStatus(201);
    }

    public function testUpdateEspace()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);
        $espace = factory(Espace::class)->create([
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id
        ]);

        $payload = [
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id,
            "domain" => "test",
            "path" => "test"
        ];

        $this->actingAs($user, 'api')->json('post', '/api/espace/'.$espace->id, $payload)
            ->assertStatus(201);
    }

    public function testDeleteEspace()
    {
        $user = factory(User::class)->create();
        $comite = factory(Comite::class)->create();
        $f_service = factory(Famille::class)->create();
        $service = factory(Service::class)->create([
            "famille_id" => $f_service->id
        ]);
        $f_contrat = factory(ContratFamille::class)->create([
            "service_id" => $service->id
        ]);
        $contrat = factory(Contrat::class)->create([
            "comite_id" => $comite->id,
            "contrat_famille_id" => $f_contrat->id,
        ]);
        $espace = factory(Espace::class)->create([
            "comite_id" => $comite->id,
            "contrat_id" => $contrat->id
        ]);

        $this->actingAs($user, 'api')->json('delete', '/api/espace/'.$espace->id)
            ->assertStatus(201);
    }
}
