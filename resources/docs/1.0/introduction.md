# Introduction

## API Reference

Cette API est organiser sous REST. Elle à pour but de faire les liaison avec les programmes de l'environnement de SRICE.
Elle accepte les envoie codée de formulaire et renvoie une réponse encodé au format JSON.

### Endpoint
```curl
    Dev: https://api.srice.io/api/
    Prod: https://api.srice.eu/api/
```

### Packages
```text
composer require srice/api-wrapper
```
