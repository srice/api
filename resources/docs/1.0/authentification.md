# Authentification

- [Paramètre](#paramtre)
- [Appel](#appel)
- [Retour](#retour)

L'API de srice utilise le système de clé api obtenue après authentification
auprès du service sous l'appel ``/api/login`` avec les paramètres suivant:

<table>
    <thead>
        <tr>
            <th>METHOD</th>
            <th>URI</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>POST</td>
            <td>/api/login</td>
        </tr>
    </tbody>
</table>

## Paramètre

````json
{
  "email": "test@test.com",
  "password": "00000"
}
````

## Appel

### CURL
```curl
curl --location --request POST 'https://api.srice.io/api/login' \
--data-raw '{
    "email": "admin@test.com",
    "password": "0000"
}'
```

## Retour

```json
{
    "data": {
        "id": 1,
        "name": "administrator",
        "email": "admin@test.com",
        "email_verified_at": null,
        "created_at": "2020-04-24T10:26:13.000000Z",
        "updated_at": "2020-05-06T15:07:36.000000Z",
        "api_token": "Icg45i9I2M0PpTwADBQiGsRKX4Yt9VBylU6vCYepO2FTqHm34YDXrnKaKA4G"
    }
}
```

Le Champs ``api_token`` servira pour chaque appel API.

