# Comite

- [POST /Comite](#creation-dun-comitcreate-comite)

## [Creation d'un comité](#create-comite)

Elle permet d'enregistrer un comité.
- <b>Endpoint</b>: /comite
- <b>Methode</b>: POST

### REQUEST

#### Curl
```curl
curl --location --request POST 'https://api.srice.io/api/comite/create' \
--data-raw '{
    "name": "eius",
    "adresse": "voluptatem",
    "codePostal": "44000",
    "ville": "Nantes",
    "tel": "00 00 00 00 00",
    "email": "contact@comite.com",
    "position": "Relative",
    "telephone": "06 00 00 00 00"
}'
```

#### PHP
```php
\Srice\Api\Comite::create([
    "name" => "eius",
    "adresse" => "No Adresse",
    "codePostal" => "44000",
    "ville" => "Nantes",
    "tel" => "00 00 00 00 00",
    "email" => "contact@comite.com",
    "position" => "Relative",
    "telephone" => "06 00 00 00 00"
]);
```


### RETURN

```json
{
    "data": {
        "name": "eius",
        "adresse": "voluptatem",
        "codePostal": "44000",
        "ville": "Nantes",
        "tel": "00 00 00 00 00",
        "email": "contact@comite.com",
        "customerId": "cus_HEHtVpDiAtMvtD",
        "id": 1
    }
}
```


