# Erreur
L'api de Srice utilise les valeur de retour de status conventionnel du language de requete HTTP à savoir ``2xx`` pour les succès, ``4xx`` pour une erreur d'appel ou de champs et ``5xx`` pour une erreur serveur.

## Code de Status HTTP
<table>
    <thead>
        <tr>
            <th>CODE</th>
            <th>MESSAGE</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>201</td>
            <td>OK - L'appel ses bien passée et les valeur de retour ont été retournée.</td>
        </tr>
        <tr>
            <td>422</td>
            <td>Field Required - Un ou plusieur champs nécéssaires à la requete ne sont pas présent ou sont renseignée de manière incorrect.</td>
        </tr>
        <tr>
            <td>500</td>
            <td>Internal Error - Erreur d'appel serveur (Très rare)</td>
        </tr>
    </tbody>
</table>
