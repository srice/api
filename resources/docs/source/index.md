---
title: API Reference

language_tabs:
- bash
- javascript
- php

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://api.srice.io/docs/collection.json)

<!-- END_INFO -->

#Authentification


[Le système d'authentification fonctionne par la connexion Basic et les identifications sont transformer en un BearerToken]
<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Handle a registration request for the application.

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/register',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login
[Connexion de l&#039;utilisateur à l&#039;API]

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"admin@test.com","password":"0000"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "admin@test.com",
    "password": "0000"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/login',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'email' => 'admin@test.com',
            'password' => '0000',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": "Information de l'utilisateur avec api_token"
}
```

### HTTP Request
`POST api/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | Email de l'utilisateur.
        `password` | string |  required  | Mot de passe de l'utilisateur.
    
<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_61739f3220a224b34228600649230ad1 -->
## Logout
[Déconnexion de l&#039;utilisateur à l&#039;API]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/logout',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": "null"
}
```

### HTTP Request
`POST api/logout`


<!-- END_61739f3220a224b34228600649230ad1 -->

#Comite


<!-- START_9ae62e0b69707448e951d86e321a4b21 -->
## Create Comité
[Permet la création d&#039;un comite, Contact Primaire, Compte Stripe et Utilisateur Primaire]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/comite/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"dolores","adresse":"commodi","codePostal":"44000","ville":"Nantes","tel":"00 00 00 00 00","email":"contact@comite.com"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "dolores",
    "adresse": "commodi",
    "codePostal": "44000",
    "ville": "Nantes",
    "tel": "00 00 00 00 00",
    "email": "contact@comite.com"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/comite/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'dolores',
            'adresse' => 'commodi',
            'codePostal' => '44000',
            'ville' => 'Nantes',
            'tel' => '00 00 00 00 00',
            'email' => 'contact@comite.com',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "message": "Retourne un message d'erreur avec les erreurs de validations"
}
```
> Example response (201):

```json
{
    "message": "Retourne les informations du comité créer"
}
```

### HTTP Request
`POST api/comite/create`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Le nom du comité
        `adresse` | string |  required  | L'adresse du comité
        `codePostal` | string |  required  | Code Postal du comité (5 Digit).
        `ville` | string |  required  | Ville du comité.
        `tel` | string |  required  | Numéro de téléphone principal du comité.
        `email` | string |  required  | Email principal du comité.
    
<!-- END_9ae62e0b69707448e951d86e321a4b21 -->

<!-- START_56631c607ae198ac01ed24b3df65eef7 -->
## Get Comité
[Permet l&#039;affichage des informations usuel d&#039;un comité]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/comite/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/comite/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "message": "Retourne les informations du comité"
}
```

### HTTP Request
`GET api/comite/{comite_id}`


<!-- END_56631c607ae198ac01ed24b3df65eef7 -->

<!-- START_c315380cc79d9f4561953939b536fdc5 -->
## Update Comité
[Permet de mettre à jour les informations du comité en liaison avec stripe]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/comite/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Comit\u00e9 de Test -> Comit\u00e9 de Test 1","adresse":"molestiae","codePostal":"44000","ville":"Nantes","tel":"00 00 00 00 00","email":"contact@comite.com"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Comit\u00e9 de Test -> Comit\u00e9 de Test 1",
    "adresse": "molestiae",
    "codePostal": "44000",
    "ville": "Nantes",
    "tel": "00 00 00 00 00",
    "email": "contact@comite.com"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/comite/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Comité de Test -> Comité de Test 1',
            'adresse' => 'molestiae',
            'codePostal' => '44000',
            'ville' => 'Nantes',
            'tel' => '00 00 00 00 00',
            'email' => 'contact@comite.com',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "message": "Retourne les informations du comité mise à jour"
}
```

### HTTP Request
`POST api/comite/{comite_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Le nom du comité.
        `adresse` | string |  optional  | L'adresse du comité
        `codePostal` | string |  optional  | Code Postal du comité (5 Digit).
        `ville` | string |  optional  | Ville du comité.
        `tel` | string |  optional  | Numéro de téléphone principal du comité.
        `email` | string |  optional  | Email principal du comité.
    
<!-- END_c315380cc79d9f4561953939b536fdc5 -->

<!-- START_17e0d1fc9c3e90cafc64b57b50d9b6f2 -->
## Delete Comité
[Supprime l&#039;ensemble des informations d&#039;un comité]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/comite/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/comite/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "message": "Texte: Comite Supprimer"
}
```

### HTTP Request
`DELETE api/comite/{comite_id}`


<!-- END_17e0d1fc9c3e90cafc64b57b50d9b6f2 -->

#Comite/Contact

[Gestion des contacts d'un comité]
<!-- START_790f39a2730031f1248a1534973156d8 -->
## Create Contact
[Permet la création d&#039;un contact et utilisateur associé]
[Lors de son execution d&#039;un email est envoyer à l&#039;adresse mail avec le mot de passe générer aléatoirement]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/comite/1/contact" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Jean Dupond","email":"jdupond@sfr.fr","position":"Comptable","telephone":"06 00 00 00 00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/contact"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Jean Dupond",
    "email": "jdupond@sfr.fr",
    "position": "Comptable",
    "telephone": "06 00 00 00 00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/comite/1/contact',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Jean Dupond',
            'email' => 'jdupond@sfr.fr',
            'position' => 'Comptable',
            'telephone' => '06 00 00 00 00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "data": "Retourne l'exeption",
    "errors": "Retourne les erreurs de validation"
}
```
> Example response (201):

```json
{
    "message": "Retourne les informations du contact"
}
```

### HTTP Request
`POST api/comite/{comite_id}/contact`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom/Prénom du contact.
        `email` | string |  required  | Email du contact.
        `position` | string |  required  | Position du contact au sein du comité.
        `telephone` | string |  required  | Téléphone personnel du contact.
    
<!-- END_790f39a2730031f1248a1534973156d8 -->

<!-- START_9361d954d21924114011d9e61359f363 -->
## Get Contact
[Permet l&#039;affichage des informations usuel d&#039;un contact]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/comite/1/contact/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/contact/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/comite/1/contact/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "message": "Retourne les informations du contact"
}
```

### HTTP Request
`GET api/comite/{comite_id}/contact/{contact_id}`


<!-- END_9361d954d21924114011d9e61359f363 -->

<!-- START_b0ecedd50eb34e07b2d09fd629118140 -->
## Update Contact
[Permet de mettre à jours les informations d&#039;un contact]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/comite/1/contact/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Jean Dupond","email":"jdupond@sfr.fr","position":"Comptable","telephone":"06 00 00 00 00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/contact/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Jean Dupond",
    "email": "jdupond@sfr.fr",
    "position": "Comptable",
    "telephone": "06 00 00 00 00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/comite/1/contact/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Jean Dupond',
            'email' => 'jdupond@sfr.fr',
            'position' => 'Comptable',
            'telephone' => '06 00 00 00 00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "message": "Retourne les informations du contact"
}
```

### HTTP Request
`POST api/comite/{comite_id}/contact/{contact_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Nom/Prénom du contact.
        `email` | string |  optional  | Email du contact.
        `position` | string |  optional  | Position du contact au sein du comité.
        `telephone` | string |  optional  | Téléphone personnel du contact.
    
<!-- END_b0ecedd50eb34e07b2d09fd629118140 -->

<!-- START_cf5b30dd2f0caf8793d5c75b1bb83549 -->
## Delete Contact
[Supprime l&#039;ensemble des informations d&#039;un contact]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/comite/1/contact/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/contact/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/comite/1/contact/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
null
```

### HTTP Request
`DELETE api/comite/{comite_id}/contact/{contact_id}`


<!-- END_cf5b30dd2f0caf8793d5c75b1bb83549 -->

#Comite/Payment


<!-- START_1cfd50087dd40273582638a51844dbb7 -->
## Create Payment
[Création du mode de paiement pour un comité particulier.&lt;br&gt;Un choix doit être fais entre carte bancaire ou IBAN]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/comite/1/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"methode":"card","card_number":"4242 4242 4242 4242","exp_month":4,"exp_year":20,"cvc":123,"iban":"FR17 1523 9652 9632 8990 1235 123"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "methode": "card",
    "card_number": "4242 4242 4242 4242",
    "exp_month": 4,
    "exp_year": 20,
    "cvc": 123,
    "iban": "FR17 1523 9652 9632 8990 1235 123"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/comite/1/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'methode' => 'card',
            'card_number' => '4242 4242 4242 4242',
            'exp_month' => 4,
            'exp_year' => 20,
            'cvc' => 123,
            'iban' => 'FR17 1523 9652 9632 8990 1235 123',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "data": "Message Exception",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "payment": "Information relative au mode de paiement",
    "intent": "Intention STIPE du mode de paiement"
}
```

### HTTP Request
`POST api/comite/{comite_id}/payment`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `methode` | string |  required  | Methode de paiement(card || sepa_debit).
        `card_number` | string |  optional  | (Valid) Numéro de la carte bancaire.
        `exp_month` | integer |  optional  | (Valid) Mois de validité de la carte bancaire.
        `exp_year` | integer |  optional  | (Valid) Année de validité de la carte bancaire.
        `cvc` | integer |  optional  | (Valid) Code de validité de la carte bancaire.
        `iban` | string |  optional  | (Valid) Iban du compte bancaire.
    
<!-- END_1cfd50087dd40273582638a51844dbb7 -->

<!-- START_84d99be637689946064a9c1d140bdea9 -->
## Get Payment
[Affiche les informations relative au mode de paiement]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/comite/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/comite/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "payment": "Information relative au mode de paiement",
    "intent": "Intention STIPE du mode de paiement"
}
```

### HTTP Request
`GET api/comite/{comite_id}/payment/{payment_id}`


<!-- END_84d99be637689946064a9c1d140bdea9 -->

<!-- START_6b38e8c50ac9523846326e03dea420ea -->
## Delete Payment
[Supprime le mode de paiement]

> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/comite/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/comite/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/comite/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "data": "null"
}
```

### HTTP Request
`DELETE api/comite/{comite_id}/payment/{payment_id}`


<!-- END_6b38e8c50ac9523846326e03dea420ea -->

#Espace


<!-- START_dc62b2dc078c9701a6fffa27cf8acfd0 -->
## List Espace
Liste des espaces

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace`


<!-- END_dc62b2dc078c9701a6fffa27cf8acfd0 -->

<!-- START_5c5bf947ff1ef3ce88f32db651219296 -->
## Create Espace
Création de l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"contrat_id":1,"domain":"test","path":"test"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "contrat_id": 1,
    "domain": "test",
    "path": "test"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'contrat_id' => 1,
            'domain' => 'test',
            'path' => 'test',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  required  | ID du comité affilier à l'espace en construction.
        `contrat_id` | integer |  required  | ID du contrat affilier à l'espace en construction.
        `domain` | string |  required  | Nom de domaine sans extension.
        `path` | string |  required  | Chemin de l'application.
    
<!-- END_5c5bf947ff1ef3ce88f32db651219296 -->

<!-- START_f50593cf30b16070e0882d0c695d0fa4 -->
## Get Espace
Information d&#039;un espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}`


<!-- END_f50593cf30b16070e0882d0c695d0fa4 -->

<!-- START_becebfd7a691fa0d6ba7e7e8286d4378 -->
## Update Espace
Mise à jour de l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"contrat_id":1,"domain":"test","path":"test","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "contrat_id": 1,
    "domain": "test",
    "path": "test",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'contrat_id' => 1,
            'domain' => 'test',
            'path' => 'test',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  optional  | ID du comité affilier à l'espace en construction.
        `contrat_id` | integer |  optional  | ID du contrat affilier à l'espace en construction.
        `domain` | string |  optional  | Nom de domaine sans extension.
        `path` | string |  optional  | Chemin de l'application.
        `state` | integer |  optional  | Etat actuel de l'application | 0: Hors Ligne <br> 1: Maintenance <br> 2: En Ligne <br> 3: n Maintenance <br> 4: Expirer.
    
<!-- END_becebfd7a691fa0d6ba7e7e8286d4378 -->

<!-- START_bacd7cb4c786cfafb8e554edd2adf6a7 -->
## Delete Espace
Supprime un espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/espace/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/espace/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/espace/{espace_id}`


<!-- END_bacd7cb4c786cfafb8e554edd2adf6a7 -->

#Espace/Install


<!-- START_40569ab8729d0bcb3432725d7a3f5a5c -->
## List Install
Liste des étapes d&#039;installation de l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/install" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/install"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/install',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/install`


<!-- END_40569ab8729d0bcb3432725d7a3f5a5c -->

<!-- START_37e0eedfa888c69f566660cecf8fc14c -->
## Create Install
Création des étapes d&#039;installation d&#039;un espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/install" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"description":"D\u00e9claration du domaine"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/install"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "description": "D\u00e9claration du domaine"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/install',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'description' => 'Déclaration du domaine',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/install`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `description` | string |  required  | Description de l'étape.
    
<!-- END_37e0eedfa888c69f566660cecf8fc14c -->

<!-- START_613620b442ddef4a28e597d228e38a65 -->
## Get Install
Information sur une étape

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/install/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/install/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/install/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/install/{install_id}`


<!-- END_613620b442ddef4a28e597d228e38a65 -->

<!-- START_0680adf5ff52d693bf5b5f701d2051c9 -->
## Update Install
Mise à jour d&#039;une étape de l&#039;installation

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/install/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"description":"D\u00e9claration du domaine"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/install/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "description": "D\u00e9claration du domaine"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/install/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'description' => 'Déclaration du domaine',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/install/{install_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `description` | string |  required  | Description de l'étape.
    
<!-- END_0680adf5ff52d693bf5b5f701d2051c9 -->

<!-- START_1c9cb75f2c233fea52c5ed31afc98c61 -->
## Delete Install
Supprime une étape de l&#039;installation

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/espace/1/install/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/install/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/espace/1/install/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/espace/{espace_id}/install/{install_id}`


<!-- END_1c9cb75f2c233fea52c5ed31afc98c61 -->

#Espace/Licence


<!-- START_a114cc69abbbacb1ab5c1af391ad7f50 -->
## List Licence
Liste des licences affilier à l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/licence" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/licence"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/licence',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/licence`


<!-- END_a114cc69abbbacb1ab5c1af391ad7f50 -->

<!-- START_52fdb3e8933096694f6bb9c468e8589e -->
## Create Licence
Création d&#039;une licence affilier à l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/licence" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"module":false,"module_id":1,"start":"2020-04-23 14:00:00","end":"2021-04-23 14:00:00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/licence"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "module": false,
    "module_id": 1,
    "start": "2020-04-23 14:00:00",
    "end": "2021-04-23 14:00:00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/licence',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'module' => false,
            'module_id' => 1,
            'start' => '2020-04-23 14:00:00',
            'end' => '2021-04-23 14:00:00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/licence`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `module` | boolean |  required  | Définie sir la licence est affilier également à un module..
        `module_id` | integer |  optional  | ID du module affilier à la licence.
        `start` | date |  required  | Date de début de la licence.
        `end` | date |  required  | Date de fin de la licence.
    
<!-- END_52fdb3e8933096694f6bb9c468e8589e -->

<!-- START_02c86585f16aee44bcf295d8bed4d6f9 -->
## Get Licence
Affiche les information d&#039;une licence

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/licence/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/licence/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/licence/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/licence/{licence_id}`


<!-- END_02c86585f16aee44bcf295d8bed4d6f9 -->

<!-- START_7f3780a0e00dcac58b6fb3e16a2f9d19 -->
## Update Licence
Mise à jour d&#039;une licence spécifique

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/licence/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"module":false,"module_id":1,"start":"2020-04-23 14:00:00","end":"2021-04-23 14:00:00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/licence/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "module": false,
    "module_id": 1,
    "start": "2020-04-23 14:00:00",
    "end": "2021-04-23 14:00:00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/licence/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'module' => false,
            'module_id' => 1,
            'start' => '2020-04-23 14:00:00',
            'end' => '2021-04-23 14:00:00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/licence/{licence_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `module` | boolean |  optional  | Définie sir la licence est affilier également à un module..
        `module_id` | integer |  optional  | ID du module affilier à la licence.
        `start` | date |  optional  | Date de début de la licence.
        `end` | date |  optional  | Date de fin de la licence.
    
<!-- END_7f3780a0e00dcac58b6fb3e16a2f9d19 -->

<!-- START_9ed494b491a6441937b0d3adb6d30762 -->
## Delete Licence
Supprime une licence

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/espace/1/licence/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/licence/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/espace/1/licence/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/espace/{espace_id}/licence/{licence_id}`


<!-- END_9ed494b491a6441937b0d3adb6d30762 -->

#Espace/Module


<!-- START_4a4ea05440a7a108bd7a3accda205301 -->
## List Module
Liste des module affilier à l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/module" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/module"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/module',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/module`


<!-- END_4a4ea05440a7a108bd7a3accda205301 -->

<!-- START_ef9a5b2841986b59f12dc3e0505d2a7d -->
## Create Module
Création de la liste des modules affilier à l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/module" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/module"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/module',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/module`


<!-- END_ef9a5b2841986b59f12dc3e0505d2a7d -->

<!-- START_2d221df66f97d35f992bc931eeeed529 -->
## Get Module
Affiche les information relative à un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/module/{module_id}`


<!-- END_2d221df66f97d35f992bc931eeeed529 -->

<!-- START_b5e3c9afe8eaecea8af571254ace96b0 -->
## Update Module
Mise à jour des informations d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/module/{module_id}`


<!-- END_b5e3c9afe8eaecea8af571254ace96b0 -->

<!-- START_566bd18fabec881d37e28362a4454ade -->
## Delete Module
Supprime un module de l&#039;espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/espace/1/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/espace/1/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/espace/{espace_id}/module/{module_id}`


<!-- END_566bd18fabec881d37e28362a4454ade -->

#Espace/Service


<!-- START_fb4baa19230421f37918707293469221 -->
## List Service
Liste les services d&#039;un espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/service',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/service`


<!-- END_fb4baa19230421f37918707293469221 -->

<!-- START_eae085464bb97401fe11b79979f568b0 -->
## Create Service
Création d&#039;un service affilier à un espace

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"server_id":1,"database":"test"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "server_id": 1,
    "database": "test"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/service',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'server_id' => 1,
            'database' => 'test',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/service`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `server_id` | integer |  required  | ID du serveur ou est installer l'application.
        `database` | string |  required  | Nom de la base de donnée.
    
<!-- END_eae085464bb97401fe11b79979f568b0 -->

<!-- START_5fafb725b8ce2f9ce1186ff522a4f8f6 -->
## Get Service
Affiche les information d&#039;un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/espace/1/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/espace/1/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/espace/{espace_id}/service/{service_id}`


<!-- END_5fafb725b8ce2f9ce1186ff522a4f8f6 -->

<!-- START_5e71c2dce9ac0e0d8445dce57ecb379b -->
## Update Service
Met à jour les information relative à un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/espace/1/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"server_id":1,"database":"test"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "server_id": 1,
    "database": "test"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/espace/1/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'server_id' => 1,
            'database' => 'test',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/espace/{espace_id}/service/{service_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `server_id` | integer |  optional  | ID du serveur ou est installer l'application.
        `database` | string |  optional  | Nom de la base de donnée.
    
<!-- END_5e71c2dce9ac0e0d8445dce57ecb379b -->

<!-- START_48bf2ed27ef55cbf1f91673272d25230 -->
## Delete Service
Supprime un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/espace/1/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/espace/1/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/espace/1/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/espace/{espace_id}/service/{service_id}`


<!-- END_48bf2ed27ef55cbf1f91673272d25230 -->

#Facturation/Commande


<!-- START_dc1ba7cf1d8246f3c8523df348a7373d -->
## List Commande
Liste des Commandes

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "commande": "Liste des commandes"
}
```

### HTTP Request
`GET api/facturation/commande`


<!-- END_dc1ba7cf1d8246f3c8523df348a7373d -->

<!-- START_9e53b3cc4e4c91ab9580c1d4241efee0 -->
## Create Commande
Création d&#039;une commande à vide

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"date":"2020-04-18 21:13:00","espaceCheck":1,"esapce_id":1}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "date": "2020-04-18 21:13:00",
    "espaceCheck": 1,
    "esapce_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'date' => '2020-04-18 21:13:00',
            'espaceCheck' => 1,
            'esapce_id' => 1,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Message d'exception de validation",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "commande": "Commande crée"
}
```

### HTTP Request
`POST api/facturation/commande`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  required  | Identifiant d'un comite affilier à la commande.
        `date` | date |  required  | Date de la commande.
        `espaceCheck` | integer |  optional  | La commande est t-elle affilier à un espace.
        `esapce_id` | integer |  optional  | Identifiant de l'esapce affilier à la commande.
    
<!-- END_9e53b3cc4e4c91ab9580c1d4241efee0 -->

<!-- START_72c2795db9f239d81b7d55a1bcd8ad85 -->
## Get Commande
Affiche les informations d&#039;une commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "commande": "Information de la commande"
}
```

### HTTP Request
`GET api/facturation/commande/{commande_id}`


<!-- END_72c2795db9f239d81b7d55a1bcd8ad85 -->

<!-- START_7c50148d8afe9e9effe295c85f54a13b -->
## Update Commande
Mise à jour de la commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"date":"2020-04-18 21:13:00","espaceCheck":1,"esapce_id":1}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "date": "2020-04-18 21:13:00",
    "espaceCheck": 1,
    "esapce_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'date' => '2020-04-18 21:13:00',
            'espaceCheck' => 1,
            'esapce_id' => 1,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "commande": "Information de la commande mise à jour"
}
```

### HTTP Request
`POST api/facturation/commande/{commande_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  optional  | Identifiant d'un comite affilier à la commande.
        `date` | date |  optional  | Date de la commande.
        `espaceCheck` | integer |  optional  | La commande est t-elle affilier à un espace.
        `esapce_id` | integer |  optional  | Identifiant de l'esapce affilier à la commande.
    
<!-- END_7c50148d8afe9e9effe295c85f54a13b -->

<!-- START_aa05cea674d657289cfe2c5a056d59de -->
## Delete Commande
Supprime la commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/commande/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/commande/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "commande": "Commande "
}
```

### HTTP Request
`DELETE api/facturation/commande/{commande_id}`


<!-- END_aa05cea674d657289cfe2c5a056d59de -->

#Facturation/Commande/Item


<!-- START_5e51999ca696d4e09b5cbb69f27cb415 -->
## List Commande Items
Liste des articles d&#039;une commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande/1/item" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/item"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande/1/item',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "items": "liste des articles d'une commande"
}
```

### HTTP Request
`GET api/facturation/commande/{commande_id}/item`


<!-- END_5e51999ca696d4e09b5cbb69f27cb415 -->

<!-- START_1d55744614858d47af2780a97be8c70b -->
## Add Item
Ajoute un article à la commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande/1/item" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"service_tarif_id":1,"description":"Lorem","quantite":1,"amount":12}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/item"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "service_tarif_id": 1,
    "description": "Lorem",
    "quantite": 1,
    "amount": 12
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande/1/item',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'service_tarif_id' => 1,
            'description' => 'Lorem',
            'quantite' => 1,
            'amount' => 12,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Message d'exception de validation",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "item": "Article crée"
}
```

### HTTP Request
`POST api/facturation/commande/{commande_id}/item`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  required  | Identifiant du service.
        `service_tarif_id` | integer |  required  | Identifiant du tarif du service.
        `description` | string |  optional  | Description supplémentaire du service.
        `quantite` | integer |  required  | Quantité de l'article.
        `amount` | integer |  required  | Montant total de l'article.
    
<!-- END_1d55744614858d47af2780a97be8c70b -->

<!-- START_d49e1351c597a556b9b2812b44c59d74 -->
## Get Item
Information d&#039;un article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "item": "Information d'un article"
}
```

### HTTP Request
`GET api/facturation/commande/{commande_id}/item/{item_id}`


<!-- END_d49e1351c597a556b9b2812b44c59d74 -->

<!-- START_477e23286870d43bc316daeea75eed39 -->
## Update Item
Mise à jour d&#039;un article de la commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"service_tarif_id":1,"description":"Lorem","quantite":1,"amount":12}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "service_tarif_id": 1,
    "description": "Lorem",
    "quantite": 1,
    "amount": 12
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'service_tarif_id' => 1,
            'description' => 'Lorem',
            'quantite' => 1,
            'amount' => 12,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "item": "Information d'un article"
}
```

### HTTP Request
`POST api/facturation/commande/{commande_id}/item/{item_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  optional  | Identifiant du service.
        `service_tarif_id` | integer |  optional  | Identifiant du tarif du service.
        `description` | string |  optional  | Description supplémentaire du service.
        `quantite` | integer |  optional  | Quantité de l'article.
        `amount` | integer |  optional  | Montant total de l'article.
    
<!-- END_477e23286870d43bc316daeea75eed39 -->

<!-- START_dd608c73689271edd1b536abea8f07be -->
## Delete Item
Supprime un article d&#039;une commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/commande/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/commande/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "item": "Article supprimée"
}
```

### HTTP Request
`DELETE api/facturation/commande/{commande_id}/item/{item_id}`


<!-- END_dd608c73689271edd1b536abea8f07be -->

#Facturation/Commande/Payment


<!-- START_6f052770c54a4e5f0eb51a4aa7c52636 -->
## List Commande Payment
Liste des paiements par commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande/1/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande/1/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "payments": "Liste des Payments"
}
```

### HTTP Request
`GET api/facturation/commande/{commande_id}/payment`


<!-- END_6f052770c54a4e5f0eb51a4aa7c52636 -->

<!-- START_160c154d81003084e3f9044b0f307dec -->
## Create Payment
Création d&#039;un payment pour la commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande/1/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"payment_id":1,"date":"2020-04-19 18:00:00","amount":"120.00","state":1}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "payment_id": 1,
    "date": "2020-04-19 18:00:00",
    "amount": "120.00",
    "state": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande/1/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'payment_id' => 1,
            'date' => '2020-04-19 18:00:00',
            'amount' => '120.00',
            'state' => 1,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Message Exception de validation",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "payment": "Payment crée"
}
```

### HTTP Request
`POST api/facturation/commande/{commande_id}/payment`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `payment_id` | integer |  required  | Mode de règlement enregistrer par le comité.
        `date` | datetime |  required  | Date du règlement.
        `amount` | string |  required  | Montant du règlement.
        `state` | integer |  optional  | Etat du règlement.
    
<!-- END_160c154d81003084e3f9044b0f307dec -->

<!-- START_293d4414f4a82cb8b43d46378e6feaeb -->
## Get Payment
Information sur un payment particulier

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/commande/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/commande/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "payment": "Payment"
}
```

### HTTP Request
`GET api/facturation/commande/{commande_id}/payment/{payment_id}`


<!-- END_293d4414f4a82cb8b43d46378e6feaeb -->

<!-- START_0349d2d84ddcc66b91dceaf31f568dc3 -->
## Update Payment
Mise à jour d&#039;un payment

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/commande/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/commande/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "payment": "Payment mise à jours"
}
```

### HTTP Request
`POST api/facturation/commande/{commande_id}/payment/{payment_id}`


<!-- END_0349d2d84ddcc66b91dceaf31f568dc3 -->

<!-- START_037719fb38a2e6984c537fe4da6f3c17 -->
## Delete Payment
Supprime un payment d&#039;une commande

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/commande/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/commande/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/commande/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "payment": "Payment supprimée"
}
```

### HTTP Request
`DELETE api/facturation/commande/{commande_id}/payment/{payment_id}`


<!-- END_037719fb38a2e6984c537fe4da6f3c17 -->

#Facturation/Contrat


<!-- START_8e730fcbeaa1f9981e95fdb2f00fdfd3 -->
## List Contrat
Liste des contrats

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/contrat/contrat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/contrat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/contrat/contrat',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/contrat/contrat`


<!-- END_8e730fcbeaa1f9981e95fdb2f00fdfd3 -->

<!-- START_88da8d83d864ebffd5e7ba45f736f3cc -->
## Create Contrat
Création d&#039;un contrat. Il peut être affilier à une commande.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/contrat/contrat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"contrat_famille_id":1,"commande_id":1,"start":"2020-04-22 15:12:00","end":"2021-04-22 00:00:00","description":"Lorem","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/contrat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "contrat_famille_id": 1,
    "commande_id": 1,
    "start": "2020-04-22 15:12:00",
    "end": "2021-04-22 00:00:00",
    "description": "Lorem",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/contrat/contrat',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'contrat_famille_id' => 1,
            'commande_id' => 1,
            'start' => '2020-04-22 15:12:00',
            'end' => '2021-04-22 00:00:00',
            'description' => 'Lorem',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/contrat/contrat`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  required  | ID du comité affilier au contrat.
        `contrat_famille_id` | integer |  required  | ID de la famille de contrat.
        `commande_id` | integer |  optional  | ID de la commande affilier au contrat.
        `start` | date |  required  | Date de début du contrat.
        `end` | date |  required  | Date de fin du contrat.
        `description` | string |  optional  | Description facultative du contrat.
        `state` | integer |  optional  | Etat du contrat | 0: Brouillon <br> 1: Valider <br> 2: En attente de signature client <br> 3: Executer <br> 4: Bientôt expirer <br> 5: Expirer <br> 6: Résilier.
    
<!-- END_88da8d83d864ebffd5e7ba45f736f3cc -->

<!-- START_a00bb1a96b63fe0a9f5a8f6fc3194b75 -->
## Get Contrat
Information du contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/contrat/contrat/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/contrat/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/contrat/contrat/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/contrat/contrat/{contrat_id}`


<!-- END_a00bb1a96b63fe0a9f5a8f6fc3194b75 -->

<!-- START_32bf102b96ce8907cf5fe0a9bf024c4a -->
## Update Contrat
Mise à jour d&#039;un contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/contrat/contrat/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"contrat_famille_id":1,"commande_id":1,"start":"2020-04-22 15:12:00","end":"2021-04-22 00:00:00","description":"Lorem","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/contrat/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "contrat_famille_id": 1,
    "commande_id": 1,
    "start": "2020-04-22 15:12:00",
    "end": "2021-04-22 00:00:00",
    "description": "Lorem",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/contrat/contrat/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'contrat_famille_id' => 1,
            'commande_id' => 1,
            'start' => '2020-04-22 15:12:00',
            'end' => '2021-04-22 00:00:00',
            'description' => 'Lorem',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/contrat/contrat/{contrat_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  optional  | ID du comité affilier au contrat.
        `contrat_famille_id` | integer |  optional  | ID de la famille de contrat.
        `commande_id` | integer |  optional  | ID de la commande affilier au contrat.
        `start` | date |  optional  | Date de début du contrat.
        `end` | date |  optional  | Date de fin du contrat.
        `description` | string |  optional  | Description facultative du contrat.
        `state` | integer |  optional  | Etat du contrat | 0: Brouillon <br> 1: Valider <br> 2: En attente de signature client <br> 3: Executer <br> 4: Bientôt expirer <br> 5: Expirer <br> 6: Résilier.
    
<!-- END_32bf102b96ce8907cf5fe0a9bf024c4a -->

<!-- START_6ce2f0c869503ea9c159bc235fd3376b -->
## Delete Contrat
Supprime le contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/contrat/contrat/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/contrat/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/contrat/contrat/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/facturation/contrat/contrat/{contrat_id}`


<!-- END_6ce2f0c869503ea9c159bc235fd3376b -->

#Facturation/Contrat/Famille


<!-- START_44d4bf69a3f7541622d83b3b801a66bc -->
## List Famille
Liste des familles de contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/contrat/famille" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/contrat/famille',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/contrat/famille`


<!-- END_44d4bf69a3f7541622d83b3b801a66bc -->

<!-- START_1a07940c673cbad3f3b05e4712532cc0 -->
## Create Famille
Création d&#039;une famille de contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/contrat/famille" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"name":"Acc\u00e8s au service"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "name": "Acc\u00e8s au service"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/contrat/famille',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'name' => 'Accès au service',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/contrat/famille`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  required  | ID du service associé à la famille de contrat.
        `name` | string |  required  | Désignation de la famille.
    
<!-- END_1a07940c673cbad3f3b05e4712532cc0 -->

<!-- START_1f2252c07b84ec3d004d97eefb6ad26f -->
## Get Famille
Affiche les information de la famille de contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/contrat/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/contrat/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/contrat/famille/{famille_id}`


<!-- END_1f2252c07b84ec3d004d97eefb6ad26f -->

<!-- START_1364d71854b9e81a050335deb9c95731 -->
## Update Famille
Met à jour la famille de contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/contrat/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"name":"Acc\u00e8s au service"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "name": "Acc\u00e8s au service"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/contrat/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'name' => 'Accès au service',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/contrat/famille/{famille_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  optional  | ID du service associé à la famille de contrat.
        `name` | string |  optional  | Désignation de la famille.
    
<!-- END_1364d71854b9e81a050335deb9c95731 -->

<!-- START_46e4e5debbb1fb4eec4493ec1d8047d0 -->
## Delete Famille
Supprime la famille de contrat

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/contrat/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/contrat/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/facturation/contrat/famille/{famille_id}`


<!-- END_46e4e5debbb1fb4eec4493ec1d8047d0 -->

<!-- START_9d1490a1fc0cc05a25668fb98a06310e -->
## api/facturation/contrat/famille/{famille_id}/contrats
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/contrat/famille/1/contrats" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/contrat/famille/1/contrats"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/contrat/famille/1/contrats',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/contrat/famille/{famille_id}/contrats`


<!-- END_9d1490a1fc0cc05a25668fb98a06310e -->

#Facturation/Facture


<!-- START_08833fcb0532a21f16face030963aff3 -->
## List Factures
Listing des factures

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture`


<!-- END_08833fcb0532a21f16face030963aff3 -->

<!-- START_5c9cc01b44e8f89c3aceea650b48d080 -->
## Create facture
Création d&#039;une facture | Peut être affilier à une commande
Note: La facture deviens automatiquement impayer après 7 jours.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"commande_id":1,"comite_id":1,"date":"2020-04-22 10:58:00","amount":"120.00","state":120}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "commande_id": 1,
    "comite_id": 1,
    "date": "2020-04-22 10:58:00",
    "amount": "120.00",
    "state": 120
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'commande_id' => 1,
            'comite_id' => 1,
            'date' => '2020-04-22 10:58:00',
            'amount' => '120.00',
            'state' => 120,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `commande_id` | integer |  optional  | ID de la commande affilier.
        `comite_id` | integer |  required  | ID du comite.
        `date` | string |  required  | Date de la facture.
        `amount` | string |  required  | Montant de la facture.
        `state` | integer |  optional  | Etat actuelle de la facture | 0: Brouillon <br> 1: Valider <br> 2: Non Payer <br> 3: Partiellement Payer <br> 4: Payer <br> 5: Impayer.
    
<!-- END_5c9cc01b44e8f89c3aceea650b48d080 -->

<!-- START_9bdb7557ed3e437aef0c14ff2753eeee -->
## Get Facture
Affiche les informations de la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture/{facture_id}`


<!-- END_9bdb7557ed3e437aef0c14ff2753eeee -->

<!-- START_059fe742662643bb730bdd8428ea599d -->
## Update Facture
Mise à jour de la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"comite_id":1,"date":"2020-04-22 10:58:00","amount":"120.00","state":120}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "comite_id": 1,
    "date": "2020-04-22 10:58:00",
    "amount": "120.00",
    "state": 120
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'comite_id' => 1,
            'date' => '2020-04-22 10:58:00',
            'amount' => '120.00',
            'state' => 120,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture/{facture_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `comite_id` | integer |  optional  | ID du comite.
        `date` | string |  optional  | Date de la facture.
        `amount` | string |  optional  | Montant de la facture.
        `state` | integer |  optional  | Etat actuelle de la facture | 0: Brouillon <br> 1: Valider <br> 2: Non Payer <br> 3: Partiellement Payer <br> 4: Payer <br> 5: Impayer.
    
<!-- END_059fe742662643bb730bdd8428ea599d -->

<!-- START_e8ad281dfbc956dfc3e0e83c7114c293 -->
## Delete Facture
Supprime la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/facture/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/facture/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/facturation/facture/{facture_id}`


<!-- END_e8ad281dfbc956dfc3e0e83c7114c293 -->

#Facturation/Facture/Item


<!-- START_822f9e0d57475c2d7d626242b10ecc3f -->
## List Items
Liste les articles d&#039;une facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture/1/item" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/item"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture/1/item',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture/{facture_id}/item`


<!-- END_822f9e0d57475c2d7d626242b10ecc3f -->

<!-- START_5cb08abb92f64a56a1380c66fe628f92 -->
## Create Item
Ajout d&#039;un article dans la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture/1/item" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"description":"Lorem || null","quantite":1,"amount":"120.00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/item"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "description": "Lorem || null",
    "quantite": 1,
    "amount": "120.00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture/1/item',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'description' => 'Lorem || null',
            'quantite' => 1,
            'amount' => '120.00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture/{facture_id}/item`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  required  | ID du service associé à l'article.
        `description` | string |  optional  | Description supplémentaire de l'article.
        `quantite` | integer |  required  | Quantite de l'article.
        `amount` | string |  required  | Monta total de l'article.
    
<!-- END_5cb08abb92f64a56a1380c66fe628f92 -->

<!-- START_cad106467dfe3434d5001da5f96616d8 -->
## Get Item
Information de l&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture/{facture_id}/item/{item_id}`


<!-- END_cad106467dfe3434d5001da5f96616d8 -->

<!-- START_c030f908d035f5113d25b323c7eff23c -->
## Update Item
Mise à jour de l&#039;article de la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_id":1,"description":"Lorem || null","quantite":1,"amount":"120.00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_id": 1,
    "description": "Lorem || null",
    "quantite": 1,
    "amount": "120.00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'service_id' => 1,
            'description' => 'Lorem || null',
            'quantite' => 1,
            'amount' => '120.00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture/{facture_id}/item/{item_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_id` | integer |  optional  | ID du service associé à l'article.
        `description` | string |  optional  | Description supplémentaire de l'article.
        `quantite` | integer |  optional  | Quantite de l'article.
        `amount` | string |  optional  | Monta total de l'article.
    
<!-- END_c030f908d035f5113d25b323c7eff23c -->

<!-- START_f57391afc36f133a079ade2e31c48bde -->
## Delete Item
Supprime un article de la facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/facture/1/item/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/item/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/facture/1/item/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/facturation/facture/{facture_id}/item/{item_id}`


<!-- END_f57391afc36f133a079ade2e31c48bde -->

#Facturation/Facture/Payment


<!-- START_fa0628a8454ce028940f421dbf27bc67 -->
## List Payment
Liste des paiement affilier à une facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture/1/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture/1/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture/{facture_id}/payment`


<!-- END_fa0628a8454ce028940f421dbf27bc67 -->

<!-- START_d3ef97a7f78af4c656b8561d916a0584 -->
## Create Payment
Création d&#039;un paiement d&#039;une facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture/1/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"payment_id":1,"date":"2020-04-22 12:30:00","amount":"120.00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "payment_id": 1,
    "date": "2020-04-22 12:30:00",
    "amount": "120.00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture/1/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'payment_id' => 1,
            'date' => '2020-04-22 12:30:00',
            'amount' => '120.00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture/{facture_id}/payment`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `payment_id` | integer |  required  | ID du mode de paiement `Comite`.
        `date` | date |  required  | Date du paiement.
        `amount` | string |  required  | Montant du paiement.
        `state` | integer |  optional  | Etat du paiement | 0: En cours d'exécution <br> 1: Executer <br> 2: Erreur de paiement.
    
<!-- END_d3ef97a7f78af4c656b8561d916a0584 -->

<!-- START_855d81e909cdf81ebfe22260e7e91dcd -->
## Get payment
Information sur le paiement d&#039;une facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/facturation/facture/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/facturation/facture/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/facturation/facture/{facture_id}/payment/{payment_id}`


<!-- END_855d81e909cdf81ebfe22260e7e91dcd -->

<!-- START_de1382b46c7e05e79076f7beb396edd3 -->
## Update Payment
Mise à jour d&#039;un paiement d&#039;une facture

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/facturation/facture/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"payment_id":1,"date":"2020-04-22 12:30:00","amount":"120.00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "payment_id": 1,
    "date": "2020-04-22 12:30:00",
    "amount": "120.00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/facturation/facture/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'payment_id' => 1,
            'date' => '2020-04-22 12:30:00',
            'amount' => '120.00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/facturation/facture/{facture_id}/payment/{payment_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `payment_id` | integer |  optional  | ID du mode de paiement `Comite`.
        `date` | date |  optional  | Date du paiement.
        `amount` | string |  optional  | Montant du paiement.
        `state` | integer |  optional  | Etat du paiement | 0: En cours d'exécution <br> 1: Executer <br> 2: Erreur de paiement.
    
<!-- END_de1382b46c7e05e79076f7beb396edd3 -->

<!-- START_e4434b98aff6a7237ed2874f60299888 -->
## Delete Payment
Supprime un paiement d&#039;une facture
Note: Suppression possible uniquement si le paiement est en cours d&#039;execution (0)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/facturation/facture/1/payment/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/facturation/facture/1/payment/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/facturation/facture/1/payment/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/facturation/facture/{facture_id}/payment/{payment_id}`


<!-- END_e4434b98aff6a7237ed2874f60299888 -->

#Infrastructure/Registar


<!-- START_96b4c21ae246091b6aa6662aa8cd4738 -->
## List Registar
Liste des Fournisseurs d&#039;accès (Registar)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/infrastructure/registar" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/registar"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/infrastructure/registar',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/infrastructure/registar`


<!-- END_96b4c21ae246091b6aa6662aa8cd4738 -->

<!-- START_1d33cbb87cb524de8bf3a9577e8d3936 -->
## Store Registar
Création d&#039;un fournisseur (Registar)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/infrastructure/registar" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"OVH"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/registar"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "OVH"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/infrastructure/registar',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'OVH',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/infrastructure/registar`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom du fournisseur.
    
<!-- END_1d33cbb87cb524de8bf3a9577e8d3936 -->

<!-- START_9e576bfe26e7e4345cfad7d5fff14c4b -->
## Get Registar
Information d&#039;un fournisseur (Registar)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/infrastructure/registar/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/registar/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/infrastructure/registar/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/infrastructure/registar/{registar_id}`


<!-- END_9e576bfe26e7e4345cfad7d5fff14c4b -->

<!-- START_de9bf47ee8b3a8772dec71f3beb7b5cc -->
## Update Registar
Met à jour les informations d&#039;un fournisseur (Registar)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/infrastructure/registar/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"OVH"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/registar/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "OVH"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/infrastructure/registar/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'OVH',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/infrastructure/registar/{registar_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom du fournisseur.
    
<!-- END_de9bf47ee8b3a8772dec71f3beb7b5cc -->

<!-- START_6770950c48ed7fb4a20a2f918de7ac2f -->
## Delete Registar
Supprime le fournisseur (Registar)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/infrastructure/registar/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/registar/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/infrastructure/registar/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/infrastructure/registar/{registar_id}`


<!-- END_6770950c48ed7fb4a20a2f918de7ac2f -->

#Infrastructure/Server


<!-- START_b09edc138c6bd08228245848f73dcdf8 -->
## List Server
Liste des Serveurs

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/infrastructure/server" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/server"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/infrastructure/server',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/infrastructure/server`


<!-- END_b09edc138c6bd08228245848f73dcdf8 -->

<!-- START_74d04b602489906f64c34de179513ad8 -->
## Create Server
Création d&#039;un serveur

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/infrastructure/server" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"registar_id":1,"name":"ns225366.ip-ovh.org","ip":"215.36.220.120"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/server"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "registar_id": 1,
    "name": "ns225366.ip-ovh.org",
    "ip": "215.36.220.120"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/infrastructure/server',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'registar_id' => 1,
            'name' => 'ns225366.ip-ovh.org',
            'ip' => '215.36.220.120',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/infrastructure/server`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `registar_id` | integer |  required  | ID du registar supportant le serveur.
        `name` | string |  required  | Nom du serveur.
        `ip` | string |  required  | Adresse IP du serveur.
    
<!-- END_74d04b602489906f64c34de179513ad8 -->

<!-- START_93c15529db4c06dce05b87c45a8dbdd6 -->
## Get Server
Affiche les information du serveur

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/infrastructure/server/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/server/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/infrastructure/server/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/infrastructure/server/{server_id}`


<!-- END_93c15529db4c06dce05b87c45a8dbdd6 -->

<!-- START_b6db365c8581700ce6115775e41b54e2 -->
## Update Server
Met à jour les information d&#039;un serveur

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/infrastructure/server/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"registar_id":1,"name":"ns225366.ip-ovh.org","ip":"215.36.220.120"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/server/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "registar_id": 1,
    "name": "ns225366.ip-ovh.org",
    "ip": "215.36.220.120"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/infrastructure/server/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'registar_id' => 1,
            'name' => 'ns225366.ip-ovh.org',
            'ip' => '215.36.220.120',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/infrastructure/server/{server_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `registar_id` | integer |  required  | ID du registar supportant le serveur.
        `name` | string |  required  | Nom du serveur.
        `ip` | string |  required  | Adresse IP du serveur.
    
<!-- END_b6db365c8581700ce6115775e41b54e2 -->

<!-- START_62f799243b9ab1a023f9a3a8fb8f9ad6 -->
## Delete Server
Supprime un serveur

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/infrastructure/server/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/infrastructure/server/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/infrastructure/server/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/infrastructure/server/{server_id}`


<!-- END_62f799243b9ab1a023f9a3a8fb8f9ad6 -->

#Prestation/Famille


<!-- START_54889095ee4f2acde4b026f512d9aafd -->
## List Famille
[Liste des familles de services]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/famille" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/famille',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "data": "Liste des Familles de service"
}
```

### HTTP Request
`GET api/prestation/famille`


<!-- END_54889095ee4f2acde4b026f512d9aafd -->

<!-- START_807b59dc3472ec98517f5bd2ddd0e382 -->
## Create Famille
[Création d&#039;une famille de service]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/famille" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Application"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Application"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/famille',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Application',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "data": "Exception",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "famille": "Famille créer"
}
```

### HTTP Request
`POST api/prestation/famille`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom de la famille.
    
<!-- END_807b59dc3472ec98517f5bd2ddd0e382 -->

<!-- START_57da0c0de4416a3b81938bf39082a32f -->
## Get Famille
[Affiche les informations d&#039;une famille]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "famille": "Information de la famille (Array)"
}
```

### HTTP Request
`GET api/prestation/famille/{famille_id}`


<!-- END_57da0c0de4416a3b81938bf39082a32f -->

<!-- START_40b1a9415052eb89ec4c1737b6c6425e -->
## Update Famille
[Met à jour la famille]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Application"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Application"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Application',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "data": "Exception",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "famille": "Famille mise à jour"
}
```

### HTTP Request
`POST api/prestation/famille/{famille_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom de la famille.
    
<!-- END_40b1a9415052eb89ec4c1737b6c6425e -->

<!-- START_56ee7148039820d2f6bf552125f21572 -->
## Delete Famille
Supprime une famille de service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/famille/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/famille/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "famille": "Famille Supprimer"
}
```

### HTTP Request
`DELETE api/prestation/famille/{famille_id}`


<!-- END_56ee7148039820d2f6bf552125f21572 -->

<!-- START_d0441aaf14f085b980cbf0c90d114e12 -->
## Get Services
Liste des services d&#039;une famille

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/famille/1/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/famille/1/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/famille/1/service',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "services": "Liste des services de la famille ID"
}
```

### HTTP Request
`GET api/prestation/famille/{famille_id}/service`


<!-- END_d0441aaf14f085b980cbf0c90d114e12 -->

#Prestation/Module


<!-- START_43ce1f10ce918f1df6e87fa77ecb8398 -->
## List Module
Les modules doivent être lister pour pouvoir être accessible par l&#039;ensemble des infrastructures de l&#039;API (Organe Moteur)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "modules": "Liste des Modules"
}
```

### HTTP Request
`GET api/prestation/module`


<!-- END_43ce1f10ce918f1df6e87fa77ecb8398 -->

<!-- START_1442b581801941657c497b2827b12af6 -->
## Create Module
Création d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"ANCV","description":"Ce module permet la gestion et la comptabilit\u00e9 des Ch\u00e8ques Vacance ANCV","version":"0.0.1","release":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ANCV",
    "description": "Ce module permet la gestion et la comptabilit\u00e9 des Ch\u00e8ques Vacance ANCV",
    "version": "0.0.1",
    "release": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'ANCV',
            'description' => 'Ce module permet la gestion et la comptabilité des Chèques Vacance ANCV',
            'version' => '0.0.1',
            'release' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
null
```
> Example response (201):

```json
{
    "module": "Module Crée"
}
```

### HTTP Request
`POST api/prestation/module`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom du module.
        `description` | text |  optional  | Description du module.
        `version` | string |  required  | Version du module.
        `release` | integer |  optional  | Etat de développement du module.
    
<!-- END_1442b581801941657c497b2827b12af6 -->

<!-- START_be78ec79b381345608c5f6f340395d96 -->
## Get Module
Information d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "module": "Module"
}
```

### HTTP Request
`GET api/prestation/module/{module_id}`


<!-- END_be78ec79b381345608c5f6f340395d96 -->

<!-- START_e7114a00990b6cd711ae7cfb85093baf -->
## Update Module
Mise à jour d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"ANCV","description":"Ce module permet la gestion et la comptabilit\u00e9 des Ch\u00e8ques Vacance ANCV","version":"0.0.1","release":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ANCV",
    "description": "Ce module permet la gestion et la comptabilit\u00e9 des Ch\u00e8ques Vacance ANCV",
    "version": "0.0.1",
    "release": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'ANCV',
            'description' => 'Ce module permet la gestion et la comptabilité des Chèques Vacance ANCV',
            'version' => '0.0.1',
            'release' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "module": "Module mis à jour"
}
```

### HTTP Request
`POST api/prestation/module/{module_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom du module.
        `description` | text |  optional  | Description du module.
        `version` | string |  required  | Version du module.
        `release` | integer |  optional  | Etat de développement du module.
    
<!-- END_e7114a00990b6cd711ae7cfb85093baf -->

<!-- START_aa06e26971da20a98d8d9ac638d23f77 -->
## Delete Module
Suppression d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/module/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/module/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "module": "Module supprimée"
}
```

### HTTP Request
`DELETE api/prestation/module/{module_id}`


<!-- END_aa06e26971da20a98d8d9ac638d23f77 -->

#Prestation/Module/Changelog


<!-- START_6459dfdfae421ecfbf87b4cc786de0f6 -->
## List Changelog
Affiche la liste des mise à jours sur un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module/1/changelog" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/changelog"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module/1/changelog',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "changelogs": "Liste des Changelogs"
}
```

### HTTP Request
`GET api/prestation/module/{module_id}/changelog`


<!-- END_6459dfdfae421ecfbf87b4cc786de0f6 -->

<!-- START_6f964325d73ed4b697c09ddc9ae72a58 -->
## Create Changelog
Création d&#039;une note

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module/1/changelog" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"version":"0.0.1","description":"Description de la mise \u00e0 jours","date":"2020-04-18 08:00:00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/changelog"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "version": "0.0.1",
    "description": "Description de la mise \u00e0 jours",
    "date": "2020-04-18 08:00:00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module/1/changelog',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'version' => '0.0.1',
            'description' => 'Description de la mise à jours',
            'date' => '2020-04-18 08:00:00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Message exception validation",
    "errors": "Liste des erreurs de validations"
}
```
> Example response (201):

```json
{
    "changelog": "Changelog Crée"
}
```

### HTTP Request
`POST api/prestation/module/{module_id}/changelog`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `version` | required |  optional  | string Version de la mise à jours.
        `description` | required |  optional  | string Description de la mise à jours.
        `date` | required |  optional  | date Date de la mise à jours.
        `state` | integer |  optional  | Etat de la mise à jours.
    
<!-- END_6f964325d73ed4b697c09ddc9ae72a58 -->

<!-- START_5718af175df06e9085b939135cfa5ff6 -->
## Get Changelog
Note de Mise à jour

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module/1/changelog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/changelog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module/1/changelog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "changelog": "Information sur une mise à jour"
}
```

### HTTP Request
`GET api/prestation/module/{module_id}/changelog/{changelog_id}`


<!-- END_5718af175df06e9085b939135cfa5ff6 -->

<!-- START_7f4955b7d1569a5b39ff7e10e99019ed -->
## Update Changelog
Mise à jour d&#039;une note de mise à jour

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module/1/changelog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"version":"0.0.1","description":"Description de la mise \u00e0 jours","date":"2020-04-18 08:00:00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/changelog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "version": "0.0.1",
    "description": "Description de la mise \u00e0 jours",
    "date": "2020-04-18 08:00:00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module/1/changelog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'version' => '0.0.1',
            'description' => 'Description de la mise à jours',
            'date' => '2020-04-18 08:00:00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "changelog": "Changelog mise à jour"
}
```

### HTTP Request
`POST api/prestation/module/{module_id}/changelog/{changelog_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `version` | string |  optional  | Version de la mise à jours.
        `description` | string |  optional  | Description de la mise à jours.
        `date` | date |  optional  | Date de la mise à jours.
        `state` | integer |  optional  | Etat de la mise à jours.
    
<!-- END_7f4955b7d1569a5b39ff7e10e99019ed -->

<!-- START_2984d736e46b86155d0856eafc55f263 -->
## Delete Changelog
Supprime une note de mise à jour

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/module/1/changelog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/changelog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/module/1/changelog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "changelog": "Changelog supprimée"
}
```

### HTTP Request
`DELETE api/prestation/module/{module_id}/changelog/{changelog_id}`


<!-- END_2984d736e46b86155d0856eafc55f263 -->

#Prestation/Module/Task


<!-- START_7355d78cb2235b2f83bf1e921285b81e -->
## List Taches
Tache d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module/1/task" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/task"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module/1/task',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "tasks": "Liste des taches"
}
```

### HTTP Request
`GET api/prestation/module/{module_id}/task`


<!-- END_7355d78cb2235b2f83bf1e921285b81e -->

<!-- START_a16dd5aa7d8133833fe63ee739583716 -->
## Create Task
Création d&#039;une tache

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module/1/task" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"subject":"Restructration du module","description":"Il s'agit de refactoriser le syst\u00e8me du module avec la norme PSR-15, PSR-16, PSR-22","start":"2020-04-17 18:00:00","end":"2020-04-23 00:00:00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/task"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "subject": "Restructration du module",
    "description": "Il s'agit de refactoriser le syst\u00e8me du module avec la norme PSR-15, PSR-16, PSR-22",
    "start": "2020-04-17 18:00:00",
    "end": "2020-04-23 00:00:00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module/1/task',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'subject' => 'Restructration du module',
            'description' => 'Il s\'agit de refactoriser le système du module avec la norme PSR-15, PSR-16, PSR-22',
            'start' => '2020-04-17 18:00:00',
            'end' => '2020-04-23 00:00:00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/prestation/module/{module_id}/task`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `subject` | string |  required  | Sujet de la tache.
        `description` | string |  required  | Description de la tache.
        `start` | date |  required  | Date de début de la tache.
        `end` | date |  required  | Date de fin de la tache.
        `state` | integer |  optional  | Etat de la tache.Default: 0.
    
<!-- END_a16dd5aa7d8133833fe63ee739583716 -->

<!-- START_961b9cc309bc379fed0d96b66e565ff4 -->
## Get Task
Information d&#039;une tache particulière

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/module/1/task/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/task/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/module/1/task/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "task": "Information de la tache"
}
```

### HTTP Request
`GET api/prestation/module/{module_id}/task/{task_id}`


<!-- END_961b9cc309bc379fed0d96b66e565ff4 -->

<!-- START_1622ad1de1906eca78d29e102eea9605 -->
## Update Tache
Mise à jour d&#039;une tache

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/module/1/task/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"subject":"Restructration du module","description":"Il s'agit de refactoriser le syst\u00e8me du module avec la norme PSR-15, PSR-16, PSR-22","start":"2020-04-17 18:00:00","end":"2020-04-23 00:00:00","state":0}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/task/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "subject": "Restructration du module",
    "description": "Il s'agit de refactoriser le syst\u00e8me du module avec la norme PSR-15, PSR-16, PSR-22",
    "start": "2020-04-17 18:00:00",
    "end": "2020-04-23 00:00:00",
    "state": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/module/1/task/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'subject' => 'Restructration du module',
            'description' => 'Il s\'agit de refactoriser le système du module avec la norme PSR-15, PSR-16, PSR-22',
            'start' => '2020-04-17 18:00:00',
            'end' => '2020-04-23 00:00:00',
            'state' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "task": "Tache mise à jour"
}
```

### HTTP Request
`POST api/prestation/module/{module_id}/task/{task_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `subject` | string |  optional  | Sujet de la tache.
        `description` | string |  optional  | Description de la tache.
        `start` | date |  optional  | Date de début de la tache.
        `end` | date |  optional  | Date de fin de la tache.
        `state` | integer |  optional  | Etat de la tache.Default: 0.
    
<!-- END_1622ad1de1906eca78d29e102eea9605 -->

<!-- START_d8d21b8e4d812141d0d40e3b23f5fa58 -->
## Delete Tache
Supprime une tache d&#039;un module

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/module/1/task/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/module/1/task/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/module/1/task/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "task": "Tache Supprimer"
}
```

### HTTP Request
`DELETE api/prestation/module/{module_id}/task/{task_id}`


<!-- END_d8d21b8e4d812141d0d40e3b23f5fa58 -->

#Prestation/Service


<!-- START_870fda5c2e36012420e29077ff717716 -->
## List Service
Liste des Services

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/service',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "services": "Liste des services"
}
```

### HTTP Request
`GET api/prestation/service`


<!-- END_870fda5c2e36012420e29077ff717716 -->

<!-- START_51265a5e832e051f149a818b687f4ee0 -->
## Create Service
Création d&#039;un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"famille_id":1,"name":"voluptas","kernel":18,"module_id":15}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "famille_id": 1,
    "name": "voluptas",
    "kernel": 18,
    "module_id": 15
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/service',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'famille_id' => 1,
            'name' => 'voluptas',
            'kernel' => 18,
            'module_id' => 15,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Message d'exception",
    "errors": "Liste des erreurs de validation"
}
```
> Example response (201):

```json
{
    "service": "Service créer"
}
```

### HTTP Request
`POST api/prestation/service`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `famille_id` | integer |  required  | ID de la famille associé au service.
        `name` | string |  required  | Désignation du service.example: Accès test au logiciel Srice
        `kernel` | integer |  required  | Liste de Choix (0: Aucun |1: création d'espace |2: Activation d'un module).example: 0
        `module_id` | integer |  optional  | Si le kernel est à 2, module à activé.
    
<!-- END_51265a5e832e051f149a818b687f4ee0 -->

<!-- START_a0c215b3dbb4c673398ffc9542bc7744 -->
## Get Service
Information sur un service précis

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "service": "Information sur le service"
}
```

### HTTP Request
`GET api/prestation/service/{service_id}`


<!-- END_a0c215b3dbb4c673398ffc9542bc7744 -->

<!-- START_5b6dee0adbbd89420a81888af6a0a5e1 -->
## Update Service
Mise à jour d&#039;un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"famille_id":1,"name":"debitis","kernel":19,"module_id":16}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "famille_id": 1,
    "name": "debitis",
    "kernel": 19,
    "module_id": 16
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'famille_id' => 1,
            'name' => 'debitis',
            'kernel' => 19,
            'module_id' => 16,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "service": "Information sur le service mis à jour"
}
```

### HTTP Request
`POST api/prestation/service/{service_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `famille_id` | integer |  optional  | ID de la famille associé au service.
        `name` | string |  optional  | Désignation du service.example: Accès test au logiciel Srice
        `kernel` | integer |  optional  | Liste de Choix (0: Aucun |1: création d'espace |2: Activation d'un module).example: 0
        `module_id` | integer |  optional  | Si le kernel est à 2, module à activé.
    
<!-- END_5b6dee0adbbd89420a81888af6a0a5e1 -->

<!-- START_1fc1c6a8f153577aaccf5583ef032480 -->
## Delete Service
Supprime un service

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/service/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "service": "Service supprimer"
}
```

### HTTP Request
`DELETE api/prestation/service/{service_id}`


<!-- END_1fc1c6a8f153577aaccf5583ef032480 -->

#Prestation/Service/Tarif


<!-- START_596041cbb8c140c79db47aaab53ee074 -->
## List Tarifs
Liste des tarifs par service

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/service/1/tarif" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1/tarif"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/service/1/tarif',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (404):

```json
{
    "message": "Aucun Tarif pour ce service"
}
```
> Example response (201):

```json
{
    "tarifs": "Liste des tarif par service"
}
```

### HTTP Request
`GET api/prestation/service/{service_id}/tarif`


<!-- END_596041cbb8c140c79db47aaab53ee074 -->

<!-- START_ef2f44769bb65a7b59b4acd1f602afee -->
## Create Tarif
Création d&#039;un tarif

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/service/1/tarif" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Tarif Unique","amount":"12.00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1/tarif"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Tarif Unique",
    "amount": "12.00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/service/1/tarif',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Tarif Unique',
            'amount' => '12.00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Exception de Validation",
    "errors": "Erreur de Validation"
}
```
> Example response (201):

```json
{
    "tarif": "Tarif Créer"
}
```

### HTTP Request
`POST api/prestation/service/{service_id}/tarif`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom du tarif.
        `amount` | string |  required  | Montant du tarif.
    
<!-- END_ef2f44769bb65a7b59b4acd1f602afee -->

<!-- START_3c566c6194e5c62beb40641c9b8018d7 -->
## Get Tarif
Information d&#039;un tarif

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/prestation/service/1/tarif/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1/tarif/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/prestation/service/1/tarif/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "tarif": "Information d'un tarif"
}
```

### HTTP Request
`GET api/prestation/service/{service_id}/tarif/{tarif_id}`


<!-- END_3c566c6194e5c62beb40641c9b8018d7 -->

<!-- START_04c98cd5bc59abd33b52cccfce93898c -->
## Update Tarif
Mise à jour d&#039;un tarif

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/prestation/service/1/tarif/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Tarif Unique","amount":"13.00"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1/tarif/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Tarif Unique",
    "amount": "13.00"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/prestation/service/1/tarif/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Tarif Unique',
            'amount' => '13.00',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (422):

```json
{
    "exception": "Exception de Validation",
    "errors": "Erreur de Validation"
}
```
> Example response (201):

```json
{
    "tarif": "Tarif Mise à jour"
}
```

### HTTP Request
`POST api/prestation/service/{service_id}/tarif/{tarif_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Nom du tarif.
        `amount` | string |  optional  | Montant du tarif.
    
<!-- END_04c98cd5bc59abd33b52cccfce93898c -->

<!-- START_62575267797fcba76f62503e88c3d868 -->
## Delete Tarif
Suppression d&#039;un tarif

> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/prestation/service/1/tarif/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/prestation/service/1/tarif/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/prestation/service/1/tarif/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "tarif": "Tarif Supprimer"
}
```

### HTTP Request
`DELETE api/prestation/service/{service_id}/tarif/{tarif_id}`


<!-- END_62575267797fcba76f62503e88c3d868 -->

#Utilisateur


<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## Information utilisateur
[Affiche les informations relative à l&#039;utilisateur connecter]

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/user',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": "Information de l'utilisateur"
}
```
> Example response (401):

```json
{
    "data": "Non Connecter"
}
```

### HTTP Request
`GET api/user`


<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

#Webservice/Blog


<!-- START_314ffc4d3372ab348187e6562d73c5ea -->
## List Blog
LIste des articles

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/webservice/blog" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/webservice/blog',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/webservice/blog`


<!-- END_314ffc4d3372ab348187e6562d73c5ea -->

<!-- START_a51485e5026e64b1e6da1e216ed309b8 -->
## Create Blog
Création d&#039;un article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/webservice/blog" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"blog_category_id":1,"title":"Nouvelle d\u00e9rogation COVID19 pour les CE et CSE","content":"Lorem"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "blog_category_id": 1,
    "title": "Nouvelle d\u00e9rogation COVID19 pour les CE et CSE",
    "content": "Lorem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/webservice/blog',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'blog_category_id' => 1,
            'title' => 'Nouvelle dérogation COVID19 pour les CE et CSE',
            'content' => 'Lorem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/webservice/blog`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `blog_category_id` | integer |  required  | ID de la catégorie affilier à l'article.
        `title` | string |  required  | Titre de l'article.
        `content` | string |  required  | Contenue de l'article.
    
<!-- END_a51485e5026e64b1e6da1e216ed309b8 -->

<!-- START_67c65d33d905df2ac64ccc96f0d75581 -->
## Get Blog
Affiche les information d&#039;un article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/webservice/blog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/webservice/blog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/webservice/blog/{blog_id}`


<!-- END_67c65d33d905df2ac64ccc96f0d75581 -->

<!-- START_6d164c8c3ddae36b82fb5012061b76a1 -->
## Update Blog
Met à jour les information d&#039;un article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/webservice/blog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"blog_category_id":1,"title":"Nouvelle d\u00e9rogation COVID19 pour les CE et CSE","content":"Lorem"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "blog_category_id": 1,
    "title": "Nouvelle d\u00e9rogation COVID19 pour les CE et CSE",
    "content": "Lorem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/webservice/blog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'blog_category_id' => 1,
            'title' => 'Nouvelle dérogation COVID19 pour les CE et CSE',
            'content' => 'Lorem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/webservice/blog/{blog_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `blog_category_id` | integer |  required  | ID de la catégorie affilier à l'article.
        `title` | string |  required  | Titre de l'article.
        `content` | string |  required  | Contenue de l'article.
    
<!-- END_6d164c8c3ddae36b82fb5012061b76a1 -->

<!-- START_3dd77b74b0ad66853e5e4fd3bfee7e12 -->
## Delete Blog
Supprime un article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/webservice/blog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/webservice/blog/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/webservice/blog/{blog_id}`


<!-- END_3dd77b74b0ad66853e5e4fd3bfee7e12 -->

#Webservice/blog/category


<!-- START_c3b7b55fde95f1ca1f8476e720b107d1 -->
## List Category
Liste des catégories d&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/webservice/blog/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/webservice/blog/category',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/webservice/blog/category`


<!-- END_c3b7b55fde95f1ca1f8476e720b107d1 -->

<!-- START_7aa2a71a33325b78d2b5e7eefa711706 -->
## Create Category
Création d&#039;une catégorie d&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/webservice/blog/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Comit\u00e9"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Comit\u00e9"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/webservice/blog/category',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Comité',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/webservice/blog/category`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nom de la catégorie.
    
<!-- END_7aa2a71a33325b78d2b5e7eefa711706 -->

<!-- START_cf3386356463ebb475d6adfe11f171aa -->
## Get Category
Affiche les information d&#039;une catégorie d&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/webservice/blog/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/webservice/blog/category/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/webservice/blog/category/{category_id}`


<!-- END_cf3386356463ebb475d6adfe11f171aa -->

<!-- START_2f98fea85b63740e7f95885b9253ffd6 -->
## Update Category
Met à jour une catégorie d&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://api.srice.io/api/webservice/blog/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Comit\u00e9"}'

```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Comit\u00e9"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/api/webservice/blog/category/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Comité',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/webservice/blog/category/{category_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Nom de la catégorie.
    
<!-- END_2f98fea85b63740e7f95885b9253ffd6 -->

<!-- START_7e1e2e2a5286448c79a921f2cf84c5a6 -->
## Delete Category
Supprime une catégorie d&#039;article

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/api/webservice/blog/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/api/webservice/blog/category/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/webservice/blog/category/{category_id}`


<!-- END_7e1e2e2a5286448c79a921f2cf84c5a6 -->

<!-- START_cb32a1d002848b4fb3328301a88cf363 -->
## List Articles For Category
Liste les articles d&#039;une catégorie spécifié

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/api/webservice/blog/category/1/blogs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/api/webservice/blog/category/1/blogs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/api/webservice/blog/category/1/blogs',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/webservice/blog/category/{category_id}/blogs`


<!-- END_cb32a1d002848b4fb3328301a88cf363 -->

#general


<!-- START_e8e7beb3fd844493eabfb1904c1c42c8 -->
## List the route requests.

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/compass/request" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/request"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/compass/request',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "list": [
            {
                "id": "d7b7952e7fdddc07c978c9bdaf757acf",
                "storageId": null,
                "title": "api\/register",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/register",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Auth\\RegisterController@register"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "c3fa189a6c95ca36ad6ac4791a873d23",
                "storageId": null,
                "title": "api\/login",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/login",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Auth\\LoginController@login"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "61739f3220a224b34228600649230ad1",
                "storageId": null,
                "title": "api\/logout",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/logout",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Auth\\LoginController@logout"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "2b6e5a4b188cb183c7e59558cce36cb6",
                "storageId": null,
                "title": "api\/user",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/user",
                    "name": null,
                    "action": "App\\Http\\Controllers\\User\\UserController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9ae62e0b69707448e951d86e321a4b21",
                "storageId": null,
                "title": "api\/comite\/create",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/comite\/create",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ComiteController@create"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "56631c607ae198ac01ed24b3df65eef7",
                "storageId": null,
                "title": "api\/comite\/{comite_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/comite\/{comite_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ComiteController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "c315380cc79d9f4561953939b536fdc5",
                "storageId": null,
                "title": "api\/comite\/{comite_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/comite\/{comite_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ComiteController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "17e0d1fc9c3e90cafc64b57b50d9b6f2",
                "storageId": null,
                "title": "api\/comite\/{comite_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/comite\/{comite_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ComiteController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "790f39a2730031f1248a1534973156d8",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/contact",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/comite\/{comite_id}\/contact",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ContactController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9361d954d21924114011d9e61359f363",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ContactController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "b0ecedd50eb34e07b2d09fd629118140",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ContactController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "cf5b30dd2f0caf8793d5c75b1bb83549",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\ContactController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1cfd50087dd40273582638a51844dbb7",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/payment",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/comite\/{comite_id}\/payment",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\PaymentController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "84d99be637689946064a9c1d140bdea9",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\PaymentController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6b38e8c50ac9523846326e03dea420ea",
                "storageId": null,
                "title": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Comite\\PaymentController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "54889095ee4f2acde4b026f512d9aafd",
                "storageId": null,
                "title": "api\/prestation\/famille",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/famille",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "807b59dc3472ec98517f5bd2ddd0e382",
                "storageId": null,
                "title": "api\/prestation\/famille",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/famille",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "57da0c0de4416a3b81938bf39082a32f",
                "storageId": null,
                "title": "api\/prestation\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "40b1a9415052eb89ec4c1737b6c6425e",
                "storageId": null,
                "title": "api\/prestation\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "56ee7148039820d2f6bf552125f21572",
                "storageId": null,
                "title": "api\/prestation\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "d0441aaf14f085b980cbf0c90d114e12",
                "storageId": null,
                "title": "api\/prestation\/famille\/{famille_id}\/service",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/famille\/{famille_id}\/service",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@services"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "870fda5c2e36012420e29077ff717716",
                "storageId": null,
                "title": "api\/prestation\/service",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/service",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "51265a5e832e051f149a818b687f4ee0",
                "storageId": null,
                "title": "api\/prestation\/service",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/service",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "a0c215b3dbb4c673398ffc9542bc7744",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5b6dee0adbbd89420a81888af6a0a5e1",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1fc1c6a8f153577aaccf5583ef032480",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "596041cbb8c140c79db47aaab53ee074",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}\/tarif",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}\/tarif",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "ef2f44769bb65a7b59b4acd1f602afee",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}\/tarif",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}\/tarif",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "3c566c6194e5c62beb40641c9b8018d7",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "04c98cd5bc59abd33b52cccfce93898c",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "62575267797fcba76f62503e88c3d868",
                "storageId": null,
                "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "43ce1f10ce918f1df6e87fa77ecb8398",
                "storageId": null,
                "title": "api\/prestation\/module",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1442b581801941657c497b2827b12af6",
                "storageId": null,
                "title": "api\/prestation\/module",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "be78ec79b381345608c5f6f340395d96",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "e7114a00990b6cd711ae7cfb85093baf",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "aa06e26971da20a98d8d9ac638d23f77",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7355d78cb2235b2f83bf1e921285b81e",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/task",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/task",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "a16dd5aa7d8133833fe63ee739583716",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/task",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/task",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "961b9cc309bc379fed0d96b66e565ff4",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1622ad1de1906eca78d29e102eea9605",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "d8d21b8e4d812141d0d40e3b23f5fa58",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6459dfdfae421ecfbf87b4cc786de0f6",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/changelog",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/changelog",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6f964325d73ed4b697c09ddc9ae72a58",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/changelog",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/changelog",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5718af175df06e9085b939135cfa5ff6",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7f4955b7d1569a5b39ff7e10e99019ed",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "2984d736e46b86155d0856eafc55f263",
                "storageId": null,
                "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@delete"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "dc1ba7cf1d8246f3c8523df348a7373d",
                "storageId": null,
                "title": "api\/facturation\/commande",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9e53b3cc4e4c91ab9580c1d4241efee0",
                "storageId": null,
                "title": "api\/facturation\/commande",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "72c2795db9f239d81b7d55a1bcd8ad85",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7c50148d8afe9e9effe295c85f54a13b",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "aa05cea674d657289cfe2c5a056d59de",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5e51999ca696d4e09b5cbb69f27cb415",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/item",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/item",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1d55744614858d47af2780a97be8c70b",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/item",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/item",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "d49e1351c597a556b9b2812b44c59d74",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "477e23286870d43bc316daeea75eed39",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "dd608c73689271edd1b536abea8f07be",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6f052770c54a4e5f0eb51a4aa7c52636",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/payment",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/payment",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "160c154d81003084e3f9044b0f307dec",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/payment",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/payment",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "293d4414f4a82cb8b43d46378e6feaeb",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "0349d2d84ddcc66b91dceaf31f568dc3",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "037719fb38a2e6984c537fe4da6f3c17",
                "storageId": null,
                "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "08833fcb0532a21f16face030963aff3",
                "storageId": null,
                "title": "api\/facturation\/facture",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5c9cc01b44e8f89c3aceea650b48d080",
                "storageId": null,
                "title": "api\/facturation\/facture",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9bdb7557ed3e437aef0c14ff2753eeee",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "059fe742662643bb730bdd8428ea599d",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "e8ad281dfbc956dfc3e0e83c7114c293",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "822f9e0d57475c2d7d626242b10ecc3f",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/item",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/item",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5cb08abb92f64a56a1380c66fe628f92",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/item",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/item",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "cad106467dfe3434d5001da5f96616d8",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "c030f908d035f5113d25b323c7eff23c",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "f57391afc36f133a079ade2e31c48bde",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "fa0628a8454ce028940f421dbf27bc67",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/payment",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/payment",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "d3ef97a7f78af4c656b8561d916a0584",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/payment",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/payment",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "855d81e909cdf81ebfe22260e7e91dcd",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "de1382b46c7e05e79076f7beb396edd3",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "e4434b98aff6a7237ed2874f60299888",
                "storageId": null,
                "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "44d4bf69a3f7541622d83b3b801a66bc",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/contrat\/famille",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1a07940c673cbad3f3b05e4712532cc0",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/contrat\/famille",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1f2252c07b84ec3d004d97eefb6ad26f",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1364d71854b9e81a050335deb9c95731",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "46e4e5debbb1fb4eec4493ec1d8047d0",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9d1490a1fc0cc05a25668fb98a06310e",
                "storageId": null,
                "title": "api\/facturation\/contrat\/famille\/{famille_id}\/contrats",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/contrat\/famille\/{famille_id}\/contrats",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@contrats"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "8e730fcbeaa1f9981e95fdb2f00fdfd3",
                "storageId": null,
                "title": "api\/facturation\/contrat\/contrat",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/contrat\/contrat",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "88da8d83d864ebffd5e7ba45f736f3cc",
                "storageId": null,
                "title": "api\/facturation\/contrat\/contrat",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/contrat\/contrat",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "a00bb1a96b63fe0a9f5a8f6fc3194b75",
                "storageId": null,
                "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "32bf102b96ce8907cf5fe0a9bf024c4a",
                "storageId": null,
                "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6ce2f0c869503ea9c159bc235fd3376b",
                "storageId": null,
                "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "dc62b2dc078c9701a6fffa27cf8acfd0",
                "storageId": null,
                "title": "api\/espace",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5c5bf947ff1ef3ce88f32db651219296",
                "storageId": null,
                "title": "api\/espace",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "f50593cf30b16070e0882d0c695d0fa4",
                "storageId": null,
                "title": "api\/espace\/{espace_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "becebfd7a691fa0d6ba7e7e8286d4378",
                "storageId": null,
                "title": "api\/espace\/{espace_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "bacd7cb4c786cfafb8e554edd2adf6a7",
                "storageId": null,
                "title": "api\/espace\/{espace_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/espace\/{espace_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "fb4baa19230421f37918707293469221",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/service",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/service",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "eae085464bb97401fe11b79979f568b0",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/service",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/service",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5fafb725b8ce2f9ce1186ff522a4f8f6",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "5e71c2dce9ac0e0d8445dce57ecb379b",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "48bf2ed27ef55cbf1f91673272d25230",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "40569ab8729d0bcb3432725d7a3f5a5c",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/install",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/install",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "37e0eedfa888c69f566660cecf8fc14c",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/install",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/install",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "613620b442ddef4a28e597d228e38a65",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "0680adf5ff52d693bf5b5f701d2051c9",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1c9cb75f2c233fea52c5ed31afc98c61",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "a114cc69abbbacb1ab5c1af391ad7f50",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/licence",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/licence",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "52fdb3e8933096694f6bb9c468e8589e",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/licence",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/licence",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "02c86585f16aee44bcf295d8bed4d6f9",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7f3780a0e00dcac58b6fb3e16a2f9d19",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9ed494b491a6441937b0d3adb6d30762",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "4a4ea05440a7a108bd7a3accda205301",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/module",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/module",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@index"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "ef9a5b2841986b59f12dc3e0505d2a7d",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/module",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/module",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "2d221df66f97d35f992bc931eeeed529",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@show"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "b5e3c9afe8eaecea8af571254ace96b0",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "566bd18fabec881d37e28362a4454ade",
                "storageId": null,
                "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "b09edc138c6bd08228245848f73dcdf8",
                "storageId": null,
                "title": "api\/infrastructure\/server",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/infrastructure\/server",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "74d04b602489906f64c34de179513ad8",
                "storageId": null,
                "title": "api\/infrastructure\/server",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/infrastructure\/server",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "93c15529db4c06dce05b87c45a8dbdd6",
                "storageId": null,
                "title": "api\/infrastructure\/server\/{server_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/infrastructure\/server\/{server_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "b6db365c8581700ce6115775e41b54e2",
                "storageId": null,
                "title": "api\/infrastructure\/server\/{server_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/infrastructure\/server\/{server_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "62f799243b9ab1a023f9a3a8fb8f9ad6",
                "storageId": null,
                "title": "api\/infrastructure\/server\/{server_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/infrastructure\/server\/{server_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "96b4c21ae246091b6aa6662aa8cd4738",
                "storageId": null,
                "title": "api\/infrastructure\/registar",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/infrastructure\/registar",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "1d33cbb87cb524de8bf3a9577e8d3936",
                "storageId": null,
                "title": "api\/infrastructure\/registar",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/infrastructure\/registar",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "9e576bfe26e7e4345cfad7d5fff14c4b",
                "storageId": null,
                "title": "api\/infrastructure\/registar\/{registar_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/infrastructure\/registar\/{registar_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "de9bf47ee8b3a8772dec71f3beb7b5cc",
                "storageId": null,
                "title": "api\/infrastructure\/registar\/{registar_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/infrastructure\/registar\/{registar_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6770950c48ed7fb4a20a2f918de7ac2f",
                "storageId": null,
                "title": "api\/infrastructure\/registar\/{registar_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/infrastructure\/registar\/{registar_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "314ffc4d3372ab348187e6562d73c5ea",
                "storageId": null,
                "title": "api\/webservice\/blog",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/webservice\/blog",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "a51485e5026e64b1e6da1e216ed309b8",
                "storageId": null,
                "title": "api\/webservice\/blog",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/webservice\/blog",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "67c65d33d905df2ac64ccc96f0d75581",
                "storageId": null,
                "title": "api\/webservice\/blog\/{blog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/webservice\/blog\/{blog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "6d164c8c3ddae36b82fb5012061b76a1",
                "storageId": null,
                "title": "api\/webservice\/blog\/{blog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/webservice\/blog\/{blog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "3dd77b74b0ad66853e5e4fd3bfee7e12",
                "storageId": null,
                "title": "api\/webservice\/blog\/{blog_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/webservice\/blog\/{blog_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "c3b7b55fde95f1ca1f8476e720b107d1",
                "storageId": null,
                "title": "api\/webservice\/blog\/category",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/webservice\/blog\/category",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@list"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7aa2a71a33325b78d2b5e7eefa711706",
                "storageId": null,
                "title": "api\/webservice\/blog\/category",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/webservice\/blog\/category",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@store"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "cf3386356463ebb475d6adfe11f171aa",
                "storageId": null,
                "title": "api\/webservice\/blog\/category\/{category_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/webservice\/blog\/category\/{category_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@get"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "2f98fea85b63740e7f95885b9253ffd6",
                "storageId": null,
                "title": "api\/webservice\/blog\/category\/{category_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "POST"
                    ],
                    "uri": "api\/webservice\/blog\/category\/{category_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@update"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "7e1e2e2a5286448c79a921f2cf84c5a6",
                "storageId": null,
                "title": "api\/webservice\/blog\/category\/{category_id}",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "DELETE"
                    ],
                    "uri": "api\/webservice\/blog\/category\/{category_id}",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@destroy"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            },
            {
                "id": "cb32a1d002848b4fb3328301a88cf363",
                "storageId": null,
                "title": "api\/webservice\/blog\/category\/{category_id}\/blogs",
                "description": null,
                "content": [],
                "info": {
                    "domain": null,
                    "methods": [
                        "GET"
                    ],
                    "uri": "api\/webservice\/blog\/category\/{category_id}\/blogs",
                    "name": null,
                    "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@blogs"
                },
                "isExample": false,
                "examples": [],
                "createdAt": "2020-04-25 19:08:15",
                "updatedAt": "2020-04-25 19:08:15"
            }
        ],
        "group": {
            "api": [
                {
                    "id": "d7b7952e7fdddc07c978c9bdaf757acf",
                    "storageId": null,
                    "title": "api\/register",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/register",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Auth\\RegisterController@register"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "c3fa189a6c95ca36ad6ac4791a873d23",
                    "storageId": null,
                    "title": "api\/login",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/login",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Auth\\LoginController@login"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "61739f3220a224b34228600649230ad1",
                    "storageId": null,
                    "title": "api\/logout",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/logout",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Auth\\LoginController@logout"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "2b6e5a4b188cb183c7e59558cce36cb6",
                    "storageId": null,
                    "title": "api\/user",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/user",
                        "name": null,
                        "action": "App\\Http\\Controllers\\User\\UserController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9ae62e0b69707448e951d86e321a4b21",
                    "storageId": null,
                    "title": "api\/comite\/create",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/comite\/create",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ComiteController@create"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "56631c607ae198ac01ed24b3df65eef7",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/comite\/{comite_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ComiteController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "c315380cc79d9f4561953939b536fdc5",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/comite\/{comite_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ComiteController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "17e0d1fc9c3e90cafc64b57b50d9b6f2",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/comite\/{comite_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ComiteController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "790f39a2730031f1248a1534973156d8",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/contact",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/comite\/{comite_id}\/contact",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ContactController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9361d954d21924114011d9e61359f363",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ContactController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "b0ecedd50eb34e07b2d09fd629118140",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ContactController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "cf5b30dd2f0caf8793d5c75b1bb83549",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/comite\/{comite_id}\/contact\/{contact_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\ContactController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1cfd50087dd40273582638a51844dbb7",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/payment",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/comite\/{comite_id}\/payment",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\PaymentController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "84d99be637689946064a9c1d140bdea9",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\PaymentController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6b38e8c50ac9523846326e03dea420ea",
                    "storageId": null,
                    "title": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/comite\/{comite_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Comite\\PaymentController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "54889095ee4f2acde4b026f512d9aafd",
                    "storageId": null,
                    "title": "api\/prestation\/famille",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/famille",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "807b59dc3472ec98517f5bd2ddd0e382",
                    "storageId": null,
                    "title": "api\/prestation\/famille",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/famille",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "57da0c0de4416a3b81938bf39082a32f",
                    "storageId": null,
                    "title": "api\/prestation\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "40b1a9415052eb89ec4c1737b6c6425e",
                    "storageId": null,
                    "title": "api\/prestation\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "56ee7148039820d2f6bf552125f21572",
                    "storageId": null,
                    "title": "api\/prestation\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "d0441aaf14f085b980cbf0c90d114e12",
                    "storageId": null,
                    "title": "api\/prestation\/famille\/{famille_id}\/service",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/famille\/{famille_id}\/service",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Famille\\FamilleController@services"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "870fda5c2e36012420e29077ff717716",
                    "storageId": null,
                    "title": "api\/prestation\/service",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/service",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "51265a5e832e051f149a818b687f4ee0",
                    "storageId": null,
                    "title": "api\/prestation\/service",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/service",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "a0c215b3dbb4c673398ffc9542bc7744",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5b6dee0adbbd89420a81888af6a0a5e1",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1fc1c6a8f153577aaccf5583ef032480",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "596041cbb8c140c79db47aaab53ee074",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}\/tarif",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}\/tarif",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "ef2f44769bb65a7b59b4acd1f602afee",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}\/tarif",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}\/tarif",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "3c566c6194e5c62beb40641c9b8018d7",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "04c98cd5bc59abd33b52cccfce93898c",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "62575267797fcba76f62503e88c3d868",
                    "storageId": null,
                    "title": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/service\/{service_id}\/tarif\/{tarif_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Service\\ServiceTarifController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "43ce1f10ce918f1df6e87fa77ecb8398",
                    "storageId": null,
                    "title": "api\/prestation\/module",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1442b581801941657c497b2827b12af6",
                    "storageId": null,
                    "title": "api\/prestation\/module",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "be78ec79b381345608c5f6f340395d96",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "e7114a00990b6cd711ae7cfb85093baf",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "aa06e26971da20a98d8d9ac638d23f77",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7355d78cb2235b2f83bf1e921285b81e",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/task",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/task",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "a16dd5aa7d8133833fe63ee739583716",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/task",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/task",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "961b9cc309bc379fed0d96b66e565ff4",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1622ad1de1906eca78d29e102eea9605",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "d8d21b8e4d812141d0d40e3b23f5fa58",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/task\/{task_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleTaskController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6459dfdfae421ecfbf87b4cc786de0f6",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/changelog",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/changelog",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6f964325d73ed4b697c09ddc9ae72a58",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/changelog",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/changelog",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5718af175df06e9085b939135cfa5ff6",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7f4955b7d1569a5b39ff7e10e99019ed",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "2984d736e46b86155d0856eafc55f263",
                    "storageId": null,
                    "title": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/prestation\/module\/{module_id}\/changelog\/{changelog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Prestation\\Module\\ModuleChangelogController@delete"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "dc1ba7cf1d8246f3c8523df348a7373d",
                    "storageId": null,
                    "title": "api\/facturation\/commande",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9e53b3cc4e4c91ab9580c1d4241efee0",
                    "storageId": null,
                    "title": "api\/facturation\/commande",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "72c2795db9f239d81b7d55a1bcd8ad85",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7c50148d8afe9e9effe295c85f54a13b",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "aa05cea674d657289cfe2c5a056d59de",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5e51999ca696d4e09b5cbb69f27cb415",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/item",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/item",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1d55744614858d47af2780a97be8c70b",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/item",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/item",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "d49e1351c597a556b9b2812b44c59d74",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "477e23286870d43bc316daeea75eed39",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "dd608c73689271edd1b536abea8f07be",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandeItemController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6f052770c54a4e5f0eb51a4aa7c52636",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/payment",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/payment",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "160c154d81003084e3f9044b0f307dec",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/payment",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/payment",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "293d4414f4a82cb8b43d46378e6feaeb",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "0349d2d84ddcc66b91dceaf31f568dc3",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "037719fb38a2e6984c537fe4da6f3c17",
                    "storageId": null,
                    "title": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/commande\/{commande_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Commande\\CommandePaymentController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "08833fcb0532a21f16face030963aff3",
                    "storageId": null,
                    "title": "api\/facturation\/facture",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5c9cc01b44e8f89c3aceea650b48d080",
                    "storageId": null,
                    "title": "api\/facturation\/facture",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9bdb7557ed3e437aef0c14ff2753eeee",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "059fe742662643bb730bdd8428ea599d",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "e8ad281dfbc956dfc3e0e83c7114c293",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "822f9e0d57475c2d7d626242b10ecc3f",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/item",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/item",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5cb08abb92f64a56a1380c66fe628f92",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/item",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/item",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "cad106467dfe3434d5001da5f96616d8",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "c030f908d035f5113d25b323c7eff23c",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "f57391afc36f133a079ade2e31c48bde",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/item\/{item_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FactureItemController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "fa0628a8454ce028940f421dbf27bc67",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/payment",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/payment",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "d3ef97a7f78af4c656b8561d916a0584",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/payment",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/payment",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "855d81e909cdf81ebfe22260e7e91dcd",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "de1382b46c7e05e79076f7beb396edd3",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "e4434b98aff6a7237ed2874f60299888",
                    "storageId": null,
                    "title": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/facture\/{facture_id}\/payment\/{payment_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Facture\\FacturePaymentController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "44d4bf69a3f7541622d83b3b801a66bc",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/contrat\/famille",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1a07940c673cbad3f3b05e4712532cc0",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/contrat\/famille",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1f2252c07b84ec3d004d97eefb6ad26f",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1364d71854b9e81a050335deb9c95731",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "46e4e5debbb1fb4eec4493ec1d8047d0",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille\/{famille_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/contrat\/famille\/{famille_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9d1490a1fc0cc05a25668fb98a06310e",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/famille\/{famille_id}\/contrats",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/contrat\/famille\/{famille_id}\/contrats",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratFamilleController@contrats"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "8e730fcbeaa1f9981e95fdb2f00fdfd3",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/contrat",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/contrat\/contrat",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "88da8d83d864ebffd5e7ba45f736f3cc",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/contrat",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/contrat\/contrat",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "a00bb1a96b63fe0a9f5a8f6fc3194b75",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "32bf102b96ce8907cf5fe0a9bf024c4a",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6ce2f0c869503ea9c159bc235fd3376b",
                    "storageId": null,
                    "title": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/facturation\/contrat\/contrat\/{contrat_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Facturation\\Contrat\\ContratController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "dc62b2dc078c9701a6fffa27cf8acfd0",
                    "storageId": null,
                    "title": "api\/espace",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5c5bf947ff1ef3ce88f32db651219296",
                    "storageId": null,
                    "title": "api\/espace",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "f50593cf30b16070e0882d0c695d0fa4",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "becebfd7a691fa0d6ba7e7e8286d4378",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "bacd7cb4c786cfafb8e554edd2adf6a7",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/espace\/{espace_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "fb4baa19230421f37918707293469221",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/service",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/service",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "eae085464bb97401fe11b79979f568b0",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/service",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/service",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5fafb725b8ce2f9ce1186ff522a4f8f6",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "5e71c2dce9ac0e0d8445dce57ecb379b",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "48bf2ed27ef55cbf1f91673272d25230",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/service\/{service_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/espace\/{espace_id}\/service\/{service_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceServiceController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "40569ab8729d0bcb3432725d7a3f5a5c",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/install",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/install",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "37e0eedfa888c69f566660cecf8fc14c",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/install",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/install",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "613620b442ddef4a28e597d228e38a65",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "0680adf5ff52d693bf5b5f701d2051c9",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1c9cb75f2c233fea52c5ed31afc98c61",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/install\/{install_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/espace\/{espace_id}\/install\/{install_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceInstallController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "a114cc69abbbacb1ab5c1af391ad7f50",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/licence",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/licence",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "52fdb3e8933096694f6bb9c468e8589e",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/licence",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/licence",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "02c86585f16aee44bcf295d8bed4d6f9",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7f3780a0e00dcac58b6fb3e16a2f9d19",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9ed494b491a6441937b0d3adb6d30762",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/espace\/{espace_id}\/licence\/{licence_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceLicenceController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "4a4ea05440a7a108bd7a3accda205301",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/module",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/module",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@index"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "ef9a5b2841986b59f12dc3e0505d2a7d",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/module",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/module",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "2d221df66f97d35f992bc931eeeed529",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@show"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "b5e3c9afe8eaecea8af571254ace96b0",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "566bd18fabec881d37e28362a4454ade",
                    "storageId": null,
                    "title": "api\/espace\/{espace_id}\/module\/{module_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/espace\/{espace_id}\/module\/{module_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Espace\\EspaceModuleController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "b09edc138c6bd08228245848f73dcdf8",
                    "storageId": null,
                    "title": "api\/infrastructure\/server",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/infrastructure\/server",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "74d04b602489906f64c34de179513ad8",
                    "storageId": null,
                    "title": "api\/infrastructure\/server",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/infrastructure\/server",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "93c15529db4c06dce05b87c45a8dbdd6",
                    "storageId": null,
                    "title": "api\/infrastructure\/server\/{server_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/infrastructure\/server\/{server_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "b6db365c8581700ce6115775e41b54e2",
                    "storageId": null,
                    "title": "api\/infrastructure\/server\/{server_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/infrastructure\/server\/{server_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "62f799243b9ab1a023f9a3a8fb8f9ad6",
                    "storageId": null,
                    "title": "api\/infrastructure\/server\/{server_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/infrastructure\/server\/{server_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "96b4c21ae246091b6aa6662aa8cd4738",
                    "storageId": null,
                    "title": "api\/infrastructure\/registar",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/infrastructure\/registar",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "1d33cbb87cb524de8bf3a9577e8d3936",
                    "storageId": null,
                    "title": "api\/infrastructure\/registar",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/infrastructure\/registar",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "9e576bfe26e7e4345cfad7d5fff14c4b",
                    "storageId": null,
                    "title": "api\/infrastructure\/registar\/{registar_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/infrastructure\/registar\/{registar_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "de9bf47ee8b3a8772dec71f3beb7b5cc",
                    "storageId": null,
                    "title": "api\/infrastructure\/registar\/{registar_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/infrastructure\/registar\/{registar_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6770950c48ed7fb4a20a2f918de7ac2f",
                    "storageId": null,
                    "title": "api\/infrastructure\/registar\/{registar_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/infrastructure\/registar\/{registar_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Infrastructure\\Server\\ServerRegistarController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "314ffc4d3372ab348187e6562d73c5ea",
                    "storageId": null,
                    "title": "api\/webservice\/blog",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/webservice\/blog",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "a51485e5026e64b1e6da1e216ed309b8",
                    "storageId": null,
                    "title": "api\/webservice\/blog",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/webservice\/blog",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "67c65d33d905df2ac64ccc96f0d75581",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/{blog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/webservice\/blog\/{blog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "6d164c8c3ddae36b82fb5012061b76a1",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/{blog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/webservice\/blog\/{blog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "3dd77b74b0ad66853e5e4fd3bfee7e12",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/{blog_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/webservice\/blog\/{blog_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "c3b7b55fde95f1ca1f8476e720b107d1",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/webservice\/blog\/category",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@list"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7aa2a71a33325b78d2b5e7eefa711706",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/webservice\/blog\/category",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@store"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "cf3386356463ebb475d6adfe11f171aa",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category\/{category_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/webservice\/blog\/category\/{category_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@get"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "2f98fea85b63740e7f95885b9253ffd6",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category\/{category_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "POST"
                        ],
                        "uri": "api\/webservice\/blog\/category\/{category_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@update"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "7e1e2e2a5286448c79a921f2cf84c5a6",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category\/{category_id}",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "DELETE"
                        ],
                        "uri": "api\/webservice\/blog\/category\/{category_id}",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@destroy"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                },
                {
                    "id": "cb32a1d002848b4fb3328301a88cf363",
                    "storageId": null,
                    "title": "api\/webservice\/blog\/category\/{category_id}\/blogs",
                    "description": null,
                    "content": [],
                    "info": {
                        "domain": null,
                        "methods": [
                            "GET"
                        ],
                        "uri": "api\/webservice\/blog\/category\/{category_id}\/blogs",
                        "name": null,
                        "action": "App\\Http\\Controllers\\Webservice\\Blog\\BlogCategoryController@blogs"
                    },
                    "isExample": false,
                    "examples": [],
                    "createdAt": "2020-04-25 19:08:15",
                    "updatedAt": "2020-04-25 19:08:15"
                }
            ]
        }
    }
}
```

### HTTP Request
`GET compass/request`


<!-- END_e8e7beb3fd844493eabfb1904c1c42c8 -->

<!-- START_02cf06bea1f8f8c1de34c5081f6f1a6b -->
## Store the route request.

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/compass/request" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/request"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/compass/request',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST compass/request`


<!-- END_02cf06bea1f8f8c1de34c5081f6f1a6b -->

<!-- START_36143457206edb082e2bc8e0b89e2bbd -->
## Get route request with the given ID.

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/compass/request/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/request/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/compass/request/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET compass/request/{id}`


<!-- END_36143457206edb082e2bc8e0b89e2bbd -->

<!-- START_2538e24829e21603d4db89b736eca099 -->
## Store the response as example.

> Example request:

```bash
curl -X POST \
    "https://api.srice.io/compass/response" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/response"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://api.srice.io/compass/response',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST compass/response`


<!-- END_2538e24829e21603d4db89b736eca099 -->

<!-- START_1d69bc1789ad4bd1a2d5b8905ebd2832 -->
## Show the example of a response by given UUID.

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/compass/response/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/response/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/compass/response/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (404):

```json
{
    "message": "No query results for model [Davidhsianturi\\Compass\\Storage\\RouteModel]."
}
```

### HTTP Request
`GET compass/response/{uuid}`


<!-- END_1d69bc1789ad4bd1a2d5b8905ebd2832 -->

<!-- START_7f18b13effad542cf6dd94bdad478202 -->
## Delete the example of a response by given UUID.

> Example request:

```bash
curl -X DELETE \
    "https://api.srice.io/compass/response/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/response/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://api.srice.io/compass/response/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE compass/response/{uuid}`


<!-- END_7f18b13effad542cf6dd94bdad478202 -->

<!-- START_5be72cbfb5350e49750ef3f15438f8a1 -->
## Get a new credential of users.

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/compass/credentials" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/credentials"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/compass/credentials',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET compass/credentials`


<!-- END_5be72cbfb5350e49750ef3f15438f8a1 -->

<!-- START_2d555ee21489f83f8818821a93487743 -->
## Display the Compass view.

> Example request:

```bash
curl -X GET \
    -G "https://api.srice.io/compass/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api.srice.io/compass/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://api.srice.io/compass/',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET compass/{view?}`


<!-- END_2d555ee21489f83f8818821a93487743 -->


