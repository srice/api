<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(["middleware" => "auth:api", "prefix" => "user", "namespace" => "User"], function () {
    Route::get('/', 'UserController@get');
});

Route::group(["middleware" => "auth:api", "prefix" => "comite", "namespace" => "Comite"], function () {

    Route::post('create', 'ComiteController@create');
    Route::get('{comite_id}', 'ComiteController@get');
    Route::post('{comite_id}', 'ComiteController@update');
    Route::delete('{comite_id}', 'ComiteController@delete');

    Route::group(["prefix" => "{comite_id}/contact"], function () {
        Route::post('/', 'ContactController@store');
        Route::get('{contact_id}', 'ContactController@get');
        Route::post('{contact_id}', 'ContactController@update');
        Route::delete('{contact_id}', 'ContactController@delete');
    });

    Route::group(["prefix" => "{comite_id}/payment"], function () {
        Route::post('/', 'PaymentController@store');
        Route::get('{payment_id}', 'PaymentController@get');
        Route::delete('{payment_id}', 'PaymentController@delete');
    });
});

Route::group(["middleware" => "auth:api", "prefix" => "prestation", "namespace" => "Prestation"], function (){
    Route::group(["prefix" => "famille", "namespace" => "Famille"], function (){
        Route::get('/', 'FamilleController@list');
        Route::post('/', 'FamilleController@store');
        Route::get('{famille_id}', 'FamilleController@get');
        Route::post('{famille_id}', 'FamilleController@update');
        Route::delete('{famille_id}', 'FamilleController@delete');

        Route::get('{famille_id}/service', 'FamilleController@services');
    });

    Route::group(["prefix" => "service", "namespace" => "Service"], function (){
        Route::get('/', 'ServiceController@list');
        Route::post('/', 'ServiceController@store');
        Route::get('{service_id}', 'ServiceController@get');
        Route::post('{service_id}', 'ServiceController@update');
        Route::delete('{service_id}', 'ServiceController@delete');

        Route::group(["prefix" => '{service_id}/tarif'], function (){
            Route::get('/', 'ServiceTarifController@list');
            Route::post('/', 'ServiceTarifController@store');
            Route::get('{tarif_id}', 'ServiceTarifController@get');
            Route::post('{tarif_id}', 'ServiceTarifController@update');
            Route::delete('{tarif_id}', 'ServiceTarifController@delete');
        });
    });

    Route::group(["prefix" => "module", "namespace" => "Module"], function (){
        Route::get('/', 'ModuleController@list');
        Route::post('/', 'ModuleController@store');
        Route::get('{module_id}', 'ModuleController@get');
        Route::post('{module_id}', 'ModuleController@update');
        Route::delete('{module_id}', 'ModuleController@delete');

        Route::group(["prefix" => "{module_id}/task"], function (){
            Route::get('/', 'ModuleTaskController@list');
            Route::post('/', 'ModuleTaskController@store');
            Route::get('{task_id}', 'ModuleTaskController@get');
            Route::post('{task_id}', 'ModuleTaskController@update');
            Route::delete('{task_id}', 'ModuleTaskController@delete');
        });

        Route::group(["prefix" => "{module_id}/changelog"], function (){
            Route::get('/', 'ModuleChangelogController@list');
            Route::post('/', 'ModuleChangelogController@store');
            Route::get('{changelog_id}', 'ModuleChangelogController@get');
            Route::post('{changelog_id}', 'ModuleChangelogController@update');
            Route::delete('{changelog_id}', 'ModuleChangelogController@delete');
        });
    });


});

Route::group(["middleware" => "auth:api", "prefix" => "facturation", "namespace" => "Facturation"], function (){
    Route::group(["prefix" => "commande", "namespace" => "Commande"], function (){
        Route::get('/', 'CommandeController@list');
        Route::post('/', 'CommandeController@store');
        Route::get('{commande_id}', 'CommandeController@get');
        Route::post('{commande_id}', 'CommandeController@update');
        Route::delete('{commande_id}', 'CommandeController@destroy');

        Route::group(["prefix" => "{commande_id}/item"], function (){
            Route::get('/', 'CommandeItemController@list');
            Route::post('/', 'CommandeItemController@store');
            Route::get('{item_id}', 'CommandeItemController@get');
            Route::post('{item_id}', 'CommandeItemController@update');
            Route::delete('{item_id}', 'CommandeItemController@destroy');
        });

        Route::group(["prefix" => "{commande_id}/payment"], function (){
            Route::get('/', 'CommandePaymentController@list');
            Route::post('/', 'CommandePaymentController@store');
            Route::get('{payment_id}', 'CommandePaymentController@get');
            Route::post('{payment_id}', 'CommandePaymentController@update');
            Route::delete('{payment_id}', 'CommandePaymentController@destroy');
        });
    });
    Route::group(["prefix" => "facture", "namespace" => "Facture"], function (){
        Route::get('/', 'FactureController@list');
        Route::post('/', 'FactureController@store');
        Route::get('{facture_id}', 'FactureController@get');
        Route::post('{facture_id}', 'FactureController@update');
        Route::delete('{facture_id}', 'FactureController@destroy');

        Route::group(["prefix" => "{facture_id}/item"], function (){
            Route::get('/', 'FactureItemController@list');
            Route::post('/', 'FactureItemController@store');
            Route::get('{item_id}', 'FactureItemController@get');
            Route::post('{item_id}', 'FactureItemController@update');
            Route::delete('{item_id}', 'FactureItemController@destroy');
        });

        Route::group(["prefix" => "{facture_id}/payment"], function (){
            Route::get('/', "FacturePaymentController@list");
            Route::post('/', "FacturePaymentController@store");
            Route::get('{payment_id}', "FacturePaymentController@get");
            Route::post('{payment_id}', "FacturePaymentController@update");
            Route::delete('{payment_id}', "FacturePaymentController@destroy");
        });
    });
    Route::group(["prefix" => "contrat", "namespace" => "Contrat"], function (){
        Route::group(["prefix" => "famille"], function (){
            Route::get('/', 'ContratFamilleController@list');
            Route::post('/', 'ContratFamilleController@store');
            Route::get('{famille_id}', 'ContratFamilleController@show');
            Route::post('{famille_id}', 'ContratFamilleController@update');
            Route::delete('{famille_id}', 'ContratFamilleController@destroy');

            Route::get('{famille_id}/contrats', 'ContratFamilleController@contrats');
        });
        Route::group(["prefix" => "contrat"], function (){
            Route::get('/', 'ContratController@index');
            Route::post('/', 'ContratController@store');
            Route::get('{contrat_id}', 'ContratController@show');
            Route::post('{contrat_id}', 'ContratController@update');
            Route::delete('{contrat_id}', 'ContratController@destroy');
        });
    });
});

Route::group(["middleware" => "auth:api", "prefix" => "espace", "namespace" => "Espace"], function (){
    Route::get('/', 'EspaceController@index');
    Route::post('/', 'EspaceController@store');
    Route::get('{espace_id}', 'EspaceController@show');
    Route::post('{espace_id}', 'EspaceController@update');
    Route::delete('{espace_id}', 'EspaceController@destroy');

    Route::group(["prefix" => "{espace_id}/service"], function (){
        Route::get('/', 'EspaceServiceController@index');
        Route::post('/', 'EspaceServiceController@store');
        Route::get('{service_id}', 'EspaceServiceController@show');
        Route::post('{service_id}', 'EspaceServiceController@update');
        Route::delete('{service_id}', 'EspaceServiceController@destroy');
    });

    Route::group(["prefix" => "{espace_id}/install"], function (){
        Route::get('/', 'EspaceInstallController@index');
        Route::post('/', 'EspaceInstallController@store');
        Route::get('{install_id}', 'EspaceInstallController@show');
        Route::post('{install_id}', 'EspaceInstallController@update');
        Route::delete('{install_id}', 'EspaceInstallController@destroy');
    });

    Route::group(["prefix" => "{espace_id}/licence"], function (){
        Route::get('/', 'EspaceLicenceController@index');
        Route::post('/', 'EspaceLicenceController@store');
        Route::get('{licence_id}', 'EspaceLicenceController@show');
        Route::post('{licence_id}', 'EspaceLicenceController@update');
        Route::delete('{licence_id}', 'EspaceLicenceController@destroy');
    });

    Route::group(["prefix" => "{espace_id}/module"], function (){
        Route::get('/', 'EspaceModuleController@index');
        Route::post('/', 'EspaceModuleController@store');
        Route::get('{module_id}', 'EspaceModuleController@show');
        Route::post('{module_id}', 'EspaceModuleController@update');
        Route::delete('{module_id}', 'EspaceModuleController@destroy');
    });
});

Route::group(["middleware" => "auth:api", "prefix" => "infrastructure", "namespace" => "Infrastructure"], function (){
    Route::group(["prefix" => "server", "namespace" => "Server"], function (){
        Route::get('/', 'ServerController@list');
        Route::post('/', 'ServerController@store');
        Route::get('{server_id}', 'ServerController@get');
        Route::post('{server_id}', 'ServerController@update');
        Route::delete('{server_id}', 'ServerController@destroy');
    });
    Route::group(["prefix" => "registar", "namespace" => "Server"], function (){
        Route::get('/', "ServerRegistarController@list");
        Route::post('/', "ServerRegistarController@store");
        Route::get('{registar_id}', "ServerRegistarController@get");
        Route::post('{registar_id}', "ServerRegistarController@update");
        Route::delete('{registar_id}', "ServerRegistarController@destroy");
    });
});

Route::group(["middleware" => "auth:api", "prefix" => "webservice", "namespace" => "Webservice"], function (){
    Route::group(["prefix" => "blog", "namespace" => "Blog"], function (){
        Route::group(["prefix" => "blog"], function (){
            Route::get('/', 'BlogController@list');
            Route::post('/', 'BlogController@store');
            Route::get('{blog_id}', 'BlogController@get');
            Route::post('{blog_id}', 'BlogController@update');
            Route::delete('{blog_id}', 'BlogController@destroy');
        });

        Route::group(["prefix" => "category"], function (){
            Route::get('/', "BlogCategoryController@list");
            Route::post('/', "BlogCategoryController@store");
            Route::get('{category_id}', "BlogCategoryController@get");
            Route::post('{category_id}', "BlogCategoryController@update");
            Route::delete('{category_id}', "BlogCategoryController@destroy");

            Route::get('{category_id}/blogs', "BlogCategoryController@blogs");
        });
    });

    Route::group(["prefix" => "notification", "namespace" => "Notification"], function (){
        Route::post('/', 'NotificationController@push');
    });
});

Route::group(["middleware" => "auth:api", "prefix" => "support", "namespace" => "Support"], function (){
    Route::group(["prefix" => "ticket", "namespace" => "Ticket"], function (){
        Route::group(["prefix" => "ticket"], function (){
           Route::get('/', 'TicketController@list');
           Route::post('/', 'TicketController@store');
           Route::get('{ticket_id}', 'TicketController@get');
           Route::post('{ticket_id}', 'TicketController@update');
           Route::delete('{ticket_id}', 'TicketController@destroy');

           Route::group(["prefix" => "{ticket_id}/conversation"], function (){
               Route::get('/', "TicketConversationController@list");
               Route::post('/', "TicketConversationController@store");
               Route::get('{conv_id}', "TicketConversationController@get");
               Route::post('{conv_id}', "TicketConversationController@update");
               Route::delete('{conv_id}', "TicketConversationController@destroy");
           });
        });
        Route::group(["prefix" => "category"], function (){
            Route::get('/', 'TicketCategoryController@list');
            Route::post('/', 'TicketCategoryController@store');
            Route::get('{category_id}', 'TicketCategoryController@get');
            Route::post('{category_id}', 'TicketCategoryController@update');
            Route::delete('{category_id}', 'TicketCategoryController@destroy');

            Route::get('{category_id}/tickets', 'TicketCategoryController@tickets');
        });
    });

    Route::group(["prefix" => "travaux", "namespace" => "Travaux"], function (){
        Route::group(["prefix" => "secteur"], function (){
            Route::get('/', 'TravauxSecteurController@list');
            Route::post('/', 'TravauxSecteurController@store');
            Route::get('{secteur_id}', 'TravauxSecteurController@get');
            Route::post('{secteur_id}', 'TravauxSecteurController@update');
            Route::delete('{secteur_id}', 'TravauxSecteurController@destroy');
            Route::get('{secteur_id}/travaux', 'TravauxSecteurController@travaux');
        });

        Route::group(["prefix" => "issue"], function (){
            Route::get('/', 'TravauxController@list');
            Route::post('/', 'TravauxController@store');
            Route::get('{issue_id}', 'TravauxController@get');
            Route::post('{issue_id}', 'TravauxController@update');
            Route::delete('{issue_id}', 'TravauxController@destroy');

            Route::group(["prefix" => "{issue_id}/comment"], function (){
                Route::get('/', 'TravauxCommentController@list');
                Route::post('/', 'TravauxCommentController@store');
                Route::get('{comment_id}', 'TravauxCommentController@get');
                Route::post('{comment_id}', 'TravauxCommentController@update');
                Route::delete('{comment_id}', 'TravauxCommentController@destroy');
            });
        });
    });
});

